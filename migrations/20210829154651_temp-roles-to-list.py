from datetime import datetime
from typing import Optional

from beanie import Document, iterative_migration


class OldMember(Document):
    guild_id: int
    user_id: int
    sticky_roles: list[int] = []
    # Members with a personal role may modify it with `[p]myrole (name|colour)`
    # Such roles are also implicitly treated as sticky, even if the associated role's document
    # does not have sticky set to True.
    personal_role: Optional[int] = None
    # could be moved into a separate document type similarly to timed roles?
    banned_until: Optional[datetime] = None
    # Muted users implicitly have Guild#mute_role as a sticky role
    # These are two separate variables to allow for permanent mutes in the future
    muted: bool = False
    muted_until: Optional[datetime] = None
    temp_roles: dict[int, datetime] = {}
    previous_nicks = []
    # See: User#blocked
    blocked: bool = False

    class Collection:
        name = "member"


class NewMember(Document):
    guild_id: int
    user_id: int
    sticky_roles: list[int] = []
    # Members with a personal role may modify it with `[p]myrole (name|colour)`
    # Such roles are also implicitly treated as sticky, even if the associated role's document
    # does not have sticky set to True.
    personal_role: Optional[int] = None
    # could be moved into a separate document type similarly to timed roles?
    banned_until: Optional[datetime] = None
    # Muted users implicitly have Guild#mute_role as a sticky role
    # These are two separate variables to allow for permanent mutes in the future
    muted: bool = False
    muted_until: Optional[datetime] = None
    temp_roles: list[dict] = []
    previous_nicks = []
    # See: User#blocked
    blocked: bool = False

    class Collection:
        name = "member"


class Forward:
    @iterative_migration()
    async def to_list(self, input_document: OldMember, output_document: NewMember):
        output_document.temp_roles = [
            {"id": k, "expiry": v} for k, v in input_document.temp_roles.items()
        ]


class Backward:
    @iterative_migration()
    async def to_dict(self, input_document: NewMember, output_document: OldMember):
        output_document.temp_roles = {
            x.get("id"): x.get("expiry") for x in input_document.temp_roles
        }
