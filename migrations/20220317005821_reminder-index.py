from beanie import Document, free_fall_migration


class Reminder(Document):
    class Collection:
        name = "reminder"


class Forward:
    # noinspection PyUnusedLocal
    @free_fall_migration(document_models=[Reminder])
    async def remove_reminder_index(self, session):
        # we have to make use of low-level methods here to drop the index, as otherwise
        # creating reminders will break due to the unique index on 'reminder_id' and 'user_id',
        # as 'reminder_id' will always be null
        coll = Reminder.get_motor_collection()
        indexes: dict[str, dict] = await coll.index_information()
        for name, index in indexes.items():
            if not index.get("unique"):
                continue
            # we can't make use of sessions here as mongodb does not support any index-related
            # operations in sessions, so we have to do this without the safety net of the session
            # to save us, even though this is fairly safe to do.
            await coll.drop_index(name)


class Backward:
    ...
