# use with https://github.com/casey/just

# load environment variables from .env
set dotenv-load

default:
    just --list --unsorted

run-bot:
    python -m argon

run-migrations DISTANCE="0":
    beanie migrate -uri "$ARGON_MONGO_URI" -db "$ARGON_MONGO_DB" -p migrations --distance {{DISTANCE}}

create-migration NAME:
    beanie new-migration -n {{NAME}} -p migrations

locale-stubs:
    redgettext argon --recursive --command-docstrings -O "$(pwd)/argon/locales"
