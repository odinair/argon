# Argon

Argon is intended as a private moderation utility bot for Discord servers I like.

## Can I use this for my own community?

**You probably shouldn't.** This bot is largely designed around communities that I specifically feel
this bot would be a decent fit in, and as such may have features designed specifically for those communities.

While documentation to set this up is provided below, **you do so at your own peril;** support
**is not guaranteed.** As such, this bot expects that you're familiar enough with Python and discord.py
to figure out (and likely work around) any issues you run into (so long as they're not genuine
bugs - if so, reports of such would be appreciated!).

### Setup & Usage

**Argon requires the following privileged intents:**
- `Members`: Required for moderation logging and permission checks
- `Message Content`: Required for text-based commands and moderation logging
- `Presences`: Required for moderation logging (although this requirement can be made optional)

**Argon additionally requires on the host system:**
- A relatively recent MongoDB version with replication support enabled
- Python 3.10 and [Poetry](https://python-poetry.org/)

This is also best run on a Linux server, or anything similar enough (such as any of the BSD family).

```sh
git clone https://gitlab.com/celestialfault/argon.git && cd argon
# creating a venv is optional as poetry will do this if you don't,
# and as such this is really only useful for being able to reliably
# determine where the python binary is, such as for making systemd units
python -m venv .venv && source .venv/bin/activate
poetry install --no-dev
# If you skipped creating a virtual environment:
poetry shell

# Copy the example environment variable file to .env and open it in your favourite editor,
# such as nano, vim, etc. This file has a lot of documentation as to how all the provided
# environment variables interact with Argon and how to load it; **please read it!**
cp example.env .env && nano .env

# Run migrations - this isn't strictly required during initial install, but is recommended anyways
# so that Beanie knows that you aren't migrating from an incredibly old database version if a new
# migration is ever added, which would likely cause issues.
beanie migrate -uri "${ARGON_MONGO_URI}" -db "${ARGON_DB_NAME}" -p migrations
# Alternatively, if you have just (https://github.com/casey/just) installed, you can
# instead run:
just run-migrations

# Start the bot
python -m argon
```

By default, the bot will only listen to mentions to itself as a prefix; for example, `@Bot ping`.
This can be changed with `@Bot prefix add` or `@Bot prefix global add`.

Slash command (and context menu interactions) are also available, albeit with a much more reduced
amount of the bot features being available as such.

## Licensing

This project is licensed under the Zlib license; the full text of this license can be found [in LICENSE](LICENSE).

Some of the core code and concepts in this project are adapted from [Red-DiscordBot](https://github.com/Cog-Creators/Red-DiscordBot),
which is licensed under the [GPLv3 license](https://github.com/Cog-Creators/Red-DiscordBot/blob/V3/develop/LICENSE).
