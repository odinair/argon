# Lines starting with '#' denote comments.

# This file is provided to document all the environment variables
# that Argon requires/supports. It can be loaded in a bash-like shell
# (such as bash itself or zsh) with the following:
#
#   $ source .env
#
# Alternatively, you can tell Argon to load this file by specifying
# an 'ARGON_ENV_FILE' environment variable that points to this file.

# If you make use of this file, remember to copy this file to '.env'
# to properly ensure that any changes you make to this file are not
# included in git commits, or otherwise conflict when pulling new
# updates.

# It's recommended to change the permissions on this file after copying it to .env
# to ensure that your token isn't leaked if another user account is somehow compromised,
# like so:
#
#   $ chmod 600 .env
#
# Do note that this is __NOT__ a silver bullet solution, and you are still responsible for performing
# proper security measures to ensure your token doesn't get leaked; this is simply one of a few
# relatively easy steps one can perform to reduce possible attack vectors for obtaining your token.

#### Required variables ####
# MongoDB configuration
ARGON_MONGO_URI=mongodb://localhost:27017
ARGON_MONGO_DB=argon
# Your bot token; you can retrieve this from the Discord Developer
# dashboard. Keep this safe!
ARGON_TOKEN=

#### Optional variables ####
# How long in milliseconds the bot will wait for when connecting to MongoDB
# It's strongly recommended that you keep this at the default, and only change it
# if you absolutely must; a recommended maximum if you must change this is around
# 1000 milliseconds, or 1 second.
ARGON_MONGO_TIMEOUT=100
# If this is present, the bot will not sync application commands (such as /slash commands)
# upon starting up; this means that you must sync them manually with `[p]jsk sync`.
# The value of this does not matter; it only matters that it's present at all.
# This is primarily intended for development where you're likely going to be restarting
# the bot frequently, and would like to avoid running up against rate limits.
#NO_SYNC_COMMANDS=true
# If present, Argon will not request the Presences intent upon starting.
# This is largely only provided if you can't or don't want to give this bot
# the presences intent.
#NO_PRESENCE_INTENT=true
# If present, a 'gimmicks' cog is loaded, which may or may not be appropriate for your given use case.
# All gimmicks present in this cog require being specifically listed in this variable as a comma-separated
# list, such as 'ARGON_GIMMICKY=feature1,feature2,...'
#ARGON_GIMMICKY=

#### Optional Jishaku variables ####
# Change `[p]jsk py` to not prepend an underscore to variables like `ctx`, `bot`, etc
JISHAKU_NO_UNDERSCORE=1
# Send tracebacks in the current channel instead of in DMs; this only applies to commands
# such as `jsk py` and `jsk debug`.
JISHAKU_NO_DM_TRACEBACK=1
# Hide `[p]jsk` and its subcommands from `[p]help` (only for bot owners; it's already hidden
# for non-owners)
JISHAKU_HIDE=0
# Retain the last returned variable of `[p]jsk py` in the `_` variable
JISHAKU_RETAIN=1

#### Debug options ####
# If present, Argon will log messages of DEBUG severity; this is very noisy, and may
# include sensitive information, and as such shouldn't be enabled on production bots.
#ARGON_DEBUG=true
# Similarly to ARGON_DEBUG, this enables more verbose logging on discord.py.
# (more specifically, this sets logging to INFO, instead of the default WARNING)
# This is something you should only ever need to enable if you're investigating a
# discord.py issue directly.
# This variable must only be present for the above to take effect, and as such the
# exact value of it doesn't matter.
#DPY_DEBUG=true
# The guild ID for which application commands (such as /slash commands) should be registered to;
# if not set, they will be registered globally. This should only be set during development to
# quickly iterate on application commands, as the bot will not register them globally
# with this set.
#SLASH_GUILD=0123456789
# If present, an extra cog intended solely for debugging will be loaded.
# This only exposes extra functionality meant to test various parts of Argon, and is largely
# useless outside of development. As with NO_SYNC_COMMANDS and DPY_DEBUG, the value of this
# does not matter, but instead it only matters that this variable is present.
#LOAD_DEBUG_COG=true
#DEBUG_COG_GUILD=9876543210
