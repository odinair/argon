import asyncio
import logging
import os
from typing import TypeVar

import discord

from argon import commands, db
from argon.bot import Argon
from argon.errors import command_error
from argon.translations import Translator
from argon.utils import SLASH_GUILD, chat_formatting
from argon.utils.chat_formatting import info

_ = Translator()
_T = TypeVar("_T")


def _find_command(
    bot: Argon, data: dict[str, _T], command: commands.Command, *, default=None
) -> _T:
    cmd_parents: list[commands.Group] = command.parents  # perf: cache command parents
    parents: dict[commands.Command, _T] = {}
    for name, value in data.items():
        cmd = bot.command_dict.get(name)
        if cmd is None:
            continue
        # If this is an exact match, return early with the appropriate value
        elif cmd == command:
            return value
        # If this isn't an exact match, but the command is a parent of the given command,
        # then record it for later if we don't find an exact match
        elif cmd in cmd_parents:
            parents[cmd] = value

    # There's no exact match, so try our best to find a parent, or return the default if none exist
    return next((parents[x] for x in cmd_parents if x in parents), default)


def setup_events(bot: Argon):  # sourcery no-metrics
    log = logging.getLogger("argon.events")
    started = asyncio.Event()

    @bot.event
    async def on_ready():
        if started.is_set():
            return
        log.info("Successfully logged into Discord as %s", bot.user)

        if "NO_SYNC_COMMANDS" not in os.environ:
            # This cannot be done in Argon#setup_hook() as otherwise the bot crashes, because
            # the application ID has not been loaded yet at that point in the login process
            log.info("Syncing application commands with Discord")
            try:
                await bot.tree.sync(guild=SLASH_GUILD)
            except discord.HTTPException:
                log.exception("Failed to sync app commands")

        if startup_log_channel := bot.get_channel(bot.bot_config.startup_channel):
            await startup_log_channel.send(info(_("Bot started successfully.").format()))
        started.set()

    @bot.check
    async def check_basic_perms(ctx: commands.Context):
        if (
            not ctx.interaction
            and ctx.guild
            and (
                # Check that we can send messages in the context channel
                not ctx.channel.permissions_for(ctx.me).send_messages
                # ... and that a moderator hasn't timed us out
                or ctx.me.is_timed_out()
            )
        ):
            raise commands.SilentCheckFailure()
        return True

    # noinspection PyProtectedMember
    @bot.check
    async def check_access(ctx: commands.Context):
        author = ctx.interaction.user if ctx.interaction else ctx.author

        if await bot.is_owner(author):
            return True
        if await bot.is_blocked(author):
            return commands.CheckFailure(
                chat_formatting.deny(_("You have been blocked from using my commands.").format())
            )

        if ctx.guild:
            guild = await db.Guild.find_one_or_create(ctx.guild)

            if blocked := _find_command(
                bot=bot, data=guild.command_blocked_roles, command=ctx.command
            ):
                if br := next((role for role in ctx.author.roles if role.id in blocked), None):
                    raise commands.RoleBlockedCommand(br, ctx.command)

        return True

    @bot.event
    async def on_command_error(ctx: commands.Context, error: Exception):
        if (
            ctx.guild
            and not ctx.channel.permissions_for(ctx.me).send_messages
            or ctx.me.is_timed_out()
        ):
            return

        if hasattr(ctx.command, "on_error"):
            return
        # noinspection PyProtectedMember
        if (cog := ctx.cog) and cog._get_overridden_method(cog.cog_command_error) is not None:
            return
        error = getattr(error, "original", error)

        # Silence errors which we never want to send messages for before ever
        # sending them onto the error handler
        if isinstance(error, (commands.CommandNotFound, commands.SilentCheckFailure)):
            return

        await command_error(error, ctx)

    # noinspection PyProtectedMember
    @bot.after_invoke
    async def delete_delay(ctx: commands.Context):
        if (
            ctx.interaction
            or not ctx.guild
            or not ctx.channel.permissions_for(ctx.me).manage_messages
            or not ctx.message
        ):
            return
        guild = await db.Guild.find_one_or_create(ctx.guild)
        if (delay := _find_command(bot, guild.command_delete, ctx.command)) is not None:
            await ctx.message.delete(delay=delay)
        elif guild.delete_delay is not None:
            await ctx.message.delete(delay=guild.delete_delay)
