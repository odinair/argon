from __future__ import annotations

from datetime import timedelta
from typing import Literal

import discord
from beanie import Document

__all__ = ("Role", "Guild", "Channel", "Webhook")


class Role(Document):
    role_id: int
    guild_id: int
    # If this is True, this role will be re-assigned to members who gain it upon rejoining
    sticky: bool = False

    @classmethod
    async def find_one_or_create(cls, role: discord.Role) -> Role:
        doc = await cls.find_one({"role_id": role.id, "guild_id": role.guild.id})
        if not doc:
            doc = cls(guild_id=role.guild.id, role_id=role.id)
            await doc.insert()
        return doc

    class Settings:
        name = "role"
        use_cache = True
        cache_expiration_time = timedelta(minutes=30)
        cache_capacity = 1000


class Guild(Document):
    guild_id: int

    # --- Core guild settings ---

    # If set this overrides Bot.prefixes
    # Mentions are always valid prefixes regardless of the values of this list
    prefixes: list[str] | None = None

    # --- Moderation settings ---

    command_blocked_roles: dict[str, list[int]] = {}
    mute_role: int | None = None
    # See argon.cogs.overseer.config.GuildConfig#__init__ for more information on the
    # data structure of this dict
    overseer: dict = {}
    modlog_channel: int | None = None  # TODO merge into overseer?
    mod_channel: int | None = None  # currently unused
    delete_delay: int | None = None
    command_delete: dict[str, int | None] = {}
    sticker_spam: bool = False

    # --- Misc settings ---

    select_emojis: dict = {"add": "\N{HEAVY PLUS SIGN}", "remove": "\N{HEAVY MINUS SIGN}"}

    @classmethod
    async def find_one_or_create(cls, guild: discord.Guild) -> Guild:
        doc = await cls.find_one({"guild_id": guild.id})
        if not doc:
            doc = cls(guild_id=guild.id)
            await doc.insert()
        return doc

    class Settings:
        name = "guild"
        use_cache = True
        cache_expiration_time = timedelta(hours=6)
        cache_capacity = 100


class Channel(Document):
    guild_id: int
    channel_id: int

    # --- Voice channels ---

    # Grant members a given role while they're in this voice channel
    # This is mostly useful for hiding voice text channels
    vc_role: int | None = None

    @classmethod
    async def find_one_or_create(cls, channel: discord.abc.GuildChannel) -> Channel:
        doc = await cls.find_one({"guild_id": channel.guild.id, "channel_id": channel.id})
        if not doc:
            doc = cls(guild_id=channel.guild.id, channel_id=channel.id)
            await doc.insert()
        return doc

    class Settings:
        name = "channel"
        use_cache = True
        cache_expiration_time = timedelta(minutes=30)
        cache_capacity = 1000


class Webhook(Document):
    webhook_id: int
    token: str
    type: Literal[1, 2, 3]
    channel_id: int | None = None

    class Settings:
        name = "webhook"
        use_cache = True
        cache_expiration_time = timedelta(hours=12)
        cache_capacity = 100
