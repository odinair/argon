from datetime import datetime, timedelta
from typing import Literal

import discord
from beanie import Document

__all__ = ("Case", "CaseAction")
# This is an alias for easier type hinting with pydantic; case action types should
# *never* be removed from this literal, unless you know for sure they were never implemented
# and/or used.
CaseAction = Literal["ban", "hackban", "unban", "mute", "unmute", "kick", "warn"]


class Case(Document):
    # Basic data
    guild_id: int
    case_id: int
    timestamp: datetime
    action: CaseAction
    # The ID of this case's mod log channel message
    case_message: int | None = None

    # Who this case is for and the moderator who created it
    target_name: str | None = None
    target: int
    moderator_name: str | None = None
    moderator: int | None

    # Extra data
    reason: str | None = None
    until: datetime | None = None
    amended_at: datetime | None = None
    amended_by: int | None = None

    @classmethod
    async def next_case_number(cls, guild: discord.Guild | discord.Object | int) -> int:
        return (
            # I really would like to not have to use this ugly mess to find
            # the highest case ID, but it works, so good enough I guess.
            next(
                iter(
                    await cls.aggregate(
                        [
                            {"$match": {"guild_id": getattr(guild, "id", guild)}},
                            {"$group": {"_id": None, "id": {"$max": "$case_id"}}},
                        ],
                        ignore_cache=True,
                    ).to_list()
                ),
                {},
            ).get("id", 0)
            + 1
        )

    class Settings:
        # this is title case for the same reason as Alias - I simply forgot to make this lower case,
        # and I can't be bothered writing a migration to fix it.
        name = "Case"
        use_cache = True
        cache_expiration_time = timedelta(minutes=30)
        cache_capacity = 500
