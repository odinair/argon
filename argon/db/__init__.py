from .alias import *
from .bot import *
from .case import *
from .guild import *
from .message import *
from .user import *


async def setup():
    from os import environ

    from beanie import init_beanie
    from motor import motor_asyncio as motor

    client = motor.AsyncIOMotorClient(
        environ["ARGON_MONGO_URI"],
        serverSelectionTimeoutMS=int(environ.get("ARGON_MONGO_TIMEOUT", "100")),
    )
    await init_beanie(
        database=client[environ["ARGON_MONGO_DB"]],
        document_models=[
            Alias,
            Case,
            Bot,
            Guild,
            Role,
            Member,
            Channel,
            Message,
            User,
            Reminder,
            Webhook,
        ],
    )
