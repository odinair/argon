from __future__ import annotations

from datetime import datetime, timedelta
from typing import Optional

import discord
from beanie import Document
from typing_extensions import TypedDict

__all__ = ("User", "Member", "TempRole", "Reminder")


class TempRole(TypedDict):
    """Typings for timed roles"""

    id: int
    expiry: datetime
    # pycharm really doesn't like the 'type | type' style of unions in TypedDict
    added_by: Optional[int]
    reason: Optional[str]


class User(Document):
    user_id: int
    previous_names: list[str] = []
    # Blocked users are globally prevented from using any bot commands.
    blocked: bool = False

    @classmethod
    async def find_one_or_create(cls, user: discord.User | discord.Member) -> User:
        doc = await cls.find_one({"user_id": user.id})
        if not doc:
            doc = cls(user_id=user.id)
            await doc.insert()
        return doc

    class Settings:
        name = "user"
        use_cache = True
        cache_expiration_time = timedelta(minutes=30)
        cache_capacity = 2500


class Member(Document):
    guild_id: int
    user_id: int
    # Members with a personal role may modify it with `[p]myrole`
    # Such roles are also implicitly treated as sticky, even if the associated role's document
    # does not have sticky set to True.
    personal_role: int | None = None
    # Muted users implicitly have Guild#mute_role as a sticky role
    # This is only ever used if timeouts cannot be used for whatever reason
    muted: bool = False
    muted_until: datetime | None = None
    banned_until: datetime | None = None
    # If True, this member is blocked from using the bot in the current guild, but is still
    # able to use it outside that guild/in DMs.
    blocked: bool = False

    previous_nicks: list[str] = []
    sticky_roles: list[int] = []
    temp_roles: list[TempRole] = []

    @classmethod
    async def find_one_or_create(cls, member: discord.Member) -> Member:
        doc = await cls.find_one({"guild_id": member.guild.id, "user_id": member.id})
        if not doc:
            doc = cls(guild_id=member.guild.id, user_id=member.id)
            await doc.insert()
        return doc

    @classmethod
    async def find_one_or_create_in_guild(
        cls, member: discord.User, guild: discord.Guild
    ) -> Member:
        doc = await cls.find_one({"guild_id": guild.id, "user_id": member.id})
        if not doc:
            doc = cls(guild_id=guild.id, user_id=member.id)
            await doc.insert()
        return doc

    class Settings:
        name = "member"
        use_cache = True
        cache_expiration_time = timedelta(minutes=20)
        cache_capacity = 5000


class Reminder(Document):
    user_id: int
    text: str
    due: datetime
    created_at: datetime
    context_message: str | None = None

    class Settings:
        name = "reminder"
