from __future__ import annotations

from typing import Literal, Optional

import discord
from beanie import Document
from typing_extensions import TypedDict

__all__ = ("Message", "SelfAssignRole", "ButtonStyleType")
ButtonStyleType = Literal[
    "blurple", "grey", "gray", "green", "red", "primary", "secondary", "success", "danger"
]


class SelectButton(TypedDict, total=False):
    message: str
    emoji: Optional[str]
    style: ButtonStyleType


# noinspection PyTypedDict
class SelfAssignRole(TypedDict, total=False):
    id: int
    style: ButtonStyleType
    label: str
    emoji: Optional[str]
    # None auto-sorts when the view is recreated
    # This does nothing when used on select-type messages
    row: Optional[Literal[0, 1, 2, 3, 4]]
    # Determines if members can add and/or remove this role from themselves
    type: Literal["add", "remove", "both"]
    # Only applicable in the context of select-type messages
    # May not be present entirely
    description: Optional[str]


class Message(Document):
    # Can be None in the context of DMs, however unlikely it may be
    guild_id: int | None
    channel_id: int
    message_id: int

    # --- Self-assignable roles ---

    role_type: Literal["button", "select"] = "button"
    select_button: SelectButton = SelectButton(
        message="Click to assign roles", emoji=None, style="grey"
    )
    roles: list[SelfAssignRole] = []
    mutually_exclusive: bool = False
    # Support for this is very hacky right now, and is really only supported through evals.
    # If this is applicable, this value will be the ID of the webhook used to send this message.
    # The webhook *must* have an entry in db.Webhook for this to function.
    is_webhook: int | None = None

    @classmethod
    async def find_one_or_create(cls, message: discord.Message | discord.PartialMessage) -> Message:
        doc = await cls.find_one({"channel_id": message.channel.id, "message_id": message.id})
        if not doc:
            doc = cls(
                guild_id=message.guild and message.guild.id or None,
                channel_id=message.channel.id,
                message_id=message.id,
            )
            await doc.insert()
        return doc

    class Settings:
        name = "message"
        # TODO bypass caching due to the endless issues that selfassign has with it
        # use_cache = True
        # cache_expiration_time = timedelta(minutes=10)
        # cache_capacity = 1500
