import logging
from datetime import timedelta

from argon import commands
from argon.errors.core import ErrorDispatch
from argon.translations import Humanize, Translator
from argon.utils import chat_formatting

log = logging.getLogger("argon.events")
__all__ = ("command_error",)
_ = Translator()
command_error = ErrorDispatch()


# noinspection PyUnusedLocal
@command_error.register(commands.DisabledCommand)
async def disabled_command(error: commands.DisabledCommand, ctx: commands.Context):
    await ctx.send(
        _("`{command}` is currently disabled.").format(command=ctx.command.qualified_name),
        ephemeral=True,
    )


# noinspection PyUnusedLocal
@command_error.register(commands.NoPrivateMessage)
async def no_dms(error: commands.NoPrivateMessage, ctx: commands.Context):
    await ctx.send(
        chat_formatting.error(_("This command cannot be used in DMs.").format()), ephemeral=True
    )


@command_error.register(commands.CheckFailure)
async def check_failure(error, ctx: commands.Context):
    if isinstance(error, commands.MissingPermissions):
        await ctx.send(
            chat_formatting.warning(
                _(
                    "You are missing {count, plural, one {a required permission}"
                    " other {required permissions}} to use this command: {permissions}"
                ).format(
                    count=len(error.missing_permissions),
                    permissions=Humanize(
                        [
                            f"__{chat_formatting.translate_permission(x)}__"
                            for x in error.missing_permissions
                        ]
                    ),
                )
            ),
            ephemeral=True,
        )
    elif isinstance(error, commands.BotMissingPermissions):
        await ctx.send(
            chat_formatting.warning(
                _(
                    "I'm missing {count, plural, one {a required permission}"
                    " other {required permissions}} to use this command: {permissions}"
                ).format(
                    count=len(error.missing_permissions),
                    permissions=Humanize(
                        [
                            f"__{chat_formatting.translate_permission(x)}__"
                            for x in error.missing_permissions
                        ]
                    ),
                )
            ),
            ephemeral=True,
        )
    elif isinstance(error, commands.NotOwner):
        await ctx.send(
            chat_formatting.deny(_("The maze is not meant for you.").format()), ephemeral=True
        )

    # If all else fails...
    elif isinstance(error, commands.EmbedCheckFailure):
        await ctx.send(embed=error.message, ephemeral=True)
    elif isinstance(error, commands.CheckFailure):
        if error.args:
            await ctx.send(error.args[0], ephemeral=True)
        elif ctx.interaction:
            await ctx.send(_("One or more checks on this command failed.").format(), ephemeral=True)
        # If there's not a message provided for this check failure and this isn't an interaction,
        # just let it silently fail


@command_error.register(commands.UserInputError)
async def generic_input_errors(error, ctx: commands.Context):
    from .user_input import user_input

    await user_input(error, ctx)


@command_error.register(commands.CommandOnCooldown)
async def cooldown(error: commands.CommandOnCooldown, ctx: commands.Context):
    await ctx.send(
        chat_formatting.warning(
            _("This command is on cooldown; try again in {delta}").format(
                delta=Humanize(timedelta(seconds=error.retry_after))
            )
        ),
        ephemeral=True,
    )
