from argon import commands
from argon.errors import ErrorDispatch
from argon.translations import Translator
from argon.utils import chat_formatting


class UserInputDispatch(ErrorDispatch):
    @staticmethod
    async def _default_callback(exc: commands.UserInputError, ctx: commands.Context):
        if exc.args and isinstance(exc.args[0], str):
            await ctx.send(exc.args[0], ephemeral=True)
        elif ctx.interaction:
            await ctx.send(
                _("One of the arguments you provided is invalid.").format(), ephemeral=True
            )
        else:
            await ctx.send_help(ctx.command)


user_input = UserInputDispatch()
_ = Translator()
__all__ = ("user_input",)


@user_input.register(commands.MissingRequiredArgument)
async def missing_argument(error: commands.MissingRequiredArgument, ctx: commands.Context):
    await ctx.send(
        chat_formatting.warning(
            _("Required argument `{argument}` is missing").format(argument=error.param.name)
        ),
        ephemeral=True,
    )
    if not ctx.interaction:
        await ctx.send_help(ctx.command)


@user_input.register(commands.TooManyArguments)
async def too_many_args(__, ctx: commands.Context):
    await ctx.send(
        chat_formatting.warning(
            _("You specified more arguments than this command expects to receive.").format()
        ),
        ephemeral=True,
    )
    if not ctx.interaction:
        await ctx.send_help(ctx.command)


@user_input.register(commands.ObjectNotFound)
async def invalid_object(error: commands.ObjectNotFound, ctx: commands.Context):
    await ctx.send(_("`{value}` is not a valid mention nor ID").format(value=error.argument))


@user_input.register(commands.ChannelNotFound)
@user_input.register(commands.ThreadNotFound)
@user_input.register(commands.ChannelNotReadable)
async def invalid_channel(
    error: commands.ChannelNotFound | commands.ChannelNotReadable | commands.ThreadNotFound,
    ctx: commands.Context,
):
    if isinstance(error, commands.ChannelNotReadable):
        await ctx.send(
            _("I do not have read messages permission in {channel}.").format(channel=error.argument)
        )
        return
    await ctx.send(
        _("Cannot convert `{value}` to a {type, select, thread {thread} other {channel}}").format(
            value=error.argument,
            type="channel" if isinstance(error, commands.ChannelNotFound) else "thread",
        )
    )


@user_input.register(commands.PartialEmojiConversionFailure)
@user_input.register(commands.EmojiNotFound)
async def emoji_not_found(
    error: commands.PartialEmojiConversionFailure | commands.EmojiNotFound, ctx: commands.Context
):
    await ctx.send(_("`{value}` is not a valid emoji").format(value=error.argument))


@user_input.register(commands.RoleNotFound)
async def role_conversion_failure(error: commands.RoleNotFound, ctx: commands.Context):
    await ctx.send(_("Cannot convert `{value}` to a role").format(value=error.argument))


@user_input.register(commands.UserNotFound)
@user_input.register(commands.MemberNotFound)
async def member_not_found(
    error: commands.UserNotFound | commands.MemberNotFound, ctx: commands.Context
):
    await ctx.send(
        _("Cannot convert `{value}` to a {type, select, member {member} user {user}}").format(
            value=error.argument,
            type="member" if isinstance(error, commands.MemberNotFound) else "user",
        )
    )


@user_input.register(commands.RangeError)
async def range_conversion_failure(error: commands.RangeError, ctx: commands.Context):
    if error.minimum is None:
        await ctx.send(
            _("Expected at most `{max}`, got `{value}`").format(
                max=error.maximum, value=error.value
            ),
            ephemeral=True,
        )
    elif error.maximum is None:
        await ctx.send(
            _("Expected at least `{min}`, got `{value}`").format(
                min=error.minimum, value=error.value
            ),
            ephemeral=True,
        )
    else:
        await ctx.send(
            _("Expected a value within the range of `{min}` to `{max}`, got `{value}`").format(
                min=error.minimum, max=error.maximum, value=error.value
            ),
            ephemeral=True,
        )
