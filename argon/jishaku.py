import itertools
import logging
import traceback

from discord.ext import commands
from jishaku import Feature
from jishaku.features.filesystem import FilesystemFeature
from jishaku.features.guild import GuildFeature
from jishaku.features.invocation import InvocationFeature
from jishaku.features.management import ManagementFeature
from jishaku.features.python import PythonFeature
from jishaku.features.root_command import RootCommand
from jishaku.features.shell import ShellFeature
from jishaku.modules import ExtensionConverter
from jishaku.paginators import WrappedPaginator

from argon.bot import Argon
from argon.utils import SLASH_GUILD

log = logging.getLogger("argon.jishaku")


class Jishaku(
    GuildFeature,
    FilesystemFeature,
    InvocationFeature,
    ShellFeature,
    PythonFeature,
    ManagementFeature,
    RootCommand,
):
    bot: Argon

    @Feature.Command(parent="jsk", name="sync")
    async def jsk_sync(self, ctx: commands.Context):
        """
        Sync application commands with Discord

        The bot will automatically sync commands upon startup, but this may be
        preferred if you don't want to restart after updating.
        """
        log.info("%s invoked manual application command sync", ctx.author)
        emoji = "\N{CLOCKWISE RIGHTWARDS AND LEFTWARDS OPEN CIRCLE ARROWS}"
        await self.bot.tree.sync(guild=SLASH_GUILD)
        if SLASH_GUILD is not None:
            await self.bot.tree.sync()
        if SLASH_GUILD:
            await ctx.send(
                f"{emoji} Synced application commands with Discord in guild"
                f" **`{self.bot.get_guild(SLASH_GUILD.id)!s}`**"
            )
        else:
            await ctx.send(
                f"{emoji} Synced global application commands with Discord; this may take"
                f" an hour to take effect due to Discord limitations."
            )

    @Feature.Command(parent="jsk", name="load", aliases=["reload"])
    async def jsk_load(self, ctx: commands.Context, *extensions: ExtensionConverter):
        """
        Loads or reloads the given extension names.

        Reports any extensions that failed to load.
        """

        paginator = WrappedPaginator(prefix="", suffix="")

        # 'jsk reload' on its own just reloads jishaku
        if ctx.invoked_with == "reload" and not extensions:
            extensions = [["argon.jishaku"]]

        for extension in itertools.chain(*extensions):
            if "." not in extension:
                extension = f"argon.cogs.{extension}"
            elif extension.startswith("."):
                extension = extension.lstrip(".")

            method, icon = (
                (
                    self.bot.reload_extension,
                    "\N{CLOCKWISE RIGHTWARDS AND LEFTWARDS OPEN CIRCLE ARROWS}",
                )
                if extension in self.bot.extensions
                else (self.bot.load_extension, "\N{INBOX TRAY}")
            )

            try:
                await method(extension)
            except Exception as exc:  # pylint: disable=broad-except
                traceback_data = "".join(
                    traceback.format_exception(type(exc), exc, exc.__traceback__, 1)
                )

                paginator.add_line(
                    f"{icon}\N{WARNING SIGN} `{extension}`\n```py\n{traceback_data}\n```",
                    empty=True,
                )
            else:
                paginator.add_line(f"{icon} `{extension}`", empty=True)

        for page in paginator.pages:
            await ctx.send(page)


async def setup(bot: commands.Bot):
    await bot.add_cog(Jishaku(bot=bot))
