from __future__ import annotations

import logging
from pathlib import Path

from babel.messages.catalog import Catalog
from babel.messages.pofile import read_po

from argon.translations.formatter import MessageFormatter

__all__ = ("Locale", "Message")
# /argon/translations/locale.py -> /argon/locales/
LOCALES_DIR = Path(__file__).parent.parent / "locales"
log = logging.getLogger("argon.translations")


class Locale:
    """A locale containing a Catalog of messages

    .. important::
        Locales do not make any attempt to ensure that the messages you retrieve
        from them actually exist in the locale file; instead, they allow this to happen
        on the assumption that this will be done from fallback locales (such as one hardcoded
        for the language you write the source strings in).

        If you want to ensure that a string exists in a given locale, you'll want
        to make use of :attr:`catalog` like so::

            >>> locale = Locale('en_US')
            >>> bool(locale.catalog.get('Hello, world!'))
            True
            >>> bool(locale.catalog.get('Does not exist'))
            False

    Example
    --------
    >>> locale = Locale('en_US')
    >>> locale['Hello, world!']
    <Message locale='en_US' message='Hello, world!'>
    """

    __slots__ = ("name", "_cache", "_formatter", "_catalog")

    def __init__(self, locale: str, /):
        self.name = locale
        self._cache: dict[str, Message] = {}
        self._formatter = MessageFormatter(locale)
        self._catalog = ...

    def __str__(self):
        return self.name

    def __getitem__(self, message: str) -> Message:
        if message in self._cache:
            return self._cache[message]
        localized = getattr(self.catalog.get(message), "string", None) or message

        self._cache[message] = message = Message(
            ast=self._formatter.parser.parse(localized),
            original=localized,
            locale=self,
        )
        return message

    @property
    def catalog(self) -> Catalog:
        """:class:`babel.messages.Catalog`: The underlying Catalog used for this locale"""
        if self._catalog is ...:
            log.debug("Loading .po file for locale %s", self.name)
            try:
                with open(LOCALES_DIR / f"{self.name}.po") as f:
                    self._catalog = read_po(f, locale=self.name)
            except FileNotFoundError:
                log.debug("%s has no file on disk, creating empty catalog", self.name)
                self._catalog = Catalog(locale=self.name)
            except IOError as e:
                log.error("Failed to load messages for locale %s", self.name, exc_info=e)
                self._catalog = Catalog(locale=self.name)
        return self._catalog


class Message:
    """A thin wrapper around locale messages

    The main purpose this serves is to provide context as to which locale the
    message originates from.
    """

    __slots__ = ("ast", "original", "_locale")

    def __init__(self, ast: list[str | dict], original: str, locale: Locale):
        self.ast = ast
        self.original = original
        self._locale = locale

    def __repr__(self):
        return f"<Message locale={self.locale!r} message={self.original!r}>"

    def __str__(self):
        return self.format()

    @property
    def locale(self) -> str:
        return self._locale.name

    def format(self, **kwargs) -> str:
        # noinspection PyProtectedMember
        return self._locale._formatter.format_ast(self.ast, kwargs, self.original)

    __call__ = format
