import datetime
import inspect
from functools import singledispatch, wraps
from inspect import getmodule
from typing import Any, Callable

from babel import Locale as _bLocale
from babel.dates import format_date, format_time, format_timedelta
from babel.lists import format_list
from babel.numbers import format_decimal

__all__ = ("Humanize", "get_babel_locale")


def get_babel_locale(locale: str | None = None) -> _bLocale:
    return _bLocale.parse(locale or "en_US")


def _babel_locale(func):
    @wraps(func)
    def wrapper(val, args: tuple, kwargs: dict):
        kwargs["locale"] = get_babel_locale(kwargs.get("locale"))
        return func(val, args, kwargs)

    return wrapper


@singledispatch
def humanize(val: Callable[..., str] | Any, args: tuple, kwargs: dict) -> str:
    if callable(val):
        module = getmodule(val)
        if module and module.__name__.split(".")[0] == "babel":
            kwargs["locale"] = get_babel_locale(kwargs.get("locale"))
        else:
            kwargs.setdefault("locale", "en_US")
        return val(*args, **kwargs)
    raise TypeError(f"{type(val).__name__!r} is not a recognized type nor a callable")


@humanize.register(datetime.timedelta)
@_babel_locale
def _timedelta(delta: datetime.timedelta, args: tuple, kwargs: dict) -> str:
    return format_timedelta(delta, *args, **kwargs)


@humanize.register(datetime.datetime)
def _datetime(dt: datetime.datetime, args: tuple, kwargs: dict) -> str:
    from argon.utils.chat_formatting import timestamp

    kwargs = kwargs.copy()
    kwargs.pop("locale", None)
    return timestamp(dt, *args, **kwargs)


@humanize.register(datetime.date)
@_babel_locale
def _date(date: datetime.date, args: tuple, kwargs: dict) -> str:
    return format_date(date, *args, **kwargs)


@humanize.register(datetime.time)
def _time(time: datetime.time, args: tuple, kwargs: dict) -> str:
    return format_time(time, *args, **kwargs)


@humanize.register(list)
@humanize.register(set)
@humanize.register(tuple)
def _sequence(seq: list | set | tuple, args: tuple, kwargs: dict) -> str:
    return format_list(list(seq), *args, **kwargs)


@humanize.register(int)
@humanize.register(float)
def _decimal(num: int | float, args: tuple, kwargs: dict) -> str:
    return format_decimal(num, *args, **kwargs)


class Humanize:
    """Special placeholder class for use in translations

    This class could be thought of as a :class:`functools.partial`-like class, meant exclusively
    for use in a translation context.

    Format argument values with this class type are special-cased by :class:`Translator`,
    and will instead be rendered in the output string with the locale currently in use.

    The following types are supported:

    * :class:`datetime.datetime` (uses ``chat_formatting.timestamp()``)
    * :class:`datetime.date` (uses `Babel`_)
    * :class:`datetime.time` (uses `Babel`_)
    * :class:`datetime.timedelta` (uses `Babel`_)
    * :class:`list` (uses `Babel`_)
    * :class:`set` (uses `Babel`_)
    * :class:`tuple` (uses `Babel`_)
    * :class:`int` (uses `Babel`_)
    * :class:`float` (uses `Babel`_)

    You may also specify arbitrary callables which accept both the arguments you specify
    to the ``Humanize`` class and a ``locale`` keyword argument; these callables must not be async.
    This effectively allows for making ad-hoc transformers without setting up
    a transformer to call on every translation.

    Use of any other types will result in a :class:`TypeError` being raised.

    .. _Babel: http://babel.pocoo.org/en/latest/

    Example
    --------
    >>> from argon.translations import Translator, Humanize
    >>> from datetime import datetime
    >>> _ = Translator()
    >>> dt = datetime.now()
    >>> _("Today is {date}, and the time is currently {time}").format(
    ...     date=Humanize(dt.date()), time=Humanize(dt.time())
    ... )
    "Today is <t:...:D>, and the time is currently <t:...:t>"

    Raises
    ------
    TypeError
        Raised if the given value either isn't of a type natively supported or is an async callable
    """

    __slots__ = ("value", "args", "kwargs")

    def __init__(self, value, *args, **kwargs):
        # Fail early if the given value is an unrecognized type
        if type(value) not in humanize.registry and not callable(value):
            raise TypeError(f"{type(value).__qualname__!r} is neither a known type or callable")
        if callable(value) and inspect.iscoroutinefunction(value):
            raise TypeError("callables given to Humanize must not be async")

        self.value = value
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return self()

    def __call__(self, *_, **kwargs):
        return humanize(self.value, self.args, {**self.kwargs, **kwargs})
