from __future__ import annotations

import warnings
from datetime import datetime, timedelta
from typing import Callable

from babel.dates import format_date, format_time, format_timedelta
from babel.localedata import load as babel_load_locale
from babel.numbers import format_currency, format_decimal
from pyicumessageformat import Parser

from argon.utils import MISSING

_FORMATTERS: dict[str, Callable] = {}
# subnumeric allows for select messages to lack an 'other' submessage,
# but they will raise an error if the requested submessage doesn't
# exist without 'other'.
PARSER = Parser({"include_indices": True, "require_other": "subnumeric"})
DISCORD_TIMESTAMPS = {"R", "F", "T", "D", "f", "t", "d"}
TIMESTAMP_FORMATS = {
    "relative": "R",
    "date": "d",
    "date:short": "d",
    "date:long": "D",
    "time": "t",
    "time:short": "t",
    "time:long": "T",
    "default": "f",
    "datetime": "f",
    "datetime:short": "f",
    "datetime:long": "F",
}


def formatter(func: Callable[[dict, dict, str], str]):
    _FORMATTERS[func.__name__] = func
    return func


class FormatException(Exception):
    def __init__(self, message: str, format_args: dict, error_message: str):
        super().__init__(
            f"Encountered an exception while parsing {message!r}\n"
            f"Format arguments used: {format_args!r}\n\n"
            f"{error_message}"
        )
        self.message = message
        self.format_args = format_args


# noinspection PyUnusedLocal
class MessageFormatter:
    """Message formatter based on pyicumessageformat"""

    __slots__ = ("locale_name", "locale")

    def __init__(self, locale: str):
        self.locale_name = locale.replace("-", "_")
        self.locale = babel_load_locale(locale.replace("-", "_"))

    @property
    def parser(self) -> Parser:
        return PARSER

    def format(self, message: str, format_args: dict) -> str:
        """Parse and format the given message"""
        return self.format_ast(PARSER.parse(message), format_args, message)

    def format_ast(self, message: list[dict | str], format_args: dict, original_string: str) -> str:
        """Format the AST generated from pyicumessageformat

        Arguments
        ---------
        message: list[dict | str]
            A list of either message components or plain strings from ``original_string``
        format_args: dict
            The dict of format arguments to use for this string
        original_string: str
            The original string; this should be the same translated version as used to generate
            the list of ``message`` components

        Returns
        -------
        str
            The formatted string

        Raises
        ------
        KeyError
            Raised if a message component is for a format argument that is not present
            in ``format_args``
        """
        output = ""

        try:
            for val in message:
                if isinstance(val, str):
                    output += val
                elif val["name"] not in format_args:
                    raise KeyError(val["name"])
                elif "type" not in val:
                    # bare format argument, i.e '{value}'
                    output += str(format_args[val["name"]])
                elif func := _FORMATTERS.get(val["type"]):
                    output += func(self, val, format_args, original_string)
                else:
                    warnings.warn(f"{val['type']} is not a recognized format type", RuntimeWarning)
                    if "start" in val and "end" in val:
                        output += original_string[val["start"] : val["end"]]
                    else:
                        output += f"{{{val['name']}}}"
        except Exception as e:
            raise FormatException(
                original_string, format_args, f"{e.__class__.__name__!s}: {e!s}"
            ) from e

        return output

    @formatter
    def plural(self, message: dict, format_args: dict, original_string: str) -> str:
        # '{messages, plural, one {One message} other {# messages}}'
        # [{'end': 56,
        #   'name': 'messages',
        #   'offset': 0,
        #   'options': {'one': ['One message'],
        #               'other': [{'end': 45,
        #                          'hash': True,
        #                          'name': 'messages',
        #                          'start': 44,
        #                          'type': 'number'},
        #                         ' messages']},
        #   'start': 0,
        #   'type': 'plural'}]

        # I have no clue if this is actually how offset works, but it seems like
        # it's consistent with the JavaScript implementation docs, so good enough, I guess.
        offset = float(message.get("offset", 0))
        value: int | float = format_args[message["name"]] + offset
        plural_type = self.locale["plural_form"](value)
        option: list[str | dict] = message["options"].get(plural_type, message["options"]["other"])
        return self.format_ast(option, format_args, original_string)

    @formatter
    def number(self, message: dict, format_args: dict, original_string: str) -> str:
        # '{N, number, integer}'
        # {'name': 'N', 'type': 'number', 'format': 'integer'}

        # default to bare numbers without commas from '#' in plural messages
        default_format = "bare" if message.get("hash") else "integer"
        # 'currency:EUR' -> ['currency', 'EUR']
        nf = message.get("format", default_format).split(":")

        numformat = nf[0]
        extra = ":".join(nf[1:])
        number = format_args[message["name"]]
        if numformat == "bare":
            return str(number)
        if numformat == "integer":
            return format_decimal(number, locale=self.locale_name)
        if numformat == "percentage":
            return f"{float(number):.{extra or '0'}f}"
        if numformat == "currency":
            return format_currency(number, extra or "USD", locale=self.locale_name)
        raise TypeError(f"{numformat} is not a valid number format")

    @formatter
    def select(self, message: dict, format_args: dict, original_string: str) -> str:
        # '{V, select, apple {Apple} orange {Orange} other {{V}}}'
        # {'end': 54,
        #  'name': 'V',
        #  'options': {'apple': ['Apple'],
        #              'orange': ['Orange'],
        #              'other': [{'end': 52, 'name': 'V', 'start': 49}]},
        #  'start': 0,
        #  'type': 'select'}

        value = format_args[message["name"]]
        default = message["options"].get("other", MISSING)
        option: list[str | dict] = message["options"].get(value, default)
        if default is MISSING:
            raise ValueError(
                f"{value!r} is missing from the given string, and 'other' was not provided"
            )
        return self.format_ast(option, format_args, original_string)

    # noinspection PyShadowingBuiltins
    @formatter
    def timestamp(self, message: dict, format_args: dict, original_string: str) -> str:
        """Custom message type, returns a Discord timestamp string"""
        # {DT, timestamp, relative}
        # {'name': 'DT', 'type': 'timestamp', 'format': 'relative'}
        from argon.utils.chat_formatting import timestamp

        format = message.get("format")
        # Provide human-friendly aliases for format types, but also allow using
        # Discord format types where applicable
        if format is not None:
            if len(format) > 1:
                format = TIMESTAMP_FORMATS[format]
            elif format not in DISCORD_TIMESTAMPS:
                raise ValueError(
                    f"{format} is not a Discord-compatible timestamp format; valid formats are"
                    f" {DISCORD_TIMESTAMPS!r} or {set(TIMESTAMP_FORMATS.keys())}"
                )

        value = format_args[message["name"]]
        if isinstance(value, int | float):
            value = datetime.utcfromtimestamp(value)
        return timestamp(value, format)

    @formatter
    def time(self, message: dict, format_args: dict, original_string: str) -> str:
        # {'name': 'T', 'type': 'time', 'format': 'full'}

        value = format_args[message["name"]]
        return format_time(value, locale=self.locale_name, format=message.get("format"))

    @formatter
    def date(self, message: dict, format_args: dict, original_string: str) -> str:
        # {'name': 'D', 'type': 'date', 'format': 'full'}

        value = format_args[message["name"]]
        return format_date(value, locale=self.locale_name, format=message.get("format"))

    @formatter
    def duration(self, message: dict, format_args: dict, original_string: str) -> str:
        # {'name': 'D', 'type': 'duration', 'format': 'full'}

        value: int | float | timedelta = format_args[message["name"]]
        if isinstance(value, int | float):
            value = timedelta(seconds=value)
        return format_timedelta(value, locale=self.locale_name, format=message.get("format"))
