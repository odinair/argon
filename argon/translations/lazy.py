from __future__ import annotations

__all__ = ('LazyStr', 'PythonLazyStr')


class LazyStr:
    """A wrapper class representing a lazily formatted string

    It's recommended that you get this from calling an instantiated :class:`Translator`, but if
    you wish you may also construct an instance of this class on your own.

    This attempts to mock :class:`str` as closely as possible, but cannot fool
    ``isinstance()`` checks or similar forms of type checking.

    An important caveat to this however, is that ``.format()`` does *not* retain
    the same type signature, but instead only accepts keyword arguments.
    """

    __slots__ = ("__translator", "__text", "__args")

    def __init__(self, translator, text: str, args: dict):
        self.__translator = translator
        self.__text = text
        self.__args = args

    def __repr__(self):
        return (
            f"<LazyStr translator={self.__translator!r} text={self.__text!r} args={self.__args!r}>"
        )

    def __str__(self) -> str:
        return self()

    def __call__(self, **args) -> str:
        return self.__translator(self.__text, self.__args | args)

    def format(self, **args) -> str:
        """Run the current LazyStr through the attached translator with the given format args

        .. important::
            This does **not** retain the same type signature as :meth:`str.format`, but instead
            only accepts the use of keyword arguments for formatting.

        Example
        -------
        >>> from argon.translations import Translator
        >>> _ = Translator()
        >>> _('Hello {name}').format(name='world')
        'Hello world'
        """
        return self(**args)

    def lazy_format(self, **args) -> LazyStr:
        """Return a new LazyStr with the given arguments

        Example
        -------
        >>> from argon.translations import Translator
        >>> _ = Translator()
        >>> _('Hello {name}').lazy_format(name='world')
        LazyStr(translator=... text='Hello {name}' args={'name': 'world'})
        """
        return LazyStr(translator=self.__translator, text=self.__text, args=self.__args | args)


class PythonLazyStr(LazyStr):
    """Lazy string implementation which simply returns a string using built-in Python formatting."""

    def __init__(self, string: str):
        super().__init__(lambda s, a: s.format(**a), string, {})


# Add attributes to more properly mock str
for _strk in dir(str):
    if _strk not in dir(LazyStr):
        # This lambda is needed as otherwise `_strk` will be of the last value in `dir(str)`,
        # which wouldn't produce the expected results.
        (
            lambda k: setattr(
                LazyStr, k, lambda self, *args, **kwargs: getattr(self(), k)(*args, **kwargs)
            )
        )(_strk)
