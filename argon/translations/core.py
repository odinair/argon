from __future__ import annotations

import functools
import logging
from contextvars import ContextVar, copy_context
from functools import wraps
from typing import Any, Callable, DefaultDict, Sequence, Type, TypeVar, Final

import discord
from discord import app_commands

from argon.translations.humanize import Humanize
from argon.translations.lazy import LazyStr
from argon.translations.locale import Locale, Message
from argon.utils import deduplicate, keydefaultdict

__all__ = ("Translator", "set_locale", "LOCALE_CTX", "cog_i18n")
log = logging.getLogger("argon.translations")
LOCALES: DefaultDict[str, Locale] = keydefaultdict(lambda locale: Locale(locale.replace("-", "_")))
LOCALE: str = "en_US"
SOURCE_LOCALE: Final[str] = "en_US"
LOCALE_CTX: ContextVar[str | Sequence[str]] = ContextVar("locale", default=...)
AppCommandT = TypeVar(
    "AppCommandT",
    Callable,
    app_commands.Command,
    app_commands.ContextMenu,
    app_commands.Group,
    Type[app_commands.Group],
)


@functools.lru_cache()
def get_available_locales() -> set[str]:
    from os import listdir

    # noinspection PyProtectedMember
    from .locale import LOCALES_DIR

    return {x.rstrip(".po") for x in listdir(LOCALES_DIR) if x.endswith(".po")}


def set_locale(locale: str) -> None:
    """Set the global locale

    .. note:: To set the context locale, use ``LOCALE_CTX.set()`` instead.
    """
    global LOCALE
    log.debug("Locale set to %s", locale)
    LOCALE = locale


def get_locales() -> list[str]:
    """Get all configured locales in order of priority"""
    locales = [LOCALE]
    if (context_locale := LOCALE_CTX.get()) is not ...:
        # str is a Sequence, but it's not a sequence we want to be iterating each character of
        if isinstance(context_locale, Sequence) and not isinstance(context_locale, str):
            locales = [*context_locale, LOCALE]
        else:
            locales.insert(0, context_locale)
    return deduplicate(locales)


def _default_transformer(args: dict, locale: str, /) -> dict:
    update = {}
    for k, v in args.items():
        if isinstance(
            v,
            (
                discord.abc.User,
                discord.abc.GuildChannel,
                discord.Role,
                discord.Emoji,
                discord.Thread,
            ),
        ):
            update[k] = v.mention
        elif isinstance(v, discord.Guild):
            update[k] = v.name
        elif isinstance(v, discord.Object):
            update[k] = f"`{v.id}`"
        elif isinstance(v, Humanize):
            update[k] = v(locale=locale)
        elif isinstance(v, LazyStr):
            update[k] = str(v)
        elif v is None:
            update[k] = "None"
    return update


class Translator:
    """Common translation utility based on the ICU message format"""

    __slots__ = ("transformers",)

    # The *_ is here for legacy purposes; this previously required a 'path' argument,
    # but this is no longer needed with how this is designed now.
    def __init__(self, *_, transformers: list[Callable[[dict, str], dict]] = None):
        self.transformers: list[Callable[[dict, str], dict]] = transformers or []
        if _default_transformer not in self.transformers:
            self.transformers.append(_default_transformer)

    def __call__(self, text: str, /) -> LazyStr:
        return LazyStr(translator=self.translate, text=text, args={})

    def __getitem__(self, item) -> Message:
        # The way this works, in short:
        #   - If applicable, the context locale is tried first, and if it has the requested message,
        #     then that message is returned;
        #   - If not, the global bot locale is tried next;
        #   - Finally, if all else fails, the source locale (en_US) is returned.
        locales = [*get_locales()]
        # ensure that the source locale is always in the locale list
        if SOURCE_LOCALE not in locales:
            locales.append(SOURCE_LOCALE)
        for idx, locale in enumerate(locales, start=1):
            locale = LOCALES[locale]
            # If the message exists, or this is the last locale in the list, return the string
            if locale.catalog.get(item) or idx == len(locales):
                return locale[item]

    @property
    def locale(self) -> str:
        if (context_locale := LOCALE_CTX.get()) is not ...:
            return context_locale
        return LOCALE

    def translate(self, text: str, args: dict) -> str:
        """Return a given string as its translated and formatted output

        Arguments
        ----------
        text: str
            The original untranslated string
        args: dict
            The arguments to use when formatting the translated text
        """
        message = self[text]
        for transformer in self.transformers:
            args |= transformer(args, message.locale)
        return message.format(**args)

    def transformer(self, transformer: Callable[[dict], dict]):
        """Decorator for adding a function as a formatting argument transformer"""
        self.transformers.insert(0, transformer)

    # noinspection PyMethodMayBeStatic
    def humanize(self, to_humanize, *args, **kwargs) -> str:
        """Convenience alias for using Humanize in a one-off context"""
        return Humanize(to_humanize, *args, **kwargs)()

    @staticmethod
    def args(**args):
        """Decorate a command or cog with format arguments

        .. important::
            This must be placed above - or called after - ``@commands.command`` or
            ``@commands.group``.

        Example
        --------
        >>> _ = Translator()
        >>> @_.args(name="world")
        ... @commands.command()
        ... async def my_command(ctx):
        ...     '''Hello {name}!'''
        ...     ...
        """
        from argon import commands

        def decorator(cog_or_command: Type[commands.Cog] | commands.Command | commands.Group):
            # Type[Cog] would return True, so we have to make sure that this isn't a type
            if type(cog_or_command) is not type and isinstance(cog_or_command, Callable):
                raise TypeError(
                    "Translator.args() decorator must be placed above your command"
                    " or group decorator"
                )
            cog_or_command.translate_args = args
            return cog_or_command

        return decorator

    def cog(self):
        """Attach a translator to a cog and its command docstrings"""
        from argon import commands

        def decorator(cog: Type[commands.Cog]) -> Type[commands.Cog]:
            log.debug("Attaching translator to cog %r", cog)
            cog.translator = self.translate
            for app_command in cog.__cog_app_commands__:
                log.debug("Attaching to application command %r", app_command)
                self.interaction(app_command)
            for c in cog.__dict__.values():
                # noinspection PyProtectedMember
                if not isinstance(c, commands._ArgonCommandMixin):
                    continue
                log.debug("Attaching to message command %r", c)
                c.translator = self.translate
            return cog

        return decorator

    @classmethod
    def interaction(cls, command: AppCommandT) -> AppCommandT:
        """Utility decorator for loading the locale from interactions for translations

        This is primarily intended for use with application commands, but can also be used
        in some other interaction-related contexts with callables.
        """
        if (
            # Type[Group]
            (type(command) is type and issubclass(command, app_commands.Group))
            # Group
            or isinstance(command, app_commands.Group)
        ):
            log.debug("Attaching to commands in group %r", command)
            for cmd in command.__discord_app_commands_group_children__:
                log.debug("Attaching to command %r", cmd)
                cls.interaction(cmd)
            return command

        func: Callable[..., Any] = ...

        async def wrapper(*args, **kwargs):
            from argon import commands

            log.debug("Running wrapper around function %r", func)
            all_args = (*args, *kwargs.values())
            locale = ...
            if interaction := next(
                (x for x in all_args if isinstance(x, discord.Interaction)), None
            ):
                locale = interaction.locale
            elif ctx := next((x for x in all_args if isinstance(x, commands.Context)), None):
                locale = ctx.interaction.locale if ctx.interaction else ctx.guild.preferred_locale
            context = copy_context()

            async def in_context():
                if locale and locale is not ...:
                    log.debug("Setting context locale to %r", locale)
                    LOCALE_CTX.set(str(locale).replace("-", "_"))
                return await func(*args, **kwargs)

            return await context.run(in_context)

        if isinstance(command, app_commands.Command | app_commands.ContextMenu):
            func = command.callback
            wrapper = wraps(func)(wrapper)
            command._callback = wrapper
            return command

        func = command
        wrapper = wraps(func)(wrapper)
        return wrapper


def cog_i18n(translator: Translator):
    """Alias to _.cog()

    This is provided so redgettext can properly get cog docstrings.
    """
    return translator.cog()
