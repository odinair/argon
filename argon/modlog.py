"""Moderation logging API

This is vaguely modeled after the modlog API included in Red.
"""

from __future__ import annotations

import asyncio
from collections import defaultdict
from datetime import datetime, timezone
from typing import DefaultDict, Final

import discord

from argon import db
from argon._shared import _
from argon.translations import Humanize
from argon.translations.lazy import LazyStr

_LOCKS: DefaultDict[discord.Guild, asyncio.Lock] = defaultdict(lambda: asyncio.Lock())
# Currently implemented:
#  - ban
#  - hackban
#  - unban
#  - mute
#  - unmute
#  - kick
# Not implemented:
#  - warn
ACTION_NAMES: Final[dict[str, LazyStr]] = {
    "ban": _("Ban"),
    "hackban": _("Hackban"),
    "unban": _("Unban"),
    "kick": _("Kick"),
    "mute": _("Mute"),
    "unmute": _("Unmute"),
    "warn": _("Warning"),
}
ACTION_EMOJIS: Final[dict[str, str]] = {
    "ban": "\N{HAMMER}",
    "hackban": "\N{HAMMER}",
    "unban": "\N{DOVE OF PEACE}",
    "kick": "\N{DASH SYMBOL}",
    "mute": "\N{SPEAKER WITH CANCELLATION STROKE}",
    "unmute": "\N{SPEAKER}",
    "warn": "\N{WARNING SIGN}",
}


def format_case(case: db.Case) -> discord.Embed:
    embed = discord.Embed(
        colour=discord.Colour.orange(),
        title=_("Case #{case} | {emoji} {action}").format(
            case=case.case_id,
            emoji=ACTION_EMOJIS[case.action],
            action=ACTION_NAMES[case.action],
        ),
    )

    moderator = (
        (
            f"{case.moderator_name} (<@{case.moderator}> `{case.moderator}`)"
            if case.moderator_name
            else f"<@{case.moderator}>"
        )
        if case.moderator
        else _("*Unknown - claim this case with `reason {id}`*").lazy_format(id=case.case_id)
    )

    description = [
        _("**User** \N{EM DASH} {name} ({mention} `{id}`)").format(
            name=case.target_name or _("*Unknown or deleted member*"),
            mention=f"<@{case.target}>",
            id=case.target,
        ),
        _("**Moderator** \N{EM DASH} {moderator}").format(moderator=moderator),
        _("**Reason** \N{EM DASH} {reason}").format(
            reason=_("*no reason specified*") if case.reason is None else case.reason
        ),
    ]

    if case.until:
        description.append(
            _("**Expires at** \N{EM DASH} {timestamp} ({delta})").format(
                timestamp=Humanize(case.until), delta=Humanize(case.until, "R")
            )
        )

    if case.amended_at:
        description.append(
            _("**Last modified** \N{EM DASH} {timestamp}").format(
                timestamp=Humanize(case.amended_at, "R")
            )
        )
    if case.amended_by and case.amended_by != case.moderator:
        description.append(
            _("**Amended by** \N{EM DASH} {mod}").format(mod=f"<@{case.amended_by}>")
        )

    embed.description = "\n".join(description)
    embed.timestamp = case.timestamp.replace(tzinfo=timezone.utc)
    return embed


async def send_to_modlog(guild: discord.Guild, *args, **kwargs) -> None:
    """Send a message to the guild's mod log channel"""
    mod_log_channel: discord.TextChannel | None = guild.get_channel(
        (await db.Guild.find_one_or_create(guild)).modlog_channel
    )
    if not mod_log_channel or not mod_log_channel.permissions_for(guild.me).send_messages:
        return
    await mod_log_channel.send(*args, **kwargs)


async def create_case(
    *,
    action: db.CaseAction,
    target: discord.abc.User | discord.User | discord.Member | discord.Object,
    moderator: discord.abc.User | discord.User | discord.Member | discord.Object | None,
    guild: discord.Guild,
    reason: str = None,
    until: datetime = None,
) -> db.Case | None:  # sourcery skip: aware-datetime-for-utc
    """Create a new modlog case in the given guild

    Returns
    --------
    Optional[db.Case]
        The :class:`Case` object, or :obj:`None` if a case was not created (usually because
        the guild doesn't have a modlog channel set)
    """
    mod_log_channel: discord.TextChannel | None = guild.get_channel(
        (await db.Guild.find_one_or_create(guild)).modlog_channel
    )
    if not mod_log_channel or not mod_log_channel.permissions_for(guild.me).send_messages:
        return None
    # This lock is necessary to avoid case ID collisions if multiple modlog entries
    # are created at once; this is (relatively) safe as this bot does not support scaling
    # beyond one process in any capacity.
    async with _LOCKS[guild]:
        case_id = await db.Case.next_case_number(guild)
        case = db.Case(
            action=action,
            target=target.id,
            moderator=getattr(moderator, "id", None),
            moderator_name=str(moderator) if isinstance(moderator, discord.abc.User) else None,
            guild_id=guild.id,
            case_id=case_id,
            reason=reason,
            until=until,
            timestamp=datetime.utcnow(),
        )

        if hasattr(target, "name"):
            case.target_name = str(target)
        msg = await mod_log_channel.send(embed=format_case(case))
        case.case_message = msg.id
        await case.insert()
    return case
