import discord
from discord.ext.commands import CheckFailure, Command

from argon._shared import _

__all__ = ("SilentCheckFailure", "RoleBlockedCommand", "EmbedCheckFailure")


class SilentCheckFailure(CheckFailure):
    """Check failure that returns no error message

    This should only be used in cases where an error message couldn't be returned,
    such as if the bot can't send messages in a given channel.

    This is effectively similar to raising a CheckFailure with no arguments,
    except that this is explicitly suppressed from sending anything in
    the command error handler.
    """


class EmbedCheckFailure(CheckFailure):
    """A check failure that supports the use of embed values"""

    def __init__(self, message: discord.Embed):
        self.message = message


class RoleBlockedCommand(CheckFailure):
    def __init__(self, role: discord.Role, command: Command):
        from argon.utils.chat_formatting import error

        self.role = role
        self.command = command
        super().__init__(
            error(
                _("Your {role} role prevents you from using this command.").format(
                    role=role.mention
                )
            )
        )
