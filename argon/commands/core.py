from __future__ import annotations

import contextvars
import logging
import warnings
from inspect import cleandoc
from typing import Callable

import discord
from discord.ext.commands import Cog as _Cog
from discord.ext.commands import Command as _Command
from discord.ext.commands import Context as _Context
from discord.ext.commands import Group as _Group
from discord.ext.commands import HybridCommand as _HybridCommand
from discord.ext.commands import HybridGroup as _HybridGroup
from discord.ext.commands import command as _command

from argon.utils.chat_formatting import box

log = logging.getLogger("argon.commands")
__all__ = (
    "Context",
    "Command",
    "Group",
    "HybridCommand",
    "HybridGroup",
    "command",
    "group",
    "hybrid_command",
    "hybrid_group",
    "Cog",
    "AlwaysAvailableCommand",
    # Exposed for use in internal type checks
    "_ArgonCommandMixin",
)


class Context(_Context):
    async def defer(self, *, ephemeral: bool = False, thinking: bool = False) -> None:
        if self.interaction:
            await self.interaction.response.defer(ephemeral=ephemeral, thinking=thinking)

    async def send_help(self, entity=None):
        await super().send_help(entity or self.command)

    @property
    def embed_colour(self) -> discord.Colour:
        """Get the bot's configured colour"""
        if self.guild:
            colour = self.guild.me.colour
            return self.bot.colour if colour.value == 0 else colour
        return self.bot.colour

    async def add_reaction(self, emoji: str | discord.Emoji) -> bool:
        """Add a reaction to the context message"""
        if self.interaction:
            warnings.warn(
                "Reactions cannot be added on an interaction context", UserWarning, stacklevel=3
            )
            return False
        try:
            await self.message.add_reaction(emoji)
            return True
        except discord.HTTPException:
            return False

    async def tick(self) -> bool:
        """Add a checkmark reaction to the context message"""
        return await self.add_reaction("\N{WHITE HEAVY CHECK MARK}")

    async def send_pagified(
        self,
        content: str,
        *,
        box_lang: str = None,
        embed: bool | discord.Embed = False,
    ):
        """Send the given context in a pagifier"""
        from argon.utils.chat_formatting import pagify
        from argon.utils.menus import PaginatorView

        pages = []
        for page in pagify(content):
            page = box(page, lang=box_lang) if box_lang is not None else page
            if embed is False:
                pages.append(page)
            else:
                embed = discord.Embed(colour=self.embed_colour) if embed is True else embed.copy()
                embed.description = page
                pages.append(embed)

        paginator = PaginatorView(self.author, pages)
        await self.send(**await paginator.send_kwargs())


class _ArgonCommandMixin:
    parent: Command | None
    callback: Callable
    extras: dict

    def __init__(self, *args, **kwargs):
        from argon.translations import Translator

        self.translate_args: dict = kwargs.get("format_args", {})
        self.__help = ""
        self.translator: Translator | Callable[[str, dict], str] = lambda text, _: text
        super().__init__(*args, **kwargs)

    def _ensure_assignment_on_copy(self, other):
        # noinspection PyProtectedMember,PyUnresolvedReferences
        super()._ensure_assignment_on_copy(other)
        other.extras = self.extras
        return other

    def _argon_translate(self, doc: str) -> str:
        return self.translator(cleandoc(doc), self.translate_args)

    @property
    def help(self) -> str | None:
        doc = self.__help or self.callback.__doc__
        if doc is None:
            return None
        return self._argon_translate(self.__help or self.callback.__doc__)

    @help.setter
    def help(self, value: str):
        self.__help = value


class Cog(_ArgonCommandMixin, _Cog):
    @property
    def description(self):
        return self._argon_translate(self.__cog_description__)

    @description.setter
    def description(self, description):
        # noinspection PyAttributeOutsideInit
        self.__cog_description__ = description


class Command(_ArgonCommandMixin, _Command):
    async def invoke(self, ctx: Context, /) -> None:
        from argon.translations import LOCALE_CTX

        invoke = super().invoke

        async def in_ctx():
            locale = (
                str(
                    ctx.interaction.locale if ctx.interaction else ctx.guild.preferred_locale
                ).replace("-", "_")
                or ...
            )
            log.debug("Setting context locale for command invocation to %r", locale)
            LOCALE_CTX.set(locale)
            # noinspection PyUnresolvedReferences
            return await invoke(ctx)

        await contextvars.copy_context().run(in_ctx)


class Group(_ArgonCommandMixin, _Group):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.autohelp: bool = kwargs.get("autohelp", not self.invoke_without_command)

    def command(self, name: str = discord.utils.MISSING, **kwargs):
        def decorator(func):
            kwargs.setdefault("parent", self)
            result = command(name=name, **kwargs)(func)
            self.add_command(result)
            return result

        return decorator

    def group(self, name: str = discord.utils.MISSING, **kwargs):
        def decorator(func):
            kwargs.setdefault("parent", self)
            result = group(name=name, **kwargs)(func)
            self.add_command(result)
            return result

        return decorator

    async def invoke(self, ctx: Context, /) -> None:
        from argon.translations import LOCALE_CTX

        # Thanks Red
        ctx.invoked_subcommand = None
        ctx.subcommand_passed = None
        view = ctx.view
        previous = view.index
        view.skip_ws()
        if trigger := view.get_word():
            ctx.subcommand_passed = trigger
            ctx.invoked_subcommand = self.all_commands.get(trigger, None)
        view.index = previous
        view.previous = previous
        invoke = super().invoke

        async def in_ctx():
            locale = (
                str(
                    ctx.interaction.locale if ctx.interaction else ctx.guild.preferred_locale
                ).replace("-", "_")
                or ...
            )
            log.debug("Setting context locale for command invocation to %r", locale)
            LOCALE_CTX.set(locale)

            if (ctx.invoked_subcommand is None or self == ctx.invoked_subcommand) and self.autohelp:
                await invoke(ctx)
                await ctx.send_help(self)
                return
            await invoke(ctx)

        await contextvars.copy_context().run(in_ctx)


class HybridCommand(Command, _HybridCommand):
    pass


class HybridGroup(Group, _HybridGroup):
    def command(self, name: str = discord.utils.MISSING, *args, **kwargs):
        def decorator(func):
            kwargs.setdefault("parent", self)
            result = hybrid_command(name=name, **kwargs)(func)
            self.add_command(result)
            return result

        return decorator

    def group(self, name: str = discord.utils.MISSING, *args, **kwargs):
        def decorator(func):
            kwargs.setdefault("parent", self)
            result = hybrid_group(name=name, **kwargs)(func)
            self.add_command(result)
            return result

        return decorator


class AlwaysAvailableCommand(Command):
    """Command class that ignores all forms of checks

    This is intended for informational commands that are required to be always be usable,
    typically to provide licensing information.
    """

    async def can_run(self, ctx):
        return True


def command(name=discord.utils.MISSING, cls=Command, **attrs):
    attrs.setdefault("cls", cls)
    return _command(name=name, **attrs)


def group(name=discord.utils.MISSING, cls=Group, **attrs):
    attrs.setdefault("cls", cls)
    return _command(name=name, **attrs)


def hybrid_command(name: str = discord.utils.MISSING, **attrs):
    def decorator(func):
        if isinstance(func, Command):
            raise TypeError("Callback is already a command.")
        return HybridCommand(func, name=name, **attrs)

    return decorator


def hybrid_group(name=discord.utils.MISSING, **attrs):
    def decorator(func):
        if isinstance(func, Command):
            raise TypeError("Callback is already a command.")
        return HybridGroup(func, name=name, **attrs)

    return decorator
