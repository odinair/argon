import discord
from discord import app_commands
from discord.ext import commands

__all__ = (
    "has_guild_permissions",
    "bot_has_guild_permissions",
    "slash_permissions",
    "guild_slash_permissions",
)


# These two decorators may be changed to ignore text permission checks in the future in
# interaction contexts, but for now both apply at the same time.
def slash_permissions(**perms: bool):
    """Combination of :func:`has_permissions` and :func:`app_commands.default_permissions`"""

    invalid = set(perms) - set(discord.Permissions.VALID_FLAGS)
    if invalid:
        raise TypeError(f"Invalid permission(s): {', '.join(invalid)}")

    def decorator(func):
        func = app_commands.default_permissions(**perms)(func)
        return commands.has_permissions(**perms)(func)

    return decorator


def guild_slash_permissions(**perms: bool):
    """Combination of :func:`has_guild_permissions` and :func:`app_commands.default_permissions`"""

    invalid = set(perms) - set(discord.Permissions.VALID_FLAGS)
    if invalid:
        raise TypeError(f"Invalid permission(s): {', '.join(invalid)}")

    def decorator(func):
        func = app_commands.default_permissions(**perms)(func)
        return has_guild_permissions(**perms)(func)

    return decorator


def has_guild_permissions(**perms: bool):
    """Carbon copy of :func:`discord.ext.commands.has_guild_permissions`,
    but also checks for administrator permissions."""

    invalid = set(perms) - set(discord.Permissions.VALID_FLAGS)
    if invalid:
        raise TypeError(f"Invalid permission(s): {', '.join(invalid)}")

    def predicate(ctx: commands.Context) -> bool:
        if not ctx.guild:
            raise commands.NoPrivateMessage()

        if ctx.author == ctx.guild.owner:
            return True
        permissions = ctx.author.guild_permissions
        if permissions.administrator:
            return True
        missing = [perm for perm, value in perms.items() if getattr(permissions, perm) != value]

        if not missing:
            return True

        raise commands.MissingPermissions(missing)

    return commands.check(predicate)


def bot_has_guild_permissions(**perms: bool):
    """Carbon copy of :func:`discord.ext.commands.bot_has_guild_permissions`,
    but also checks for administrator permissions."""

    invalid = set(perms) - set(discord.Permissions.VALID_FLAGS)
    if invalid:
        raise TypeError(f"Invalid permission(s): {', '.join(invalid)}")

    def predicate(ctx: commands.Context) -> bool:
        if not ctx.guild:
            raise commands.NoPrivateMessage()

        if ctx.guild.me == ctx.guild.owner:
            return True
        permissions = ctx.me.guild_permissions
        if permissions.administrator:
            return True
        missing = [perm for perm, value in perms.items() if getattr(permissions, perm) != value]

        if not missing:
            return True

        raise commands.BotMissingPermissions(missing)

    return commands.check(predicate)
