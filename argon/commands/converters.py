import re
from typing import TYPE_CHECKING

import discord
from discord.ext.commands import (
    Converter,
    MemberConverter,
    MemberNotFound,
    UserConverter,
    UserInputError,
)

from argon.commands.core import Context

__all__ = (
    "ReferenceMember",
    "ReferenceUser",
    "ReferenceNotFound",
    "UserOrID",
    "MemberOrID",
)
MENTION_REGEX = re.compile(r"<@!?(\d{15,20})>$")


class ReferenceNotFound(UserInputError):
    """Raised when ReferenceMember fails to find a member from a replied message"""

    def __init__(self):
        super().__init__(
            "Either you aren't replying to a message, or I could not find"
            " the author of the replied message."
        )


if TYPE_CHECKING:
    ReferenceMember = discord.Member
    ReferenceUser = discord.User
    MemberOrID = discord.Member | discord.Object
    UserOrID = discord.User | discord.Object

else:

    class ReferenceMember(Converter):
        async def convert(self, ctx: Context, argument: str) -> discord.Member:
            if argument == "^":
                from argon.utils import find_member_from_ref

                member = await find_member_from_ref(ctx.message)
                if not member or not isinstance(member, discord.Member):
                    raise ReferenceNotFound()
                return member

            return await MemberConverter().convert(ctx, argument)

    class ReferenceUser(Converter):
        async def convert(self, ctx: Context, argument: str) -> discord.User:
            if argument == "^":
                from argon.utils import find_user_from_ref

                member = await find_user_from_ref(ctx.message)
                if not member:
                    raise ReferenceNotFound()
                return member

            return await UserConverter().convert(ctx, argument)

    class _OrID(Converter):
        def _from_id(
            self, ctx: Context, user_id: int
        ) -> discord.User | discord.Member | discord.Object:
            raise NotImplementedError

        async def convert(
            self, ctx: Context, argument: str
        ) -> discord.User | discord.Member | discord.Object:
            if argument.isdigit() and 22 >= len(argument) >= 16:
                user_id = int(argument)
            elif match := MENTION_REGEX.match(argument):
                user_id = int(match.group(1))
            else:
                raise MemberNotFound(argument)
            return self._from_id(ctx, user_id) or discord.Object(id=user_id)

    class MemberOrID(_OrID):
        def _from_id(self, ctx: Context, user_id: int) -> discord.Member | discord.Object:
            return ctx.guild.get_member(user_id)

    class UserOrID(_OrID):
        def _from_id(self, ctx: Context, user_id: int) -> discord.User | discord.Object:
            return ctx.bot.get_user(user_id)
