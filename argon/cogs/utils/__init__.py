import dateparser
import discord
from discord import app_commands

from argon import commands
from argon.bot import Argon
from argon.cogs.utils._shared import _
from argon.translations import Humanize
from argon.utils import slash_guild
from argon.utils.chat_formatting import warning


async def setup(bot: Argon):
    await bot.add_cog(Utils(bot))


@_.cog()
class Utils(commands.Cog):
    """Various small utility commands"""

    def __init__(self, bot: Argon):
        self.bot = bot
        super().__init__()

    @commands.hybrid_command()
    @app_commands.allowed_contexts(guilds=True, dms=True, private_channels=True)
    @app_commands.allowed_installs(guilds=True, users=True)
    @slash_guild
    async def timestamp(self, ctx: commands.Context, *, when: str):
        """Get a timestamp which auto-converts to the timezone of the viewer

        `when` can be a date (such as `Jan 1st, 2022`, `1 August 2011 EDT`),
        a date with time (`October 30th, 12:00 GMT`, `2 July 2015, 14:27 MDT`),
        or a relative time (`in 12 hours`, `12:00 AM PST`).

        Not providing a timezone on an absolute date/time will assume a base timezone of UTC.
        """
        await ctx.defer()
        parsed = await self.bot.loop.run_in_executor(
            None,
            lambda: dateparser.parse(
                when,
                settings={
                    # Convert to UTC
                    "TO_TIMEZONE": "UTC",
                    # Always return a timezone, even if one wasn't provided
                    "RETURN_AS_TIMEZONE_AWARE": True,
                },
            ),
        )
        if parsed is None:
            await ctx.send(warning(_("Failed to parse the given input into a timestamp").format()))
            return
        ts = int(parsed.timestamp())
        timestamps = [
            f"> **<t:{ts}>**\n`<t:{ts}>`",  # short date/time
            f"> **<t:{ts}:F>**\n`<t:{ts}:F>`",  # long date/time
            f"> **<t:{ts}:t>**\n`<t:{ts}:t>`",  # short time
            f"> **<t:{ts}:T>**\n`<t:{ts}:T>`",  # long time
            f"> **<t:{ts}:d>**\n`<t:{ts}:d>`",  # short date
            f"> **<t:{ts}:D>**\n`<t:{ts}:D>`",  # long date
            f"> **<t:{ts}:R>**\n`<t:{ts}:R>`",  # relative
        ]
        await ctx.send(
            embed=discord.Embed(description="\n\n".join(timestamps), colour=ctx.embed_colour)
        )

    # this can't be hybrid as discord sucks and rejects snowflake IDs as ints, to 'protect against
    # other languages that cant handle it', whatever that means.
    @commands.command()
    async def snowflake(self, ctx: commands.Context, snowflake: int):
        """Retrieve when a given ID was created"""
        await ctx.send(
            _("`{snowflake}` was created {delta}").format(
                snowflake=snowflake, delta=Humanize(discord.utils.snowflake_time(snowflake), "R")
            )
        )
