from datetime import datetime
from typing import Iterable, Optional

from babel.dates import format_timedelta
from parse import parse

from argon.bot import Argon
from argon.cogs.status.shared import _, log
from argon.translations import LazyStr, get_babel_locale

warned_deprecated = set()


class Placeholder:
    def __init__(
        self,
        func,
        deprecated: bool = False,
        aliases: list[str] = None,
        name: str = None,
        description: Optional[LazyStr] = None,
    ):
        self._func = func
        self.name = func.__name__ if name is None else name
        self.deprecated = deprecated
        self.aliases = aliases or []
        self.description = description or _("*No description provided*")

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"<Placeholder name={self.name!r} aliases={self.aliases!r}>"

    def __call__(self, *args, **kwargs):
        return self._func(*args, **kwargs)


def placeholder(
    maybe_func=None,
    *,
    name: str = None,
    deprecated: bool = False,
    aliases: list[str] = None,
    description: Optional[LazyStr] = None,
):
    def decorator(func):
        return Placeholder(
            func, deprecated=deprecated, aliases=aliases, name=name, description=description
        )

    if maybe_func:
        return decorator(maybe_func)

    return decorator


# noinspection PyUnusedLocal
class Placeholders(Iterable[tuple[str, Placeholder]]):
    def __iter__(self):
        for k in dir(self):
            v = getattr(self, k)
            if isinstance(v, Placeholder):
                yield k, v

    def __str__(self):
        return "\n".join(
            f"**{{{k}}}** \N{EM DASH} {v.description!s}" for k, v in self if not v.deprecated
        )

    @classmethod
    def parse(cls, status: str, bot: Argon, error_deprecated: bool = False):
        self = cls()
        placeholders = dict(self)
        keys = {}
        for plc in placeholders.values():
            keys[plc.name] = plc.name
            for alias in plc.aliases:
                keys[alias] = plc.name
        # Doing this also filters out invalid placeholders, and lets us filter down to just
        # invoking the placeholder parsers for what we need.
        args = {*parse(status, status.format(**keys)).named.values()}
        kwargs = {}
        log.debug("Extracted args %r from parse", args)
        for arg in args:
            plc: Placeholder = placeholders[arg]
            if plc.deprecated:
                if error_deprecated:
                    raise KeyError(arg)
                if arg not in warned_deprecated:
                    log.warning(
                        "%r is a deprecated placeholder and will be removed in the near"
                        " future, rendering any strings that contain it unable to be"
                        " chosen to be the bot's activity status.",
                        arg,
                    )
                    warned_deprecated.add(arg)
            val = plc(self, bot)
            kwargs[plc.name] = val
            for alias in plc.aliases:
                kwargs[alias] = val
        return status.format(**kwargs)

    @placeholder(aliases=["guilds"], description=_("How many servers this bot is in"))
    def servers(self, bot: Argon) -> int:
        return len(bot.guilds)

    @placeholder(description=_("How many members the bot can see"))
    def members(self, bot: Argon):
        return sum(x.member_count for x in bot.guilds)

    @placeholder(description=_("How many unique users the bot can see"))
    def users(self, bot: Argon):
        return len(bot.users)

    @placeholder(description=_("How many channels the bot can see"))
    def channels(self, bot: Argon):
        return sum(len(x.channels) for x in bot.guilds)

    @placeholder(description=_("How long the bot has been online for"))
    def uptime(self, bot: Argon):
        return format_timedelta(
            datetime.utcnow() - bot.uptime, locale=get_babel_locale(), format="narrow"
        )
