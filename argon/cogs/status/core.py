import re
from random import choice
from typing import Literal, Optional, Union

import discord
from discord.ext.tasks import loop
from discord.utils import escape_markdown

from argon import commands
from argon.bot import Argon
from argon.translations import Humanize
from argon.utils.chat_formatting import pagify, success, warning
from argon.utils.menus import ConfirmMenu, PaginatorView

from .placeholders import Placeholders
from .shared import _, log

TWITCH_REGEX = re.compile(r"<?(?:https?://)?twitch\.tv/[^ ]+>?", re.IGNORECASE)
AVAILABLE_ACTIVITY_TYPES = {
    "playing": discord.ActivityType.playing,
    "listening": discord.ActivityType.listening,
    "watching": discord.ActivityType.watching,
    "competing": discord.ActivityType.competing,
    "streaming": discord.ActivityType.streaming,
}
GAME_TYPES = {
    discord.ActivityType.playing: _("Playing"),
    discord.ActivityType.listening: _("Listening to"),
    discord.ActivityType.watching: _("Watching"),
    discord.ActivityType.competing: _("Competing in"),
    discord.ActivityType.streaming: _("Streaming"),
}


class ActivityTypeConverter(commands.Converter):
    async def convert(self, ctx, argument):
        argument = argument.lower()
        if argument not in AVAILABLE_ACTIVITY_TYPES:
            raise commands.BadArgument(
                _("`game_type` is not of either {options}").format(
                    options=Humanize([*AVAILABLE_ACTIVITY_TYPES])
                )
            )
        return AVAILABLE_ACTIVITY_TYPES[argument]


@_.cog()
class Status(commands.Cog):
    """Randomly set an activity status every 10 minutes"""

    def __init__(self, bot: Argon):
        super().__init__()
        self.bot = bot
        self.status_task.start()

    def cog_unload(self):
        self.status_task.stop()

    @staticmethod
    def extract_status(data: Union[str, dict]) -> tuple[int, str]:
        if isinstance(data, str):
            return 0, data
        return data.get("type", 0), data["game"]

    def format_status(self, status: Union[str, dict], **kwargs) -> discord.Activity:
        game_type = 0
        url = None
        if isinstance(status, dict):
            game_type: int = status.get("type", 0)
            url: Optional[str] = status.get("url")
            status: str = status.get("game")

        formatted = Placeholders.parse(status, self.bot, **kwargs)
        # noinspection PyArgumentList
        return discord.Activity(name=formatted, url=url, type=discord.ActivityType(game_type))

    async def update_status(self, statuses: list[dict]):
        if not statuses:
            return
        status = choice(statuses)
        try:
            game = self.format_status(status)
        except KeyError as e:
            log.exception(
                "Encountered invalid placeholder while attempting to parse status "
                "#%s, skipping status update.",
                statuses.index(status) + 1,
                exc_info=e,
            )
            return
        await self.bot.change_presence(
            activity=game, status=discord.Status(self.bot.bot_config.activity_status)
        )

    @loop(minutes=10)
    async def status_task(self):
        await self.bot.wait_until_ready()
        try:
            await self.update_status(self.bot.bot_config.activities)
        except Exception as e:  # pylint:disable=broad-except
            log.exception("Failed to update bot status", exc_info=e)

    ##############################################

    @commands.group()
    @commands.is_owner()
    async def status(self, ctx: commands.Context):
        """Manage the bot's random activity statuses

        Any invalid placeholders will cause the status to be ignored.
        """

    @status.command(name="add", usage="[activity_type=playing] <status>")
    async def status_add(
        self, ctx: commands.Context, game_type: Optional[ActivityTypeConverter], *, game: str
    ):
        """Add a random status

        `activity_type` may be any of either playing, watching, listening, competing, or streaming.
        Defaults to `playing`.

        If `activity_type` is `streaming`, the first argument after `streaming`
        must be a Twitch channel URL."""

        stream = None
        if game_type is None:
            game_type = discord.ActivityType.playing
        elif game_type == discord.ActivityType.streaming:
            game = game.split(" ")
            stream, game = game.pop(), " ".join(game)
            if not TWITCH_REGEX.match(stream):
                raise commands.CheckFailure(
                    _(
                        "Given activity type was `streaming`, but no Twitch stream"
                        " link was provided."
                    )
                )
        activity = {
            "type": game_type.value,
            "game": game,
            "url": stream.strip("<>") if stream else None,
        }

        try:
            self.format_status(activity, error_deprecated=True)
        except KeyError as e:
            await ctx.send(
                warning(
                    _(
                        "Failed to parse the given status: `{placeholder}` is not a valid"
                        " placeholder name"
                    ).format(placeholder=str(e))
                )
            )
        else:
            statuses = self.bot.bot_config.activities
            statuses.append(activity)
            await self.bot.bot_config.set({"activities": self.bot.bot_config.activities})
            await ctx.send(success(_("Added status **#{id}**").format(id=len(statuses))))

    @status.command(name="parse")
    async def status_parse(self, ctx: commands.Context, *, status: str):
        """Parse placeholder arguments in a given string"""
        try:
            result = self.format_status(status, error_deprecated=True)
        except KeyError as e:
            await ctx.send(
                warning(
                    _(
                        "Failed to parse the given status: `{placeholder}` is not a valid"
                        " placeholder name"
                    ).format(placeholder=str(e))
                )
            )
        else:
            await ctx.send(
                _("\N{INBOX TRAY} **Input:** {input}\n\N{OUTBOX TRAY} **Result:** {result}").format(
                    input=status, result=result.name
                ),
                allowed_mentions=discord.AllowedMentions.none(),
            )

    @status.command(name="remove", aliases=["delete"])
    @commands.bot_has_permissions(embed_links=True)
    async def status_remove(self, ctx: commands.Context, status: int):
        """Remove a given status from the list of statuses to select from"""
        statuses = self.bot.bot_config.activities
        if len(statuses) < status:
            await ctx.send(warning(_("No such status with the ID {id} exists").format(id=status)))
            return

        removed = statuses.pop(status - 1)
        if not statuses:
            await self.bot.change_presence(activity=None, status=getattr(ctx.me, "status", None))
        await self.bot.bot_config.set({"activities": self.bot.bot_config.activities})

        activity_type, activity_name = self.extract_status(removed)
        # noinspection PyArgumentList
        removed_activity = f"**{GAME_TYPES[discord.ActivityType(activity_type)]}** {activity_name}"

        await ctx.send(
            success(_("Removed status #{id}").format(id=status)),
            embed=discord.Embed(description=removed_activity),
        )

    @status.command(name="list")
    async def status_list(self, ctx: commands.Context):
        """List all set statuses"""
        statuses = list(self.bot.bot_config.activities)
        if not statuses:
            await ctx.send(
                _("No statuses are currently set! Use `{prefix}status add` to add some!").format(
                    prefix=ctx.clean_prefix
                )
            )
            return

        pages = []
        for status in statuses:
            index = statuses.index(status) + 1
            game_type = 0
            stream_url = None
            if isinstance(status, dict):
                game_type = status.get("type", 0)
                stream_url = status.get("url")
                status = status.get("game")
            status = escape_markdown(status)
            if stream_url:
                status = f"[{status}]({stream_url})"
            # noinspection PyArgumentList
            pages.append(
                f"**#{index}** \N{EM DASH} **{GAME_TYPES[discord.ActivityType(game_type)]}**"
                f" {status}"
            )

        pages = [
            discord.Embed(colour=ctx.embed_colour, description=x) for x in pagify("\n".join(pages))
        ]
        await ctx.send(**await PaginatorView(ctx.author, pages).send_kwargs())

    @status.command(name="clear")
    async def status_clear(self, ctx: commands.Context):
        """Delete all currently set statuses"""
        amount = len(self.bot.bot_config.activities)
        if await ConfirmMenu(ctx.author).prompt(
            ctx,
            content=_(
                "Are you sure you want to remove"
                " {count, plural, one {# status} other {# statuses}}?\nThis action is irreversible!"
            ).format(count=amount),
        ):
            await self.bot.bot_config.set({"activities": []})
            await self.bot.change_presence(activity=None, status=self.bot.guilds[0].me.status)
            await ctx.send(
                success(
                    _("Removed {count, plural, one {# status} other {# statuses}}.").format(
                        count=amount
                    )
                )
            )
        else:
            await ctx.send(_("Cancelled.").format())

    @status.command(name="activity")
    async def status_activity(
        self, ctx: commands.Context, status: Literal["online", "idle", "dnd", "offline"]
    ):
        """Set the activity status used when changing statuses"""
        await self.bot.bot_config.set({"activity_status": status})
        await ctx.tick()
