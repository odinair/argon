from argon.bot import Argon


async def setup(bot: Argon):
    from .core import Status

    await bot.add_cog(Status(bot))
