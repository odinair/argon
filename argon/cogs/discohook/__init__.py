from argon.bot import Argon

from argon.cogs.discohook.core import Discohook


async def setup(bot: Argon):
    await bot.add_cog(Discohook())
