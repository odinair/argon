from typing import cast

import discord
from discord import app_commands

from argon import commands
from argon.cogs.discohook.share import DiscohookHTTPException, from_discohook, share_discohook_url
from argon.translations import Translator
from argon.utils import missing_permissions, slash_guild

_ = Translator(__file__)


@_.cog()
class Discohook(commands.Cog):
    @commands.hybrid_group()
    @commands.has_guild_permissions(administrator=True)
    @slash_guild
    async def discohook(self, ctx: commands.Context):
        """Discohook related commands"""
        if not ctx.invoked_subcommand:
            await ctx.send_help()

    @discohook.command()
    @app_commands.describe(message="A link to the message to load in Discohook")
    async def load(self, ctx: commands.Context, message: commands.MessageConverter):
        """Load a message into Discohook"""
        message = cast(discord.Message, message)
        if not message.content and not message.embeds:
            await ctx.send(
                _(
                    "That message doesn't have any content nor embeds that can be restored"
                    " as a Discohook link."
                ).format(),
                ephemeral=True,
            )
            return

        if ctx.interaction:
            await ctx.interaction.response.defer(thinking=True)
        url, expires = await share_discohook_url([message])
        view = discord.ui.View()
        view.add_item(
            discord.ui.Button(
                style=discord.ButtonStyle.link, url=url, label=_("Open in Discohook").format()
            )
        )
        await ctx.send(
            content=_(
                "Click the button to open this message in Discohook; link expires"
                " {expiry, timestamp, relative}"
            ).format(expiry=expires),
            view=view,
        )

    @discohook.command()
    @app_commands.describe(
        discohook_url="A direct or shared discohook.org link to load messages from",
        channel="The channel to send messages in; defaults to current",
    )
    async def send(
        self, ctx: commands.Context, discohook_url: str, channel: discord.TextChannel = None
    ):
        """Send a Discohook message as the bot

        `discohook_url` should be a shared message link from <https://discohook.org/>,
        but a direct link (i.e copying the contents of the address bar) will also work.
        """
        channel = channel or ctx.channel
        if missing := missing_permissions(
            channel=channel,
            member=ctx.me,
            permissions=discord.Permissions(send_messages=True, embed_links=True),
        ):
            raise commands.BotMissingPermissions([*missing])

        if ctx.interaction:
            await ctx.interaction.response.defer(thinking=True)
        try:
            messages = await from_discohook(discohook_url)
        except DiscohookHTTPException:
            await ctx.send(
                _(
                    "The given share link is invalid; has it expired, or did you make a typo?"
                ).format()
            )
            return
        if not messages or all(not x[0] and not x[1] for x in messages):
            await ctx.send(_("That link doesn't have any messages to send.").format())
            return

        for content, embeds in messages:
            if content or embeds:
                await channel.send(content, embeds=embeds)

        await ctx.send(
            _("{count, plural, one {# message} other {# messages}} sent in {channel}.").format(
                count=len(messages), channel=channel
            )
        )

    @discohook.command()
    @app_commands.describe(
        discohook_url="A direct or shared discohook.org link to load the message from",
        message="A link to the message to edit",
    )
    async def edit(
        self, ctx: commands.Context, message: commands.MessageConverter, discohook_url: str
    ):
        """Edit a message previously sent by the bot

        See `[p]help discohook send` for information on `discohook_url`.
        """
        message = cast(discord.Message, message)
        # Discord doesn't prevent us from editing messages we've already sent, so we don't
        # need to check permissions for that here; and yes, that includes embeds, even if
        # we no longer have embed links permissions. I don't know why, but it simplifies the
        # logic required for this command, so I won't complain.
        if message.author != ctx.me:
            await ctx.send(_("That message wasn't sent by me!").format(), ephemeral=True)
            return

        if ctx.interaction:
            await ctx.interaction.response.defer(thinking=True)
        try:
            data = await from_discohook(discohook_url)
        except DiscohookHTTPException:
            await ctx.send(
                _(
                    "The given share link is invalid; has it expired, or did you make a typo?"
                ).format()
            )
            return

        if not data:
            await ctx.send(_("That link doesn't have any messages to send.").format())
            return
        elif len(data) > 1:
            await ctx.send(
                _("This command only supports Discohook links with exactly one message.").format()
            )
            return

        content, embeds = next(iter(data))
        if not content and not embeds:
            await ctx.send(
                _("The given Discohook message does not have any content nor embeds.").format()
            )
            return

        try:
            await message.edit(content=content, embeds=embeds)
        except discord.HTTPException:
            await ctx.send(_("Failed to edit message.").format())
        else:
            await ctx.send(_("Message edited successfully.").format())
