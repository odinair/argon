import base64
from datetime import datetime
from typing import Iterable
from urllib.parse import urlparse, urlunparse, parse_qsl

import aiohttp
import discord
import ujson
from cachetools import TTLCache

__all__ = ("DiscohookHTTPException", "from_discohook", "to_discohook_url", "share_discohook_url")

DISCOHOOK_DATA_URL_BASE = "https://discohook.org/?data="
DISCOHOOK_SHARE_CREATE_URI = "https://share.discohook.app/create"
PARSED_SHARE_CACHE = TTLCache(ttl=30 * 60, maxsize=512)


class DiscohookHTTPException(Exception):
    """Raised when Discohook returns an HTTP status >=400"""

    def __init__(self, response: aiohttp.ClientResponse):
        self.response = response


def embed_from_dict(embed: dict) -> discord.Embed:
    """Special handling around embed data

    This is required as some data - most notably embed colours - require extra handling
    to work properly with d.py.
    """
    if "color" in embed and embed["color"] is None:
        embed["color"] = 0
    return discord.Embed.from_dict(embed)


async def from_discohook(url: str) -> list[tuple[str, list[discord.Embed]]] | None:
    """Convert a Discohook URL to usable message data

    This supports both direct and shared URLs.
    """
    parsed = urlparse(url)
    query = {}
    if parsed.netloc == "share.discohook.app":
        if url in PARSED_SHARE_CACHE:
            query = PARSED_SHARE_CACHE[url]
        else:
            async with aiohttp.ClientSession() as session:
                async with session.get(
                    urlunparse(("https", parsed.netloc, parsed.path, "", "", "")),
                    allow_redirects=False,
                ) as resp:
                    # The former check of this if block will pretty much always
                    # catch the latter, but it doesn't hurt to be safe.
                    if resp.status >= 400 or "location" not in resp.headers:
                        raise DiscohookHTTPException(resp)
                    PARSED_SHARE_CACHE[url] = query = dict(
                        parse_qsl(urlparse(resp.headers["location"]).query)
                    )
    elif parsed.netloc == "discohook.org":
        query = dict(parse_qsl(parsed.query))
    else:
        return None

    if not query or "data" not in query:
        return None
    b64_string = query["data"]
    # discohook doesn't include padding in the query string, so we have to manually add it
    b64_string += "=" * (-len(b64_string) % 4)
    try:
        data = ujson.loads(base64.urlsafe_b64decode(b64_string.encode()).decode())
    except ValueError:
        return None

    data = [
        (
            message["data"].get("content") or None,
            [embed_from_dict(embed) for embed in message["data"].get("embeds") or []],
        )
        for message in data["messages"]
    ]
    return data


def get_message_data(message: discord.Message) -> dict:
    data = {"content": message.content or None, "embeds": []}

    for embed in message.embeds:
        if embed.type != "rich":
            continue

        embed_dict = embed.to_dict()
        embed_dict.pop("type", None)
        embed_dict.get("image", {}).pop("proxy_url", None)
        embed_dict.get("image", {}).pop("width", None)
        embed_dict.get("image", {}).pop("height", None)
        embed_dict.get("thumbnail", {}).pop("proxy_url", None)
        embed_dict.get("thumbnail", {}).pop("width", None)
        embed_dict.get("thumbnail", {}).pop("height", None)
        embed_dict.get("author", {}).pop("proxy_icon_url", None)
        embed_dict.get("footer", {}).pop("proxy_icon_url", None)

        data["embeds"].append(embed_dict)

    return data


def to_discohook_url(
    messages: Iterable[discord.Message], /, webhook: discord.Webhook = None
) -> str:
    """Convert the given messages to a direct Discohook URL

    .. note::
        This may be too long to send in Discord; you should use `share_discohook_url`
        if you need to share the link.
    """
    data = {"messages": []}

    for message in messages:
        mdata = {"data": get_message_data(message)}
        if webhook:
            mdata["reference"] = message.jump_url
        data["messages"].append(mdata)
    if webhook:
        data["targets"] = [{"url": webhook.url}]

    encoded_data = (
        base64.urlsafe_b64encode(ujson.dumps(data, ensure_ascii=False).encode())
        .decode()
        .rstrip("=")
    )
    return DISCOHOOK_DATA_URL_BASE + encoded_data


async def share_discohook_url(
    messages: Iterable[discord.Message], webhook: discord.Webhook = None
) -> tuple[str, datetime]:
    """Get a Discohook share URL for the given messages"""
    url = to_discohook_url(messages, webhook=webhook)
    async with aiohttp.ClientSession() as session:
        async with session.post(DISCOHOOK_SHARE_CREATE_URI, json={"url": url}) as resp:
            if resp.status >= 400:
                raise DiscohookHTTPException(resp)

            data = await resp.json()
            return data["url"], datetime.fromisoformat(data["expires"])
