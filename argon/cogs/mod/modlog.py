import asyncio
from datetime import datetime
from typing import Optional, cast

import discord
from beanie.odm.enums import SortDirection
from discord import app_commands

from argon import commands, db, modlog
from argon.bot import Argon
from argon.cogs.mod._shared import _
from argon.db import CaseAction
from argon.utils import slash_guild
from argon.utils.chat_formatting import info, inline, success, warning
from argon.utils.time import FutureTime


class ModLog:
    bot: Argon
    _timing_out: set[int]

    @commands.hybrid_command()
    @commands.guild_only()
    @commands.has_guild_permissions(manage_messages=True)
    @slash_guild
    @app_commands.describe(case="The case number to retrieve")
    async def case(self, ctx: commands.Context, case: int):
        """Retrieve a given mod log case"""
        case: db.Case = await db.Case.find_one({"case_id": case, "guild_id": ctx.guild.id})
        if not case:
            return await ctx.send(warning(_("That case doesn't exist.").format()))
        await ctx.send(embed=modlog.format_case(case))

    @commands.hybrid_group(fallback="for")
    @commands.guild_only()
    @commands.has_guild_permissions(manage_messages=True)
    @slash_guild
    @app_commands.describe(user="The member to get cases against")
    async def cases(self, ctx: commands.Context, *, user: discord.User):
        """Get all mod log cases against a given user"""
        uid = getattr(user, "id", user)
        # noinspection PyTypeChecker
        cases: list[db.Case] = await db.Case.find_many(
            {"target": uid, "guild_id": ctx.guild.id}, projection_model=db.Case
        ).to_list()
        if not cases:
            return await ctx.send(
                info(
                    _("{user} has no cases created against them.").format(
                        user=inline(str(user)) if isinstance(user, int) else user
                    )
                )
            )
        count = len(cases)
        await ctx.send_pagified(
            info(
                _("{user} has {count, plural, one {# case} other {# cases}}.\n\n{cases}").format(
                    user=inline(str(user)) if isinstance(user, int) else user,
                    count=count,
                    cases="\n".join(
                        f"**Case #{case.case_id} \N{EM DASH} {modlog.ACTION_NAMES[case.action]}**"
                        f" | {_('*no reason given*') if case.reason is None else case.reason}"
                        for case in cases
                    ),
                )
            )
        )

    @cases.command(name="by")
    async def cases_by(self, ctx: commands.Context, *, moderator: discord.Member):
        """Return all cases created by a given moderator"""
        # noinspection PyTypeChecker
        cases: list[db.Case] = await db.Case.find_many(
            {"guild_id": ctx.guild.id, "moderator": moderator.id}, projection_model=db.Case
        ).to_list()
        if not cases:
            return await ctx.send(
                info(_("{moderator} has not created any cases.").format(moderator=moderator))
            )
        await ctx.send_pagified(
            info(
                _(
                    "{user} has created {count, plural, one {# case} other {# cases}}.\n\n{cases}"
                ).format(
                    user=moderator,
                    count=len(cases),
                    cases="\n".join(
                        f"**Case #{case.case_id} \N{EM DASH} {modlog.ACTION_NAMES[case.action]}**"
                        f" | {_('*no reason given*') if case.reason is None else case.reason}"
                        for case in cases
                    ),
                )
            )
        )

    @commands.hybrid_command(require_var_positional=True)
    @commands.guild_only()
    @commands.has_guild_permissions(manage_messages=True)
    @slash_guild
    @app_commands.describe(
        case="The case to modify the reason for; defaults to your most recent case",
        reason="The updated reason for this case",
    )
    async def reason(self, ctx: commands.Context, case: Optional[int], *, reason: str):
        """Update the reason for a given case

        If no case is specified, the most recent case by yourself is edited.
        """
        case: db.Case | None = (
            await db.Case.find_one({"case_id": case, "guild_id": ctx.guild.id})
            if case
            else next(
                iter(
                    (
                        # Find all cases by this moderator
                        await db.Case.find_many(
                            {"guild_id": ctx.guild.id, "moderator": ctx.author.id},
                            projection_model=db.Case,
                        )
                        # Sort so the most recent one is first
                        .sort(("case_id", SortDirection.DESCENDING))
                        # And only grab the first such case
                        .limit(1).to_list()
                    )
                )
            )
        )
        if not case:
            return await ctx.send(warning(_("That case doesn't exist.").format()))
        guild = await db.Guild.find_one_or_create(ctx.guild)
        channel: discord.TextChannel = ctx.guild.get_channel(guild.modlog_channel)
        if not channel:
            return await ctx.send(
                warning(_("Your mod log channel appears to no longer exist.").format())
            )
        update = {"reason": reason, "amended_by": ctx.author.id, "amended_at": datetime.utcnow()}
        if case.moderator is None:
            update["moderator"] = ctx.author.id
        await case.set(update)
        try:
            await channel.get_partial_message(case.case_message).edit(
                embed=modlog.format_case(case)
            )
        except discord.NotFound:
            # This should be a rare case, so we're safe to do a second set here.
            msg = await channel.send(embed=modlog.format_case(case))
            await case.set({"case_message": msg.id})
        await ctx.send(success(_("Updated reason for case #{case}").format(case=case.case_id)))

    @commands.group()
    @commands.guild_only()
    @commands.has_guild_permissions(manage_guild=True)
    async def modlog(self, ctx: commands.Context):
        """Manage this server's modlog

        Note that this is separate from `[p]overseer`, as this only logs direct
        moderator actions (such as bans, kicks, mutes, etc).

        Actions performed by moderators without the bot will also be logged,
        such as manually timing out, kicking, or banning a member.
        """

    @modlog.command(name="channel")
    async def modlog_channel(self, ctx: commands.Context, channel: discord.TextChannel):
        """Set the server's mod log channel"""
        data = await db.Guild.find_one_or_create(ctx.guild)
        await data.set({"modlog_channel": channel.id})
        await ctx.send(
            success(_("Mod log cases will now be sent to {channel}.").format(channel=channel))
        )

    @modlog.command(hidden=True, name="debug")
    @commands.is_owner()
    async def modlog_debug(
        self,
        ctx: commands.Context,
        case_type: Optional[db.CaseAction] = "ban",
        until: FutureTime = None,
        reason: str = None,
    ):
        """Create a test case in the current guild"""
        await modlog.create_case(
            action=case_type,
            guild=ctx.guild,
            moderator=ctx.author,
            target=ctx.author,
            until=datetime.utcnow() + until if until else None,
            reason=reason,
        )

    @staticmethod
    async def __get_audit_action(
        action: discord.AuditLogAction,
        target: discord.User | discord.Member | discord.Object,
        guild: discord.Guild,
    ) -> Optional[discord.AuditLogEntry]:
        if guild.me.guild_permissions.view_audit_log:
            async for entry in guild.audit_logs(limit=5, action=action):
                if entry.target.id == target.id:
                    return entry

    @commands.Cog.listener()
    async def on_audit_log_entry_create(self, entry: discord.AuditLogEntry):
        if entry.action not in (
            discord.AuditLogAction.kick,
            discord.AuditLogAction.ban,
            discord.AuditLogAction.unban,
        ):
            return
        # wait a second to allow for whatever code is currently running to mark this as being
        # logged independently, so as to allow for correct mod logs from things such as hackban
        await asyncio.sleep(1)

        # ignore any entries we've created ourselves as we're likely either intentionally
        # not logging them, or have already logged them
        if entry.user_id == self.bot.user.id:
            return

        action = cast(
            CaseAction,
            (
                "ban"
                if entry.action == discord.AuditLogAction.ban
                else "unban"
                if entry.action == discord.AuditLogAction.unban
                else "kick"
            ),
        )
        await modlog.create_case(
            action=action,
            target=entry.target,
            moderator=entry.user,
            guild=entry.guild,
            reason=entry.reason,
        )

    @commands.Cog.listener("on_member_update")
    async def on_member_timeout(self, before: discord.Member, after: discord.Member):
        # This is safe to do as Discord will only ever update this field if a member is
        # being actively timed out, or their timeout is being removed by a moderator
        if before.timed_out_until == after.timed_out_until:
            return
        await asyncio.sleep(1)
        if after.id in self._timing_out:
            self._timing_out.discard(after.id)
            return

        reason: Optional[str] = None
        moderator: Optional[discord.abc.User] = None
        if entry := await self.__get_audit_action(
            discord.AuditLogAction.member_update, after, after.guild
        ):
            reason = entry.reason
            moderator = entry.user
        action = cast(CaseAction, "mute" if after.is_timed_out() else "unmute")
        await modlog.create_case(
            action=action,
            target=after,
            moderator=moderator,
            guild=after.guild,
            reason=reason,
            until=after.timed_out_until,
        )
