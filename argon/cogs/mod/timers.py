import asyncio
from datetime import datetime, timedelta
from typing import Awaitable, Callable, Optional

import discord
from beanie.odm.enums import SortDirection
from discord.ext.tasks import loop

from argon import db, modlog
from argon.bot import Argon
from argon.cogs.mod._shared import _, log
from argon.utils.scheduler import Scheduler


class Timers:
    bot: Argon
    _get_mute_role: Callable[[discord.Guild], Awaitable[Optional[discord.Role]]]
    scheduler: Scheduler

    async def __tempban_expire(self, user_id: int, guild: discord.Guild):
        if not guild.me.guild_permissions.ban_members:
            return

        member = await db.Member.find_one({"user_id": user_id, "guild_id": guild.id})
        # Attempt to find the case for this member's tempban
        case: Optional[db.Case] = next(
            iter(
                await db.Case.find(
                    {
                        "guild_id": guild.id,
                        "user_id": user_id,
                        "action": {"$in": ["ban", "hackban"]},
                    }
                )
                .sort(("case_id", SortDirection.DESCENDING))
                .limit(1)
                .to_list()
            ),
            None,
        )

        case_id = case.case_id if case else -1
        mod = guild.get_member(case.moderator) if case else None
        reason = (
            _("Temporary ban expired (case #**{case}**, applied by {mod})")
            if case
            else _("Temporary ban expired")
        )
        unknown = _("*unknown*")
        # str() is used in the audit reason to ensure we get a username instead of a mention,
        # as audit logs don't render mentions
        audit_reason = reason.format(case=case_id, mod=str(mod or unknown))
        case_reason = reason.format(case=case_id, mod=mod or unknown)

        try:
            await guild.unban(discord.Object(id=user_id), reason=audit_reason)
        except discord.HTTPException:
            log.exception(
                "Failed to remove tempban for user with id %s in guild %r", user_id, guild
            )
        else:
            await member.set({"banned_until": None})
            await modlog.create_case(
                action="unban",
                moderator=guild.me,
                target=discord.Object(id=user_id),
                reason=case_reason,
                guild=guild,
            )

    async def __mute_expire(self, user_id: int, guild: discord.Guild):
        if not guild.me.guild_permissions.manage_roles:
            return

        user = await db.Member.find_one({"user_id": user_id, "guild_id": guild.id})
        member = guild.get_member(user.user_id)
        if not member:
            return
        role = await self._get_mute_role(guild)
        if not role:
            return

        try:
            await member.remove_roles(role, reason="Automatic unmute")
        except discord.DiscordServerError:
            log.exception(
                "Discord returned an error while attempting to unmute %r, trying again later",
                member,
            )
        except discord.HTTPException:
            # The most common case for this except block to be hit is if the mute role
            # is above our topmost role, in which case there isn't anything we can do about
            # it, so the server moderators will unfortunately have to figure this one out
            # on their own, as this isn't something that I feel is worth investing
            # extra time into properly handling, and instead is just something to not
            # continue attempting.
            await user.set({"muted": False, "muted_until": None})
            log.exception("Cannot unmute %r, giving up.", member)
        else:
            await user.set({"muted": False, "muted_until": None})
            await modlog.create_case(
                action="unmute",
                guild=guild,
                moderator=guild.me,
                target=member,
                reason="Automatic unmute",
            )

    @loop(minutes=15)
    async def _expire_punishment_loop(self):
        await self.bot.wait_until_ready()
        _index = 0
        async for user in db.Member.find_many(
            {
                "$or": [
                    {
                        "muted": True,
                        "muted_until": {
                            "$exists": True,
                            "$ne": None,
                            "$lt": datetime.utcnow() + timedelta(minutes=20),
                        },
                    },
                    {
                        "banned_until": {
                            "$exists": True,
                            "$ne": None,
                            "$lt": datetime.utcnow() + timedelta(minutes=20),
                        }
                    },
                ]
            }
        ):
            if _index >= 30:
                _index = 0
                # Give the bot time to do other things periodically
                await asyncio.sleep(0)
            _index += 1

            guild: discord.Guild = self.bot.get_guild(user.guild_id)
            if not guild or guild.unavailable:
                continue

            if (
                user.banned_until
                and user.banned_until < datetime.utcnow()
                and user.user_id not in self.scheduler
            ):
                self.scheduler.add_callback(
                    self.__tempban_expire, user.user_id, user.banned_until, guild
                )
            elif user.muted and user.user_id not in self.scheduler:
                await self.scheduler.add_callback(
                    self.__mute_expire, user.user_id, user.muted_until, guild
                )
