from argon.bot import Argon
from argon.cogs.mod.core import Mod


async def setup(bot: Argon):
    cog = Mod(bot)
    # noinspection PyProtectedMember
    cog._expire_punishment_loop.start()
    await bot.add_cog(cog)
