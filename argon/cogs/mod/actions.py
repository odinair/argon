import inspect
from datetime import timedelta
from typing import Sequence, cast

import discord
from discord import app_commands

from argon import commands, db, modlog
from argon.cogs.mod._shared import _
from argon.translations import Humanize
from argon.utils import deduplicate, format_audit_reason, slash_guild
from argon.utils.chat_formatting import deny, error, success, warning
from argon.utils.embeds import mod_action
from argon.utils.mod import hierarchy_allows
from argon.utils.time import FutureTime

TimeoutDuration = FutureTime().min_time("1 minute").max_time("28 days")
BanDuration = FutureTime().min_time("1 hour")


class ModActions:
    _timing_out: set[int]

    @commands.hybrid_command()
    @commands.bot_has_guild_permissions(ban_members=True)
    @commands.has_guild_permissions(ban_members=True)
    @slash_guild
    @app_commands.describe(
        member="The member to ban",
        duration="How long to ban the member for (e.g. '7 days'), minimum 1 hour",
        reason="The reason for this ban",
    )
    async def tempban(
        self,
        ctx: commands.Context,
        member: discord.User,
        duration: BanDuration,
        *,
        reason: str = None,
    ):
        """Temporarily ban a member"""
        if mb := ctx.guild.get_member(member.id):
            if err := hierarchy_allows(mod=ctx.author, target=mb):
                return await ctx.send(deny(err))

        if ctx.interaction:
            await ctx.interaction.response.defer(thinking=True)
        until = discord.utils.utcnow() + duration
        member_data = await db.Member.find_one_or_create_in_guild(member, ctx.guild)
        try:
            await ctx.guild.ban(
                member, reason=format_audit_reason(moderator=ctx.author, reason=reason)
            )
        except discord.Forbidden:
            return await ctx.send(error(_("I cannot ban that member.").format()))
        except discord.HTTPException as e:
            return await ctx.send(
                warning(_("Failed to ban {member}: {error}").format(member=member, error=e.text))
            )
        else:
            await member_data.set({"banned_until": until})
            await modlog.create_case(
                action="ban",
                guild=ctx.guild,
                moderator=ctx.author,
                target=member,
                until=until,
                reason=reason,
            )

        await ctx.send(
            embed=mod_action(
                _("Banned {member} until {ts, timestamp}").format(member=member, ts=until),
                reason=reason,
                colour=discord.Colour.dark_orange(),
            )
        )

    @commands.hybrid_command(aliases=["timeout"])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_messages=True)
    @commands.bot_has_guild_permissions(moderate_members=True, manage_roles=True)
    @slash_guild
    @app_commands.describe(
        member="The member to mute",
        duration="Any duration between 1 minute and 28 days (e.g. '1 hour')",
        reason="The reason for this mute",
    )
    async def mute(
        self,
        ctx: commands.Context,
        member: discord.Member,
        duration: TimeoutDuration,
        *,
        reason: str = None,
    ):
        """Apply a timeout to the given member

        This allows for timeouts of custom duration, which aren't restricted
        to the few options ranging from 60 seconds to 1 week which Discord
        allows for through the native timeout UI.

        Due to Discord limitations, timeouts may only be at most 28 days in length.
        """
        if member == ctx.author:
            return await ctx.send(deny(_("You cannot mute yourself.").format()))
        elif err := hierarchy_allows(mod=ctx.author, target=member):
            return await ctx.send(deny(err))

        until_absolute = discord.utils.utcnow() + duration
        self._timing_out.add(member.id)
        await member.edit(
            timed_out_until=until_absolute, reason=format_audit_reason(ctx.author, reason)
        )

        # noinspection PyTypeChecker
        await modlog.create_case(
            action="mute",
            guild=ctx.guild,
            moderator=ctx.author,
            target=member,
            until=until_absolute,
            reason=reason,
        )

        await ctx.send(
            embed=mod_action(
                _("{user} has been muted until {until}").format(
                    user=member, until=Humanize(until_absolute)
                ),
                reason=reason,
                colour=discord.Colour.yellow(),
            )
        )

    @commands.command(aliases=["untimeout"])
    @commands.guild_only()
    @commands.has_guild_permissions(manage_messages=True)
    @commands.bot_has_guild_permissions(moderate_members=True, manage_roles=True)
    @slash_guild
    @app_commands.describe(
        user="The member to remove a timeout on", reason="The reason for removing this timeout"
    )
    async def unmute(self, ctx: commands.Context, user: discord.Member, *, reason: str = None):
        """Remove a timeout for a given user"""
        if user.top_role >= ctx.author.top_role:
            return await ctx.send(
                deny(
                    _(
                        "Cannot perform action; {user} is equal to or higher than you"
                        " in this server's role hierarchy."
                    ).format(user=user)
                )
            )
        elif user.top_role >= ctx.me.top_role:
            return await ctx.send(
                deny(
                    _(
                        "Cannot perform action; {user} is equal to or higher than me"
                        " in this server's role hierarchy."
                    ).format(user=user)
                )
            )

        udata = await db.Member.find_one_or_create(user)
        if not udata.muted and not user.is_timed_out():
            return await ctx.send(
                warning(_("{member} is not currently muted.").format(member=user))
            )

        if udata.muted:
            gdata = await db.Guild.find_one_or_create(ctx.guild)
            role = ctx.guild.get_role(gdata.mute_role)
            # The way this is done has the consequence of returning an incorrect unmuted message
            # once if the server's muted role has since been deleted, but that's something
            # I'm okay with.
            if role and role in user.roles:
                await user.remove_roles(role, reason=format_audit_reason(ctx.author, reason))
            await udata.set({"muted": False, "muted_until": None})
        if user.is_timed_out():
            await user.edit(timed_out_until=None, reason=format_audit_reason(ctx.author, reason))

        # noinspection PyTypeChecker
        await modlog.create_case(
            action="unmute",
            guild=ctx.guild,
            moderator=ctx.author,
            target=user,
            reason=reason,
        )
        await ctx.send(embed=mod_action(_("{user} is no longer muted").format(user=user), reason))

    @commands.hybrid_command(aliases=["hackban"], usage="<user id>... [reason]")
    @commands.guild_only()
    @commands.has_guild_permissions(ban_members=True)
    @commands.bot_has_guild_permissions(ban_members=True)
    @slash_guild
    @app_commands.describe(
        users="One or more mentions or IDs of users to ban",
        reason="The reason for banning these users",
    )
    async def massban(
        self,
        ctx: commands.Context,
        users: commands.Greedy[commands.MemberOrID],
        *,
        reason: str = None,
    ):
        """Ban multiple members by ID, even if they've since left the server

        This command only accepts user mentions and IDs.
        """
        users = deduplicate(cast(Sequence[discord.Member | discord.Object], users))
        if not users:
            # the alternative here is to use a private enum, which pycharm seems to disagree
            # that it even exists
            # noinspection PyTypeChecker
            param = inspect.Parameter(kind=1, name="users")
            raise commands.MissingRequiredArgument(param)

        original = await ctx.send(
            _(
                "\N{HOURGLASS} Working on banning {count, plural, one {**#** member}"
                " other {**#** members}}..."
            ).format(count=len(users))
        )

        out = []
        banned = []
        failed = []
        for user in users:
            is_member = isinstance(user, discord.Member)
            if is_member and (err := hierarchy_allows(ctx.author, user)):
                out.append(err)
                continue
            try:
                await ctx.guild.ban(user, reason=format_audit_reason(ctx.author, reason))
            except discord.HTTPException:
                failed.append(user)
            else:
                await modlog.create_case(
                    # the cast call here is necessary to keep pycharm from complaining
                    # that this doesn't match the Literal, despite the fact that this is
                    # a subset of said literal
                    action=cast(db.CaseAction, "ban" if is_member else "hackban"),
                    moderator=ctx.author,
                    target=user,
                    reason=reason,
                    guild=ctx.guild,
                )
                banned.append(user)
        if failed:
            out.append(
                warning(
                    _(
                        "I failed to ban {failed, plural, one {# member} other {# members}}"
                        " out of the {total, plural, one {# member} other {# members}} specified."
                        " These may have failed due to a Discord issue, or be caused by the"
                        " server's role hierarchy."
                    ).format(failed=len(failed), total=len(users))
                )
            )
        if banned:
            out.append(
                success(
                    _("Banned {count, plural, one {# member} other {# members}}.").format(
                        count=len(banned)
                    )
                )
            )
        else:
            out.append(error(_("I couldn't ban any of the specified members.").format()))

        results = "\n".join(out)
        if len(results) < 2000:
            await original.edit(content=results)
        else:
            await original.delete()
            await ctx.send_pagified(results, reference=ctx.message)

    @commands.hybrid_command(aliases=["cleanup"])
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True, read_message_history=True)
    @app_commands.describe(
        limit="How far back in the channel history to look for messages to purge",
        users="Mentions or IDs of users to delete messages from",
        after="A link to a message to delete messages after",
        pinned="Include pinned messages in messages to be deleted",
    )
    @slash_guild
    async def purge(
        self,
        ctx: commands.Context,
        limit: commands.Range[int, 1, 100],
        users: commands.Greedy[commands.MemberOrID] = None,
        after: commands.PartialMessageConverter = None,
        pinned: bool = False,
    ):
        """Delete all messages within a certain limit of channel message history

        `limit` determines how many messages the bot will look back in the channel
        history to determine if they need to be deleted; not the total amount of
        messages that will be deleted.

        This command can only delete messages that were sent in the past 14 days.
        Pinned messages will not be deleted unless specifically requested.

        If `pinned` is `true`, then pinned messages will be included in the
        messages to be deleted.
        """
        if limit > 100:
            return await ctx.send_help()

        cutoff = ctx.message.created_at - timedelta(days=14)
        to_delete = []
        pins = await ctx.channel.pins()

        matched_too_old = 0
        async for msg in (
            ctx.channel.history(limit=limit, after=cast(discord.PartialMessage, after))
            if after
            else ctx.channel.history(limit=limit, before=ctx.message)
        ):
            if msg in pins and not pinned:
                continue
            if users and all(x.id != msg.author.id for x in users):
                continue
            if msg.created_at <= cutoff:
                matched_too_old += 1
                continue
            to_delete.append(msg)

        if len(to_delete) > 1:
            await ctx.channel.delete_messages(to_delete)
        elif len(to_delete) == 1:
            await to_delete[0].delete()
        else:
            # If no messages were deleted, but we found messages that would've otherwise
            # been deleted had they not been over 14 days old, send a message explicitly stating so
            if matched_too_old:
                await ctx.send(
                    warning(
                        _(
                            "None of those messages could be deleted; {count, plural, one "
                            " {# message} other {# messages}} within the given limit matched the"
                            " given criteria, but could not be deleted as it was sent over 14"
                            " days ago."
                        ).format(count=matched_too_old)
                    )
                )
            # Otherwise, there's genuinely just no messages that matched the given options.
            else:
                await ctx.send(warning(_("Found no messages to delete.").format()))
            return

        await ctx.send(
            success(
                _("Deleted {count, plural, one {# message} other {# messages}}.").format(
                    count=len(to_delete)
                )
            )
        )
