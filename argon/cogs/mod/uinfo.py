from datetime import datetime
from typing import Awaitable, Callable

import discord
from babel.lists import format_list
from discord import app_commands

from argon import commands
from argon.bot import Argon
from argon.cogs.mod._shared import _
from argon.translations import Humanize, get_babel_locale
from argon.utils import slash_guild
from argon.utils.chat_formatting import inline, translate_permission
from argon.utils.pronoundb import format_pronouns

KEY_PERMISSIONS = discord.Permissions(
    # General server permissions
    manage_channels=True,
    manage_roles=True,
    manage_emojis=True,
    view_audit_log=True,
    manage_webhooks=True,
    manage_guild=True,
    # Membership permissions
    manage_nicknames=True,
    kick_members=True,
    ban_members=True,
    moderate_members=True,
    # Text channel permissions
    mention_everyone=True,
    manage_messages=True,
    manage_threads=True,
    # Voice channel permissions
    mute_members=True,
    deafen_members=True,
    move_members=True,
    # Misc permissions
    manage_events=True,
    administrator=True,
)


class UserInfo:
    # defined in Mod
    bot: Argon
    get_names_and_nicks: Callable[[discord.abc.User], Awaitable[tuple[list[str], list[str]]]]

    async def __build_description(self, member: discord.Member) -> str | None:
        desc = []

        # bots can't have pronouns set, so avoid making a query for something we know
        # we won't get any useful information for
        if not member.bot:
            pronouns = await format_pronouns(member)
            if pronouns is None:
                desc.append(
                    _("\N{WAVING HAND SIGN} No pronouns set on [PronounDB]({url})").format(
                        url="https://pronoundb.org/"
                    )
                )
            else:
                desc.append(f"\N{WAVING HAND SIGN} {pronouns!s}")

        if member.bot:
            desc.append(_("\N{ROBOT FACE} Bot").format())
        elif await self.bot.is_owner(member):
            desc.append(_("\N{WRENCH} Bot Owner").format())

        if member.nick:
            desc.append(_("\N{LABEL} Nicknamed as {nick}").format(nick=member.nick))

        if member.voice:
            desc.append(
                _("\N{SPEAKER} In voice channel {channel}").format(
                    channel=member.voice.channel.mention
                )
            )

        if member.premium_since:
            desc.append(
                _("\N{WHITE MEDIUM STAR} Nitro boosting this server since {delta}").format(
                    delta=Humanize(member.premium_since, "R")
                )
            )

        if member.guild_avatar:
            desc.append(_("\N{FRAME WITH PICTURE} Has a server-specific avatar").format())

        desc.append(
            _("\N{SPIRAL CALENDAR PAD} Joined Discord {discord}, this server {guild}").format(
                discord=Humanize(member.created_at, "R"), guild=Humanize(member.joined_at, "R")
            )
        )

        return "\n".join(desc)

    async def send_whois(
        self, ctx: commands.Context, member: discord.Member, *, ephemeral: bool = False
    ):
        await ctx.defer(ephemeral=ephemeral, thinking=True)

        member = member or ctx.author
        embed = (
            discord.Embed(
                colour=member.colour if member.colour.value != 0 else None,
                description=await self.__build_description(member),
            )
            .set_thumbnail(url=str(member.display_avatar.with_static_format("png")))
            .set_author(
                name=str(member),
                icon_url=member.avatar if member.guild_avatar else None,
            )
        )

        now = datetime.utcnow()
        member_n = sorted(member.guild.members, key=lambda m: m.joined_at or now).index(member) + 1
        embed.set_footer(text=_("Member #{n} | User ID: {id}").format(n=member_n, id=member.id))

        roles = list(reversed([x.mention for x in member.roles if not x.is_default()]))
        cap = 40
        if len(roles) > cap:
            roles = [
                *roles[:cap],
                _("{num, plural, one {# more role} other {# more roles}}").format(
                    num=len(roles) - cap
                ),
            ]
        if roles:
            embed.add_field(
                name=_("Roles").format(),
                value=format_list(roles, locale=get_babel_locale()),
                inline=False,
            )

        if (
            key := discord.Permissions(
                ctx.channel.permissions_for(member).value & KEY_PERMISSIONS.value
            )
        ).value:
            value = (
                [inline(translate_permission("administrator"))]
                if key.administrator
                else [inline(translate_permission(k)) for k, v in key if v]
            )
            embed.add_field(name=_("Key Permissions").format(), value=_.humanize(value))

        names, nicks = await self.get_names_and_nicks(member)
        if names:
            embed.add_field(name=_("Past Names").format(), value=", ".join(names), inline=False)
        if nicks:
            embed.add_field(name=_("Past Nicknames").format(), value=", ".join(nicks), inline=False)

        await ctx.send(embed=embed, ephemeral=ephemeral)

    @commands.hybrid_command(aliases=["userinfo", "user"])
    @commands.guild_only()
    @commands.bot_has_permissions(embed_links=True)
    @slash_guild
    @app_commands.describe(member="The member to get info on, defaults to you")
    async def whois(self, ctx: commands.Context, *, member: discord.Member = None):
        """Get information on a given user"""
        await self.send_whois(ctx, member or ctx.author)
