from __future__ import annotations

from typing import Literal

import discord

from argon import db
from argon.bot import Argon
from argon.cogs.selfassign.message_types.core import RoleMessage
from argon.cogs.selfassign.shared import _, log
from argon.translations import Humanize
from argon.utils import MISSING
from argon.utils.chat_formatting import error, warning


class RoleButton(discord.ui.Button):
    __slots__ = ("role_id", "guild", "message")

    def __init__(
        self, *args, role_id: int, guild: discord.Guild, message: ButtonRoleMessage, **kwargs
    ):
        self.role_id = role_id
        self.guild = guild
        self.message = message
        super().__init__(*args, **kwargs)

    async def callback(self, interaction: discord.Interaction):
        log.debug("Processing button interaction %r", interaction)

        if not interaction.guild.me.guild_permissions.manage_roles:
            return await interaction.response.send_message(
                ephemeral=True,
                content=error(
                    _(
                        "I do not have Manage Roles permissions;"
                        " please ask an admin for assistance."
                    ).format()
                ),
            )

        role = self.guild.get_role(self.role_id)
        if not role:
            await interaction.response.send_message(
                ephemeral=True,
                content=error(
                    _(
                        "The role this button is attached to seems to not exist anymore."
                        " Ask an admin for assistance if you think this is in error."
                    ).format()
                ),
            )
            return

        roles = interaction.user.roles.copy()
        message = []
        removed = []
        if self.message.mutually_exclusive:
            # noinspection PyProtectedMember
            for other_role in self.message.roles.keys():
                other_role = self.guild.get_role(other_role)
                if other_role is None or other_role == role:
                    continue
                if other_role in roles:
                    removed.append(other_role)
                    roles.remove(other_role)

        if role in interaction.user.roles:
            if self.message.role_type(role) == "add":
                message.append(warning(_("You already have that role!").format()))
            else:
                roles.remove(role)
                removed.append(role)
        elif self.message.role_type(role) == "remove":
            message.append(warning(_("You do not have that role!").format()))
        else:
            roles.append(role)
            message.append(
                _("\N{INBOX TRAY} **Added** \N{EM DASH} {role}").format(role=role.mention)
            )

        if roles != interaction.user.roles:
            await interaction.user.edit(
                roles=roles,
                reason=_("Self-assignable roles in {channel}").format(
                    channel=interaction.channel.name
                ),
            )

        if removed:
            message.insert(
                0,
                _("\N{OUTBOX TRAY} **Removed** \N{EM DASH} {role}").format(
                    role=Humanize([x.mention for x in removed])
                ),
            )
        await interaction.response.send_message(content="\n".join(message), ephemeral=True)


class ButtonRoleMessage(RoleMessage):
    __slots__ = ()
    TYPE = "button"

    @classmethod
    def load(
        cls, bot: Argon, data: db.Message, *, webhook: discord.Webhook = None
    ) -> ButtonRoleMessage:
        self = cls(
            discord.PartialMessage(channel=bot.get_channel(data.channel_id), id=data.message_id),
            webhook=webhook,
        )
        return self.reload(bot, data)

    def reload(self, bot: Argon, data: db.Message) -> ButtonRoleMessage:
        self.mutually_exclusive = data.mutually_exclusive
        self.roles = {x["id"]: x for x in data.roles}
        self._build_view()
        return self

    def _build_view(self):
        if self.view.children:
            # Stop the existing view
            self.view.stop()
            # Create new view
            self.view = discord.ui.View(timeout=None)

        view = self.view
        for role in self.roles.values():
            button = RoleButton(
                label=role["label"],
                emoji=role["emoji"],
                style=getattr(discord.ButtonStyle, role["style"]),
                row=role["row"],
                custom_id=f"role_{self.message.id}__{role['id']}",
                role_id=role["id"],
                guild=self.message.guild,
                message=self,
            )
            view.add_item(button)

    def add_role(
        self,
        role: discord.Role,
        emoji: str | None = None,
        label: str = None,
        row: Literal[1, 2, 3, 4, 5] | None = None,
        role_type: Literal["add", "remove", "both"] = "both",
        style: discord.ButtonStyle = discord.ButtonStyle.grey,
        description: str = MISSING,
    ) -> None:
        super().add_role(role, emoji, label, row, role_type, style, description)
        self.view.add_item(
            RoleButton(
                label=label or role.name,
                emoji=emoji,
                style=style,
                row=row,
                role_id=role.id,
                custom_id=f"role_{self.message.id}__{role.id}",
                guild=self.message.guild,
                message=self,
            )
        )

    def remove_role(self, role: discord.Role) -> None:
        super().remove_role(role)
        try:
            self.view.remove_item(
                next(
                    x
                    for x in self.view.children
                    if isinstance(x, RoleButton) and x.role_id == role.id
                )
            )
        except StopIteration:
            pass
