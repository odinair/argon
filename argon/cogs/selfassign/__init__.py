from argon.bot import Argon
from argon.cogs.selfassign.core import SelfAssign


async def setup(bot: Argon):
    await bot.add_cog(SelfAssign(bot))
