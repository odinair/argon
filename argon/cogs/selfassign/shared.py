import logging

from argon.translations import Translator

_ = Translator(__file__)
log = logging.getLogger("argon.selfassign")
