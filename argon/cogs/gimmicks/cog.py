import os
from typing import ClassVar

import discord

from argon import commands


class Gimmicks(commands.Cog):
    GIMMICKS: ClassVar[list[str]] = os.environ.get("ARGON_GIMMICKY", "").lower().split(",")

    @commands.Cog.listener("on_message")
    async def shut_up_community_updates(self, message: discord.Message):
        if not message.guild:
            return
        if (
            # require that this feature is enabled
            "shutupcommunityupdates" not in self.GIMMICKS
            # and that we can send messages
            or not message.channel.permissions_for(message.guild.me).send_messages
        ):
            return
        # ensure that this is the guild's community updates channel
        if message.channel != message.guild.public_updates_channel:
            return
        if (
            # community update messages use the crossposting system
            message.flags.is_crossposted
            # and always come from a bot named Community Updates
            and message.author.name == "Community Updates"
            and message.author.bot
            # ... and this is required because the user ID is different per-guild.
        ):
            # if all of the above is true, then tell community updates to Shut Up
            await message.reply("shut up")
