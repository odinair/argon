from argon.bot import Argon


async def setup(bot: Argon):
    from .cog import Gimmicks

    await bot.add_cog(Gimmicks())
