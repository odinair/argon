# wtf is this?

This is a cog comprised of mostly gimmicky features that don't *really* belong here, but I had
no better place to put them. As such, this its own self-contained, opt-in cog.

There's a very strong chance you won't find anything here of use, unless you also find some of
the gimmicks here interesting.
