import logging
from copy import copy
from typing import Optional

import discord

from argon import commands, db
from argon.bot import Argon
from argon.translations import Translator
from argon.utils.chat_formatting import error, info, success, warning
from argon.utils.menus import ConfirmMenu

_ = Translator(__file__)
log = logging.getLogger("argon.alias")


class Alias(commands.Cog):
    """Alias commands and blocks of text to commands"""

    def __init__(self, bot: Argon):
        self.bot = bot
        super().__init__()

    @commands.group()
    @commands.guild_only()
    @commands.has_guild_permissions(manage_messages=True)
    async def alias(self, ctx: commands.Context):
        """Manage command aliases"""

    @alias.command()
    async def list(self, ctx: commands.Context):
        """List all configured aliases"""
        aliases = [x async for x in db.Alias.find_many(db.Alias.guild_id == ctx.guild.id)]
        if not aliases:
            await ctx.send(warning(_("There's no aliases in this server.").format()))
            return
        alias_list = ", ".join(f"`{x}`" for x in aliases)
        await ctx.send_pagified(
            alias_list,
            embed=discord.Embed(
                colour=ctx.embed_colour,
                title=_("{count, plural, one {# Alias} other {# Aliases}} for {guild}").format(
                    guild=ctx.guild.name, count=len(aliases)
                ),
            ),
        )

    @alias.command()
    async def show(self, ctx: commands.Context, alias: str):
        """Show what command an alias directs to"""
        alias = alias.casefold()
        alias = await db.Alias.find_one(
            db.Alias.guild_id == ctx.guild.id and db.Alias.name == alias
        )
        if not alias:
            await ctx.send(
                warning(_("`{alias}` is not a registered alias").format(alias=alias)),
                allowed_mentions=discord.AllowedMentions.none(),
            )
            return

        if alias.type == db.AliasType.TEXT:
            await ctx.send(info(_("`{alias}` is a text alias.").format(alias=alias.name)))
        elif alias.type == db.AliasType.COMMAND:
            await ctx.send(
                info(
                    _("`{alias}` is an alias to `{command}`").format(
                        alias=alias.name, command=alias.command
                    )
                ),
                allowed_mentions=discord.AllowedMentions.none(),
            )

    @alias.group(aliases=["new", "create"])
    async def add(self, ctx: commands.Context):
        """Create a new alias

        Created aliases are case sensitive.
        """

    @add.command(name="command", aliases=["c"])
    async def add_command(self, ctx: commands.Context, alias: str, *, command: str):
        """Add a command alias"""
        if not self.bot.get_command(command.split()[0]):
            await ctx.send(warning(_("That isn't a registered command.").format()))
            return
        alias = alias.casefold()
        if self.bot.get_command(alias):
            await ctx.send(
                warning(_("`{alias}` is already used by a command").format(alias=alias)),
                allowed_mentions=discord.AllowedMentions.none(),
            )
            return
        al: Optional[db.Alias] = await db.Alias.find_one({"guild_id": ctx.guild.id, "name": alias})
        if al:
            if al.type != db.AliasType.COMMAND:
                return await ctx.send(
                    error(
                        _("`{alias}` already exists and is not a command alias").format(alias=alias)
                    )
                )
            if not await ConfirmMenu(ctx.author).prompt(
                ctx,
                _("`{alias}` already exists; would you like to update it?").format(alias=alias),
                delete_after=True,
            ):
                await ctx.send(warning(_("Not updating alias.").format()))
                return
            await al.set({"command": command})
        else:
            # pydantic doesn't like using the actual enum here, so we have to settle for using
            # the enum value instead
            await db.Alias(type=1, name=alias, command=command, guild_id=ctx.guild.id).insert()
        await ctx.send(
            success(
                _("`{alias}` is now an alias for `{command}`.").format(alias=alias, command=command)
            )
        )

    @add.command(name="text", aliases=["t"])
    async def add_text(self, ctx: commands.Context, alias: str, *, text: str):
        """Add an alias that returns the given text when used"""
        if len(text) > 2000:
            await ctx.send(
                error(_("Text aliases must be at most 2,000 characters in length").format())
            )
            return
        alias = alias.casefold()
        if self.bot.get_command(alias):
            await ctx.send(
                warning(_("`{alias}` is already used by a command").format(alias=alias)),
                allowed_mentions=discord.AllowedMentions.none(),
            )
            return
        al = await db.Alias.find_one(db.Alias.guild_id == ctx.guild.id and db.Alias.name == alias)
        if al:
            if al.type != db.AliasType.TEXT:
                await ctx.send(
                    error(_("`{alias}` already exists and is not a text alias").format(alias=alias))
                )
                return

            if not await ConfirmMenu(ctx.author).prompt(
                ctx,
                info(
                    _(
                        "`{alias}` already exists; would you like to edit it with the given text?"
                    ).format(alias=alias)
                ),
                delete_after=True,
            ):
                await ctx.send(warning(_("Not updating alias.").format()))
                return

            await al.set({"text": text})
            await ctx.send(success(_("Updated text alias `{alias}`.").format(alias=alias)))
        else:
            await db.Alias(type=0, name=alias, text=text, guild_id=ctx.guild.id).insert()
            await ctx.send(success(_("Added text alias `{alias}`.").format(alias=alias)))

    @alias.command(aliases=["delete"])
    async def remove(self, ctx: commands.Context, alias: str):
        """Remove an alias"""
        alias = alias.casefold()
        alias = await db.Alias.find_one(
            db.Alias.guild_id == ctx.guild.id and db.Alias.name == alias
        )
        if not alias:
            await ctx.send(
                warning(_("`{alias}` is not a registered alias").format(alias=alias)),
                allowed_mentions=discord.AllowedMentions.none(),
            )
            return
        await alias.delete()
        await ctx.send(success(_("`{alias}` is no longer an alias.").format(alias=alias)))

    async def process_aliases(self, message: discord.Message):
        if (
            not message.content
            or not message.guild
            or message.author.bot
            or await self.bot.is_blocked(message.author)
        ):
            self.bot.dispatch("message_without_command", message)
            return

        aliases = {x.name: x async for x in db.Alias.find_many({"guild_id": message.guild.id})}
        prefixes = await self.bot.prefixes(..., message)
        try:
            prefix = next(
                x for x in sorted(prefixes, key=len, reverse=True) if message.content.startswith(x)
            )
        except StopIteration:
            self.bot.dispatch("message_without_command", message)
            return
        try:
            command = message.content.replace(prefix, "", 1).split()[0]
        except IndexError:
            log.error("Failed to get possible alias command from message %r", message.content)
            command = ""
        try:
            alias = aliases[command]
        except KeyError:
            log.debug("No such alias %r (%r)", command, aliases.keys())
            self.bot.dispatch("message_without_command", message)
            return

        if alias.type == db.AliasType.TEXT:
            await message.reply(alias.text, allowed_mentions=discord.AllowedMentions.none())
        elif alias.type == db.AliasType.COMMAND:
            new_message = copy(message)
            new_message.content = message.content.replace(
                f"{prefix}{command}", f"{prefix}{alias.command}", 1
            )
            await self.bot.process_commands(new_message, dispatch_without_command=False)
        else:
            log.warning(
                "Unrecognized alias type %r (%r in guild %r)",
                alias.type,
                alias.name,
                alias.guild_id,
            )
