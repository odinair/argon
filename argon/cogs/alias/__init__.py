from argon.bot import Argon
from argon.cogs.alias.core import Alias


async def setup(bot: Argon):
    await bot.add_cog(Alias(bot))
