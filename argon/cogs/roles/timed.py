import asyncio
from datetime import datetime
from math import ceil
from typing import Union

import discord
from discord.ext.tasks import loop

from argon import commands, db
from argon.bot import Argon
from argon.cogs.roles._shared import _, log
from argon.translations import Humanize
from argon.utils import format_audit_reason
from argon.utils.chat_formatting import deny, error, timestamp
from argon.utils.embeds import mod_action
from argon.utils.menus import PaginatorView
from argon.utils.time import FutureTime


class TimedRole:
    bot: Argon

    @commands.group()
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def timedrole(self, ctx: commands.Context):
        """Manage timed roles

        Unlike mutes and personal roles, timed roles are *not* implicitly
        sticky, and instead must be set as such with `[p]stickyroles`.
        """

    @staticmethod
    async def __list_all_roles(ctx: commands.Context):
        roles: dict[Union[discord.Role, int], int] = {}
        async for member in db.Member.find_many(
            {"temp_roles": {"$exists": True, "$ne": []}}, projection_model=db.Member
        ):
            for role in member.temp_roles:
                roles.setdefault(role["id"], 0)
                roles[role["id"]] += 1
        if not roles:
            return await ctx.send(error(_("There's no timed roles in this server.").format()))
        roles = {role: count for rid, count in roles.items() if (role := ctx.guild.get_role(rid))}
        base_embed: discord.Embed = discord.Embed(
            colour=ctx.embed_colour,
            description=_(
                "There {count, plural, one {is `#` timed role} other {are `#` timed roles}}"
                " currently active in this server"
            ).format(count=len(roles)),
        ).set_author(
            name=_("Timed roles in {guild}").format(guild=ctx.guild),
            icon_url=ctx.guild.icon,  # noqa
        )
        page_count = ceil(len(roles) / 5)
        pages = []
        for index, page in enumerate(discord.utils.as_chunks([*roles.items()], 5), start=1):
            embed = base_embed.copy()
            embed.set_footer(
                text=_("Page {current}/{total}").format(current=index, total=page_count)
            )
            for role, count in page:
                embed.add_field(
                    name="\N{ZERO WIDTH JOINER}",
                    value=_(
                        "**Role** \N{EM DASH} {role}\n"
                        "**Applied to** \N{EM DASH} {count, plural, one {`#` member}"
                        " other {`#` members}}"
                    ).format(role=role, count=count),
                )
            pages.append(embed)
        await ctx.send(**await PaginatorView(ctx.author, pages).send_kwargs())

    @staticmethod
    async def __list_role(ctx: commands.Context, role: discord.Role):
        members = await db.Member.find_many(
            {"temp_roles.id": role.id}, projection_model=db.Member
        ).to_list()
        if not members:
            return await ctx.send(error(_("Nobody has that role as a timed role.").format()))
        base_embed: discord.Embed = discord.Embed(
            colour=ctx.embed_colour,
            description=_(
                "{count, plural, one {`#` member has} other {`#` members have}} {role}"
            ).format(count=len(members), role=role),
        ).set_author(
            name=_("Members with {role}").format(role=role.name),
            icon_url=ctx.guild.icon,  # noqa
        )
        pages = []
        page_count = ceil(len(members) / 10)
        for index, page in enumerate(discord.utils.as_chunks(members, 10), start=1):
            embed = base_embed.copy()
            embed.set_footer(
                text=_("Page {current}/{total}").format(current=index, total=page_count)
            )
            desc = []
            for member in page:
                rd = next(x for x in member.temp_roles if x["id"] == role.id)
                desc.append(
                    f"<@{member.user_id}> \N{EM DASH} expires {timestamp(rd['expiry'], 'R')}"
                )
            embed.add_field(name="\N{ZERO WIDTH JOINER}", value="\n".join(desc))
            pages.append(embed)
        await ctx.send(**await PaginatorView(ctx.author, pages).send_kwargs())

    @staticmethod
    async def __list_member(ctx: commands.Context, member: discord.Member):
        data = await db.Member.find_one_or_create(member)
        if not data.temp_roles:
            return await ctx.send(
                error(_("{member} doesn't have any timed roles.").format(member=member))
            )
        base_embed: discord.Embed = discord.Embed(
            colour=ctx.embed_colour,
            description=_(
                "{member} has {count, plural, one {`#` timed role} other {`#` timed roles}}"
            ).format(member=member, count=len(data.temp_roles)),
        ).set_author(
            name=_("{member}'s timed roles").format(member=member.display_name),
            icon_url=ctx.guild.icon,  # noqa
        )
        pages = []
        page_count = ceil(len(data.temp_roles) / 4)
        for index, page in enumerate(discord.utils.as_chunks(data.temp_roles, 4), start=1):
            embed = base_embed.copy()
            embed.set_footer(
                text=_("Page {current}/{total}").format(current=index, total=page_count)
            )
            for role in page:
                embed.add_field(
                    name="\N{ZERO WIDTH JOINER}",
                    value=_(
                        "**Role** \N{EM DASH} {role}\n"
                        "**Expires** \N{EM DASH} {delta}\n"
                        "**Added by** \N{EM DASH} {mod}\n"
                        "**Reason** \N{EM DASH} {reason}"
                    ).format(
                        role=ctx.guild.get_role(role["id"]),
                        delta=timestamp(role["expiry"], "R"),
                        mod=f"<@{role['added_by']}>" if "added_by" in role else _("*unknown*"),
                        reason=role.get("reason", _("*no reason specified*")),
                    ),
                )
            pages.append(embed)
        await ctx.send(**await PaginatorView(ctx.author, pages).send_kwargs())

    @timedrole.command(name="list", usage="[member/role]")
    async def _tr_list(
        self, ctx: commands.Context, *, value: Union[discord.Member, discord.Role] = None
    ):
        """List all timed roles for this server or a single member"""
        if not value:
            await self.__list_all_roles(ctx)
        elif isinstance(value, discord.Role):
            await self.__list_role(ctx, value)
        else:
            await self.__list_member(ctx, value)

    @timedrole.command(name="add")
    async def _tr_add(
        self,
        ctx: commands.Context,
        member: discord.Member,
        role: discord.Role,
        duration: FutureTime,
        *,
        reason: str = None,
    ):
        """Grant a role to a member for a limited amount of time

        Added timed roles may be removed by simply removing them from the member like
        with any other role.
        """
        if role >= ctx.me.top_role:
            return await ctx.send(
                deny(
                    _(
                        "That role cannot be given as a timed role, as it is above my topmost role."
                    ).format()
                )
            )
        if role in member.roles:
            return await ctx.send(
                error(
                    _("{member} already has {role} assigned to them.").format(
                        member=member, role=role
                    )
                )
            )
        data = await db.Member.find_one_or_create(member)
        if any(x["id"] == role.id for x in data.temp_roles):
            return await ctx.send(
                error(
                    _(
                        "{member} already has {role} assigned to them as a temporary role,"
                        " but does not possess it."
                    ).format(member=member, role=role)
                )
            )
        expiry = datetime.utcnow() + duration

        tr = {"id": role.id, "expiry": expiry, "added_by": ctx.author.id}
        if reason:
            tr["reason"] = reason

        await data.set({"temp_roles": [*data.temp_roles, tr]})
        await member.add_roles(role, reason=format_audit_reason(ctx.author, reason))
        await ctx.send(
            embed=mod_action(
                _("Added {role} to {member} until {expiry}").format(
                    member=member, role=role, expiry=Humanize(expiry)
                ),
                reason,
            )
        )

    @commands.Cog.listener("on_member_update")
    async def _timed_role_remove_listener(self, before: discord.Member, after: discord.Member):
        if before.roles == after.roles:
            return

        removed = {x.id: x for x in set.difference({*before.roles}, {*after.roles})}
        if not removed:
            return

        data = await db.Member.find_one({"user_id": after.id, "guild_id": after.guild.id})
        if not data:
            return
        removed_temp = False
        roles = data.temp_roles.copy()
        for role in data.temp_roles:
            if role["id"] in removed:
                log.debug("Removed timed role %r from member %r", role, after)
                removed_temp = True
                roles.remove(role)
        if removed_temp:
            await data.set({"temp_roles": roles})

    @loop(minutes=1)
    async def _expire_timed_roles(self):
        log.debug("Checking for expired timed roles")
        now = datetime.utcnow()
        _index = 0
        async for member in db.Member.find_many({"temp_roles.expiry": {"$lt": now}}):
            if _index >= 10:
                await asyncio.sleep(0)
                _index = 0
            _index += 1

            member: db.Member = member
            guild: discord.Guild = self.bot.get_guild(member.guild_id)
            if not guild or guild.unavailable:
                continue
            user = guild.get_member(member.user_id)
            if not user:
                continue

            expired = [x for x in member.temp_roles if x["expiry"] <= now]
            log.debug(
                "%s (guild %s) has %s expired role(s): %r",
                member.user_id,
                member.guild_id,
                len(expired),
                expired,
            )

            to_remove = [
                r
                for x in expired
                if (r := guild.get_role(x["id"])) and r in user.roles and guild.me.top_role > r
            ]
            if to_remove:
                # Removing the timed role from the database is handled by the role remove
                # event listener, so as to avoid having to duplicate the code to handle that.
                await user.remove_roles(
                    *to_remove,
                    reason=_(
                        "Removing {count, plural, one {expired time role}"
                        " other {# expired timed roles}}"
                    ).format(count=len(to_remove)),
                )
