from typing import Optional

import discord

from argon import commands, db
from argon.cogs.roles._shared import _, log
from argon.utils.chat_formatting import success, warning


class VCRole:
    _vc_roles: dict[int, Optional[int]]

    @commands.group()
    @commands.has_guild_permissions(manage_roles=True)
    async def vcrole(self, ctx: commands.Context):
        """Manage voice channel roles"""

    @vcrole.command()
    async def list(self, ctx: commands.Context):
        """List all voice channels with roles"""
        # noinspection PyUnboundLocalVariable
        channels: list[tuple[discord.VoiceChannel, discord.Role]] = [
            (c, r)
            async for x in db.Channel.find_many(db.Channel.guild_id == ctx.guild.id)
            if (c := ctx.guild.get_channel(x.channel_id))
            and isinstance(c, discord.VoiceChannel)
            and x.vc_role
            and (r := ctx.guild.get_role(x.vc_role))
        ]
        if not channels:
            return await ctx.send(
                warning(_("There's no voice channels with linked roles in this server.").format())
            )
        await ctx.send_pagified(
            "".join(f"{c.mention} \N{EM DASH} {r.mention}" for c, r in channels),
            embed=discord.Embed(
                colour=ctx.embed_colour,
                title=_("Voice Roles for {guild}").format(guild=ctx.guild.name),
            ),
        )

    @vcrole.command()
    async def link(
        self, ctx: commands.Context, voice_channel: discord.VoiceChannel, role: discord.Role
    ):
        """Link a voice channel to a role

        Members who join the given voice channel will be granted this role upon joining,
        and similarly it will be revoked when they leave.
        """
        if role >= ctx.me.top_role:
            await ctx.send(
                warning(
                    _(
                        "That role is equal to or above my highest role; move it lower in"
                        "the role hierarchy, or move my topmost role above it."
                    ).format()
                )
            )
            return
        channel = await db.Channel.find_one_or_create(voice_channel)
        await channel.set({"vc_role": role.id})
        self._vc_roles[voice_channel.id] = role.id
        await ctx.send(
            success(
                _("Members who join {channel} will now receive the {role} role.").format(
                    channel=voice_channel.mention, role=role.mention
                )
            ),
            allowed_mentions=discord.AllowedMentions.none(),
        )

    @vcrole.command()
    async def unlink(self, ctx: commands.Context, voice_channel: discord.VoiceChannel):
        """Unlink a voice channel's role"""
        channel = await db.Channel.find_one_or_create(voice_channel)
        await channel.set({"vc_role": None})
        self._vc_roles[voice_channel.id] = None
        await ctx.send(
            success(
                _("Members who join {channel} will no longer receive a role.").format(
                    channel=voice_channel.mention
                )
            ),
            allowed_mentions=discord.AllowedMentions.none(),
        )

    @commands.Cog.listener()
    async def on_voice_state_update(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if before.channel == after.channel:
            return
        if after.channel:
            if after.channel.id not in self._vc_roles:
                self._vc_roles[after.channel.id] = (
                    await db.Channel.find_one_or_create(after.channel)
                ).vc_role
            if role := member.guild.get_role(self._vc_roles[after.channel.id]):
                try:
                    await member.add_roles(role, reason=_("Voice channel role").format())
                except discord.HTTPException:
                    log.exception("Failed to add voice role to %r", member)

        if before.channel:
            if before.channel.id not in self._vc_roles:
                self._vc_roles[before.channel.id] = (
                    await db.Channel.find_one_or_create(before.channel)
                ).vc_role
            if (
                role := member.guild.get_role(self._vc_roles[before.channel.id])
            ) and role in member.roles:
                try:
                    await member.remove_roles(role, reason=_("Voice channel role").format())
                except discord.HTTPException:
                    log.exception("Failed to remove voice role from %r", member)
