from argon.bot import Argon

from .core import Roles


async def setup(bot: Argon):
    cog = Roles(bot)
    await bot.add_cog(cog)
