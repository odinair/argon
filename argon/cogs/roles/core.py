from argon import commands
from argon.bot import Argon

from ._shared import _
from .personalrole import PersonalRole
from .sticky import StickyRoles
from .timed import TimedRole
from .vcrole import VCRole


@_.cog()
class Roles(commands.Cog, PersonalRole, VCRole, StickyRoles, TimedRole):
    def __init__(self, bot: Argon):
        super().__init__()
        self.bot = bot
        self._vc_roles = {}  # used by VCRole

    def cog_unload(self):
        self._expire_timed_roles.cancel()
