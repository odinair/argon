import asyncio

import discord

from argon import commands, db
from argon.bot import Argon
from argon.cogs.roles._shared import _, log
from argon.utils.chat_formatting import error, info, success, warning


def has_role_icons():
    def predicate(ctx: commands.Context):
        if not ctx.guild:
            raise commands.NoPrivateMessage()
        if "ROLE_ICONS" not in ctx.guild.features:
            raise commands.CheckFailure(
                error(_("This server doesn't have access to role icons").format())
            )
        return True

    return commands.check(predicate)


class PersonalRole:
    bot: Argon

    @commands.group()
    @commands.guild_only()
    @commands.bot_has_permissions(manage_roles=True)
    async def myrole(self, ctx: commands.Context):
        """Modify your personal role"""

    @myrole.command(name="assign")
    @commands.has_guild_permissions(manage_roles=True)
    async def myrole_assign(
        self, ctx: commands.Context, member: discord.Member, role: discord.Role
    ):
        """Assign a personal role to a member"""
        data = await db.Member.find_one_or_create(member)
        await data.set({"personal_role": role.id})
        if role not in member.roles:
            await member.add_roles(role, reason=_("Assigning personal role").format())
        await ctx.send(
            success(
                _("{user} is now assigned to role {role}").format(
                    user=member.mention, role=role.mention
                )
            ),
            allowed_mentions=discord.AllowedMentions.none(),
        )

    @myrole.command(name="unassign")
    @commands.has_guild_permissions(manage_roles=True)
    async def myrole_unassign(self, ctx: commands.Context, member: discord.Member):
        """Unassign a member's personal role"""
        data = await db.Member.find_one_or_create(member)
        if not data.personal_role:
            return await ctx.send(
                warning(
                    _("{user} doesn't have a personal role assigned.").format(user=member.mention),
                ),
                allowed_mentions=discord.AllowedMentions.none(),
            )
        await data.set({"personal_role": None})
        await ctx.send(
            success(_("Unassigned {user}'s personal role.").format(user=member.mention)),
            allowed_mentions=discord.AllowedMentions.none(),
        )

    @myrole.command(name="colour")
    @commands.cooldown(rate=1, per=30, type=commands.BucketType.user)
    async def myrole_colour(self, ctx: commands.Context, colour: discord.Colour):
        """Change your personal role colour"""
        member = await db.Member.find_one_or_create(ctx.author)
        role = ctx.guild.get_role(member.personal_role)
        if not role:
            return await ctx.send(warning(_("You don't have a role you can modify!").format()))
        before = role.colour
        try:
            await role.edit(
                colour=colour, reason=_("Change requested by {user}").format(user=str(ctx.author))
            )
        except discord.HTTPException:
            await ctx.send(
                error(
                    _(
                        "Failed to edit your role; ask an admin for assistance if this persists."
                    ).format()
                )
            )
        else:
            self.bot.dispatch("personal_role_colour", ctx.author, before, colour)
            await ctx.send(success(_("Your role colour is now `{colour}`.").format(colour=colour)))

    @myrole.command(name="name")
    @commands.cooldown(rate=1, per=30, type=commands.BucketType.user)
    async def myrole_name(self, ctx: commands.Context, *, name: str):
        """Change your personal role name"""
        member = await db.Member.find_one_or_create(ctx.author)
        role = ctx.guild.get_role(member.personal_role)
        if not role:
            return await ctx.send(warning(_("You don't have a role you can modify!").format()))
        before = role.name
        try:
            await role.edit(
                name=name, reason=_("Change requested by {user}").format(user=str(ctx.author))
            )
        except discord.HTTPException:
            await ctx.send(
                error(
                    _(
                        "Failed to edit your role; ask an admin for assistance if this persists."
                    ).format()
                )
            )
        else:
            self.bot.dispatch("personal_role_name", ctx.author, before, name)
            await ctx.send(
                success(
                    _("Your role name is now `{name}`.").format(
                        name=discord.utils.escape_markdown(name)
                    )
                ),
                allowed_mentions=discord.AllowedMentions.none(),
            )

    @myrole.command(name="icon")
    @commands.cooldown(rate=1, per=60, type=commands.BucketType.user)
    @has_role_icons()
    async def myrole_icon(self, ctx: commands.Context):
        """Modify your personal role icon"""
        member = await db.Member.find_one_or_create(ctx.author)
        role = ctx.guild.get_role(member.personal_role)
        if not role:
            return await ctx.send(warning(_("You don't have a role you can modify!").format()))

        if not ctx.message.attachments:
            temp = await ctx.send(
                info(_("Please upload the image you would like to use for your role icon").format())
            )
            try:
                message = await self.bot.wait_for(
                    "message",
                    check=lambda m: m.author == ctx.author
                    and m.channel == ctx.channel
                    and m.attachments,
                    timeout=55.0,
                )
            except asyncio.TimeoutError:
                await temp.delete()
                await ctx.send(
                    warning(
                        _("Timed out; re-run this command if you would like to try again.").format()
                    )
                )
                return
        else:
            message = ctx.message

        attachment = next(
            iter(x for x in message.attachments if x.content_type in ("image/png", "image/jpeg")),
            None,
        )
        if not attachment:
            ctx.command.reset_cooldown(ctx)
            await ctx.send(
                warning(_("The given icon must be either a `png` or `jpg`/`jpeg` file").format())
            )
            return
        if attachment.size > (256 * 1024):
            ctx.command.reset_cooldown(ctx)
            await ctx.send(warning(_("Icons must be at most 256KB in size").format()))
            return

        try:
            await role.edit(display_icon=await attachment.read())
        except discord.HTTPException:
            log.exception("Failed to update role icon for member %r", ctx.author)
            await ctx.send(
                warning(
                    _(
                        "Failed to update your role icon; ask an admin for assistance"
                        " if this persists."
                    ).format()
                )
            )
        else:
            await ctx.send(success(_("Updated your role icon.").format()))
