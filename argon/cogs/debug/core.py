import os
from typing import Literal

import discord
from discord import app_commands

from argon import commands
from argon.utils import slash_guild


def debug_guild(func):
    if "DEBUG_COG_GUILD" in os.environ:
        return app_commands.guilds(int(os.environ["DEBUG_COG_GUILD"]))
    return slash_guild(func)


class Debug(commands.Cog):
    @commands.hybrid_group()
    @commands.is_owner()
    @debug_guild
    async def debug(self, ctx: commands.Context):
        """Debug commands"""

    @debug.command()
    @commands.bot_has_guild_permissions(administrator=True)
    async def check_failure(self, ctx: commands.Context):
        """Debug check failure"""
        # if we ever get to this point, then either checks don't work, or the bot has admin
        # permissions, and one of these outcomes are significantly more likely than the other,
        # so...
        await ctx.send(
            "**Why are you giving this bot Administrator permissions? Stop that."
            " Don't give bots Administrator permissions.**"
        )

    @debug.command()
    async def int_literal(self, ctx: commands.Context, value: Literal[1, 2]):
        await ctx.send(f"{type(value)}")

    @debug.command()
    async def member(self, ctx: commands.Context, member: discord.Member):
        await ctx.send(embed=discord.Embed(description=str(member)))
