from argon.bot import Argon
from argon.cogs.debug.core import Debug
from argon.utils import SLASH_GUILD


async def setup(bot: Argon):
    await bot.add_cog(Debug(), guild=SLASH_GUILD)
