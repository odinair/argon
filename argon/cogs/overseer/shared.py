from __future__ import annotations

import logging
from typing import MutableMapping
from weakref import WeakValueDictionary

import discord

from argon.bot import Argon
from argon.cogs.overseer.abc import ModuleABC
from argon.translations import Translator

__all__ = ("icu", "config", "log", "bot", "modules")
bot: Argon = ...
log = logging.getLogger("argon.overseer")
icu = _ = Translator(__file__)
modules: MutableMapping[str, ModuleABC] = WeakValueDictionary()
_unknown_member = _("`an unknown member`")


# noinspection PyUnusedLocal
@icu.transformer
def transform_dpy_objects(kwargs: dict, locale: str = None, /) -> dict:
    from argon.cogs.overseer.audit import AuditResult, dummy_member

    modified = {}
    for k, v in kwargs.items():
        if isinstance(v, AuditResult):
            modified[k] = _unknown_member if v.member is dummy_member else v.mention
        elif isinstance(v, discord.Role):
            # Using .mention for @everyone produces a mention of '@@everyone', which just
            # looks wrong. This is safe to do as we're already suppressing all mentions
            # in event logging, with no way for events to override it without overriding methods.
            modified[k] = v.name if v.is_default() else v.mention
        elif isinstance(v, discord.abc.GuildChannel):
            modified[k] = v.mention
        elif isinstance(v, discord.abc.User):
            modified[k] = f"{v.mention} ({discord.utils.escape_markdown(str(v))} `{v.id}`)"
        elif isinstance(v, discord.Guild):
            modified[k] = v.name
    return modified
