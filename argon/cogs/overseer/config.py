"""Overseer module/event config storage

This module is responsible for loading and caching guild configurations.
"""

from __future__ import annotations

import enum
from contextlib import suppress
from dataclasses import dataclass
from typing import ClassVar, overload

import discord

from argon import db
from argon.cogs.overseer import shared
from argon.cogs.overseer.abc import EventABC, ModuleABC
from argon.cogs.overseer.shared import log
from argon.utils import flatten

__all__ = ("GuildConfig", "EventStatus")


# If you ever add new enum types to this, remember to also add relevant strings for it
# in .cog.event_list and .cog.overseer
class EventStatus(enum.Enum):
    DISABLED = enum.auto()
    """Event is disabled"""

    ENABLED = enum.auto()
    """Event is enabled"""

    ENABLED_WITH_OVERRIDE_CHANNEL = enum.auto()
    """Event is enabled, and overrides the module-level channel"""

    ENABLED_WITHOUT_CHANNEL = enum.auto()
    """Event is enabled, but no destination channel is set"""

    ENABLED_MUTUAL_SUPPRESSED = enum.auto()
    """Event is enabled, but suppressed by a higher priority event in the same mutually
    exclusive event group"""

    ENABLED_MUTUAL_SUPPRESSED_OVERRIDE = enum.auto()
    """Combination of :attr:`ENABLED_WITH_OVERRIDE_CHANNEL` and :attr:`ENABLED_MUTUAL_SUPPRESSED`;
    such events are still treated the same as suppressed events and won't log anything."""


class GuildConfig:
    __slots__ = ("guild", "_cache")
    __guild_cache: ClassVar[dict[discord.Guild, GuildConfig]] = {}

    def __init__(self, guild: discord.Guild):
        self.guild = guild
        self._cache: dict = ...
        """
        The general configuration layout for Overseer is as follows::

            {
                _audit: bool = False,
                _prefer_embeds: bool = True,
                # during runtime this is a set, but for storage this is converted to a list
                _suppress: Set[int] = set(),
    
                <module id>: {
                    events: {
                        <event id>: {
                            enabled: bool = False,
                            # if not set, this defaults to the module-level channel
                            channel: int? = None
                        }
                    },
                    # if not set then all events that don't have a `channel` override
                    # never log anything, even if they'd otherwise be enabled
                    channel: int? = None
                }
            }
        """

    # Convenience method to allow for `await GuildConfig.get()` instead of
    # `await GuildConfig.get().load()`
    def __await__(self):
        return self.load().__await__()

    @classmethod
    def get(cls, guild: GuildConfig | discord.Guild) -> GuildConfig:
        """Get a :class:`GuildConfig` object for a given :class:`discord.Guild`

        If the given value is already a :class:`GuildConfig`, this method simply returns
        the given value, and is comparable to a no-op.
        """
        if isinstance(guild, GuildConfig):
            return guild
        if not isinstance(guild, discord.Guild):
            raise TypeError("GuildConfig.get() only accepts Guild values")
        if guild not in cls.__guild_cache:
            cls.__guild_cache[guild] = cls(guild)
        return cls.__guild_cache[guild]

    @classmethod
    def remove_from_cache(cls, guild: discord.Guild) -> None:
        """Remove a :class:`discord.Guild` from the config cache"""
        with suppress(KeyError):
            del cls.__guild_cache[guild]

    def __repr__(self):
        return f"<{type(self).__name__} guild={self.guild!r} cache_loaded={bool(self._cache)}>"

    @overload
    def __getitem__(self, item: ModuleABC) -> ModuleConfig:
        ...

    @overload
    def __getitem__(self, item: EventABC) -> EventConfig:
        ...

    def __getitem__(self, item):
        if not self.cache:
            raise RuntimeError("Use GuildConfig#load before retrieving modules/events")
        if isinstance(item, EventABC):
            return self[item.module][item]
        if not isinstance(item, ModuleABC):
            raise TypeError("GuildConfig only supports Module or Event keys")
        return ModuleConfig(self, item)

    @property
    def cache(self) -> dict | None:
        return self._cache if self._cache is not ... else None

    @property
    def prefer_embeds(self) -> bool:
        """Whether embeds should be preferred wherever possible"""
        return self.cache.get("_prefer_embeds", True)

    @prefer_embeds.setter
    def prefer_embeds(self, value: bool):
        self.cache["_prefer_embeds"] = value

    @property
    def audit_enabled(self) -> bool:
        """Whether to retrieve audit log information to include in logging"""
        return self.cache.get("_audit", True)

    @audit_enabled.setter
    def audit_enabled(self, value: bool):
        self.cache["_audit"] = value

    @property
    def suppress(self) -> set[int]:
        """Which IDs should be suppressed from logging

        IDs in this set may represent any of either members, roles, or channels.

        Category channels also implicitly suppress channels contained in it.

        Roles do not implicitly suppress members who have it, but do
        suppress logging of member role updates that involve
        such roles.
        """
        try:
            return self.cache["_suppress"]
        except KeyError:
            suppressed = self.cache["_suppress"] = set()
            return suppressed

    def reset(self, reset_all=False) -> None:
        """Reset configuration for the current guild

        If ``reset_all`` is :obj:`True`, all configuration options are reset;
        this includes options such as :prop:`suppress`.
        """
        if reset_all is True:
            self._cache = {}
        elif not self.cache:
            raise RuntimeError("Cannot reset before the guild's data is loaded into memory")
        else:
            # Preserve simple config values like audit and embeds while resetting module configs
            self._cache = {k: v for k, v in self.cache.items() if k.startswith("_")}

    async def load(self) -> GuildConfig:
        """Load the guild data from Config

        This can also be done by simply awaiting the :class:`GuildConfig` object itself.
        """
        if not self.cache:
            self._cache = (await db.Guild.find_one_or_create(self.guild)).overseer
            self.cache["_suppress"] = set(self.cache.get("_suppress", set()))
            log.debug("Loaded config for guild %r", self.guild)
            log.debug("Config: %r", self._cache)
        return self

    async def save(self) -> None:
        """Save the guild config"""
        if self.cache is ...:
            raise RuntimeError("Cannot save before the guild's data is loaded into memory")
        data = self.cache.copy()
        data["_suppress"] = list(data.get("_suppress", []))
        await db.Guild.find_one({"guild_id": self.guild.id}).set({"overseer": data})
        log.debug("Saved config for guild %r", self.guild)


@dataclass(slots=True)
class ModuleConfig:
    guild: GuildConfig
    module: ModuleABC

    def __repr__(self):
        return f"<ModuleConfig module={self.module!r}>"

    def __getitem__(self, item) -> EventConfig:
        return self.all_events[item]

    @property
    def cache(self) -> dict:
        if self.module.id not in self.guild.cache:
            data = self.guild.cache[self.module.id] = {"channel": None, "events": {}}
        else:
            data = self.guild.cache[self.module.id]

        # This converter should work, but hasn't been fully tested in a production environment.
        if "_log_channel" in data:
            log.debug("Converting data for module %r in guild %r", self.module.id, self.guild)
            lc = data.pop("_log_channel", None)
            events = {
                k: {"enabled": bool(v), "channel": v if type(v) is int else None}
                for k, v in flatten(data).items()
            }
            data.clear()
            data["channel"] = lc
            data["events"] = events

        return self.guild.cache[self.module.id]

    @property
    def all_events(self):
        return {
            event: EventConfig(self, event)
            for event in self.module.events
            if not event.children and event.event_id != ""
        }

    @property
    def channel(self) -> discord.TextChannel | None:
        """Which channel events belonging to this module should log to

        This may be overridden on a per-event basis; see :attr:`EventConfig.override_channel`
        """
        return shared.bot.get_channel(self.cache.get("channel"))

    @channel.setter
    def channel(self, value: discord.TextChannel | None):
        self.cache["channel"] = getattr(value, "id", None)

    @property
    def event_config(self) -> dict:
        """A :class:`dict` containing configuration values for this module's events

        The returned dict should resemble the following structure::

            {
                "event": {"enabled": False, "channel": None},
                # nested events use their dotted full id instead of being nested in dicts
                "group.child": {"enabled": True, "channel": None},
                # enabled will always be True if a channel is set
                "group.override": {"enabled": True, "channel": 123456}
            }

        """
        return self.cache["events"]

    def reset(self):
        """Reset configuration for the current module"""
        with suppress(KeyError):
            del self.guild.cache[self.module.id]


@dataclass(slots=True)
class EventConfig:
    module: ModuleConfig
    event: EventABC

    @property
    def default(self) -> dict:
        return {"enabled": False, "channel": None}

    @property
    def cache(self):
        if self.event.full_id not in self.module.cache["events"]:
            self.module.cache["events"][self.event.full_id] = self.default
        return self.module.cache["events"][self.event.full_id]

    @property
    def channel(self) -> discord.TextChannel | None:
        """What channel this event should log to

        If :attr:`override_channel` is :obj:`None`, this will be the same value as
        :attr:`ModuleConfig.channel`.

        .. warning:: The warning from :attr:`enabled` applies here too.
        """
        return self.override_channel or self.module.channel if self.enabled else None

    @property
    def override_channel(self) -> discord.TextChannel | None:
        """Which channel this event should log to, ignoring the module channel"""
        return shared.bot.get_channel(self.cache.get("channel"))

    @override_channel.setter
    def override_channel(self, value: discord.TextChannel | None):
        self.cache["channel"] = getattr(value, "id", None)

    @property
    def enabled(self) -> bool:
        """Whether this event should log anything or not

        .. warning::
            Even if this returns :obj:`True`, you should still ensure that this event
            isn't suppressed in a mutually exclusive event group.
        """
        return self.cache["enabled"]

    @enabled.setter
    def enabled(self, value: bool):
        self.cache["enabled"] = value

    def status(self) -> EventStatus:
        """Return a basic representation of this event's current state

        .. important:: This shouldn't be used for anything other than a very basic overview.
        """
        if not self.enabled:
            return EventStatus.DISABLED
        if not self.channel:
            return EventStatus.ENABLED_WITHOUT_CHANNEL
        if (
            self.event.parent
            and self.event.parent.mutually_exclusive
            and self.event not in self.event.parent.resolve(self.module.guild)
        ):
            if self.override_channel:
                return EventStatus.ENABLED_MUTUAL_SUPPRESSED_OVERRIDE
            return EventStatus.ENABLED_MUTUAL_SUPPRESSED
        if self.override_channel:
            return EventStatus.ENABLED_WITH_OVERRIDE_CHANNEL
        return EventStatus.ENABLED

    def update(self, new_val: bool | discord.TextChannel) -> None:
        """Convenience method to update an event's config

        This is the same as setting both :attr:`enabled` and/or :attr:`override_channel`.
        """
        if isinstance(new_val, bool):
            self.enabled = new_val
            self.override_channel = None
        else:
            self.enabled = True
            self.override_channel = new_val
