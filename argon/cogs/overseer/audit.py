from __future__ import annotations

import warnings
from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from typing import Awaitable, Callable

import discord
from discord.utils import maybe_coroutine

from argon.cogs.overseer import shared
from argon.cogs.overseer.abc import ARGUMENTS, SkipEvent
from argon.cogs.overseer.config import GuildConfig
from argon.cogs.overseer.shared import log
from argon.cogs.overseer.utils import GUILD

MaybeAsyncBool = bool | Awaitable[bool]
AuditCheck = Callable[..., MaybeAsyncBool]


class _DummyMember:
    def __init__(self):
        self.id = 0
        self.mention = "<unknown user>"
        self.bot = False

    def __str__(self):
        return "#".join(["unknown member", "0000"])

    def __eq__(self, other):
        return isinstance(other, _DummyMember)

    def __bool__(self):
        return False


dummy_member = _DummyMember()


def id_check(entry: discord.AuditLogEntry, *args, **kwargs) -> bool:
    """A simple check that can be used to return True if an audit log entry target correlates to any
    arguments passed to the event parser"""
    return any(entry.target.id == x.id for x in (*args, *kwargs.values()) if hasattr(x, "id"))


@dataclass(frozen=True, slots=True)
class AuditResult:
    member: _DummyMember | discord.abc.User
    reason: str | None

    def __bool__(self):
        return bool(self.member)

    def __str__(self):
        return str(self.member)

    def __eq__(self, other):
        return isinstance(other, discord.Member) and other == self.member

    @property
    def id(self) -> int:
        return self.member.id

    @property
    def mention(self) -> str:
        return self.member.mention

    @property
    def bot(self) -> bool:
        return self.member.bot


class AuditHandler:
    """Utility class to retrieve a responsible user from audit log actions

    Arguments
    ----------
    skip_bots: bool
        Optional keyword argument. If this is :obj:`True`, if the responsible user
        is a bot user a :class:`SkipEvent` exception will be raised. Defaults to :obj:`False`.
        This can be overridden on a per-event basis when calling this object.

    Example
    --------
    >>> audit = AuditHandler()
    >>> audit.check(discord.AuditLogAction.ban, check=id_check)
    >>> async def my_event(...):
    ...     # if no moderator is found from the audit log, the return value will
    ...     # be a dummy member that resolves to False when used in an if check;
    ...     # this dummy member object has `id` and `mention` attributes for convenience.
    ...     # the audit log action type is optional here, but is recommended if
    ...     # you retrieve from two or more action types
    ...     mod = await audit(discord.AuditLogAction.ban)
    """

    def __init__(self, *, skip_bots: bool = False):
        self._checks: dict[discord.AuditLogAction, Callable] = {}
        self._skip_bots = skip_bots

    @property
    def _all_actions(self) -> set[discord.AuditLogAction]:
        return {*self._checks.keys()}

    # pycharm apparently believes Callable | None is attempting to use Callable.__or__
    def __getitem__(self, item: discord.AuditLogAction) -> Callable | None:  # noqa
        return self._checks.get(item)

    async def __call__(
        self, *actions: discord.AuditLogAction, skip_bots: bool = None
    ) -> AuditResult | None:
        # Fail before ever making API requests if the bot owner has specifically
        # requested that this functionality be disabled
        if not shared.bot.get_command("overseer audit").enabled:
            return AuditResult(member=dummy_member, reason=None)

        skip_bots = self._skip_bots if skip_bots is None else skip_bots
        if not self._checks:
            return AuditResult(member=dummy_member, reason=None)
        actions = {*actions} or self._all_actions
        guild = GUILD.get()
        cfg = await GuildConfig.get(guild).load()
        if not guild.me.guild_permissions.view_audit_log or not cfg.audit_enabled:
            return AuditResult(member=dummy_member, reason=None)

        oldest_acceptable = (datetime.utcnow() + timedelta(seconds=30)).replace(tzinfo=timezone.utc)
        filter_to = next(iter(actions)) if len(actions) == 1 else None
        log.debug("Retrieving 5 audit log entries (filter %r)", actions)
        # Ideally we'd want to cache whatever we can from this, but implementing
        # such a cache is more of a headache than I care enough to figure out;
        # I can live with some logs being slightly delayed because of rate limits.
        async for entry in guild.audit_logs(limit=5, action=filter_to):
            if entry.action not in actions:
                continue
            if entry.created_at > oldest_acceptable:
                continue
            if check := self[entry.action]:
                log.debug("Calling check %r for action %r", check, entry.action)
                args, kwargs = ARGUMENTS.get()
                if await maybe_coroutine(check, entry, *args, **kwargs):
                    log.debug("%r is responsible", entry.user)
                    if entry.user.bot and skip_bots:
                        raise SkipEvent("Bot user is responsible")
                    return AuditResult(member=entry.user, reason=entry.reason)

        return AuditResult(member=dummy_member, reason=None)

    def check(
        self,
        action: discord.AuditLogAction,
        *actions: discord.AuditLogAction,
        check: Callable[..., MaybeAsyncBool] | None = None,
    ) -> Callable | Callable[[Callable], Callable]:  # noqa
        """Add a new check for audit log action types

        This method may be used either as a regular function call or as a function decorator.

        Only one callable will ever be used for a given :class:`~discord.AuditLogAction`.

        The arguments passed to registered checks will be the same as the arguments passed
        when calling the current event parser.

        .. seealso:: :func:`id_check`

        Example
        -------
        >>> import discord
        >>> audit = AuditHandler()
        >>> # Use as a regular function call:
        >>> audit.check(discord.AuditLogAction.member_update, check=lambda *a, **kw: ...)
        >>> # Use as a decorator:
        >>> @audit.check(
        ...     # More than one action type may be specified, and all of them will be registered
        ...     # with the given check callable
        ...     discord.AuditLogAction.channel_update, discord.AuditLogAction.channel_create
        ... )
        ... def audit_check(*a, **kw):
        ...     ...
        """

        actions = (action, *actions)

        def decorator(func: AuditCheck) -> AuditCheck:
            for act in actions:
                if act in self._checks:
                    warnings.warn(
                        f"Re-registering audit check for action {act!r}", UserWarning, stacklevel=3
                    )
                self._checks[act] = func
            return func

        if check:
            return decorator(check)
        else:
            return decorator
