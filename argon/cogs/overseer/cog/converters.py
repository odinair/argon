from dataclasses import dataclass
from fnmatch import fnmatch
from typing import TYPE_CHECKING, Optional, Union

import discord

from argon import commands
from argon.cogs.overseer.event import Event
from argon.cogs.overseer.module import Module
from argon.cogs.overseer.shared import icu as _
from argon.cogs.overseer.shared import modules
from argon.cogs.overseer.utils import ensure_required_permissions
from argon.utils import deduplicate, flatten

NO_MODULE = _("No such module `{module}` exists")
INVALID_ARGS = _(
    "Argument `{argument}` is not a correctly formed argument (see `{prefix}help overseer set`)"
)
INVALID_CHANNEL = _("Failed to convert `{arg}` to a boolean or text channel")
INVALID_EVENT = _("Failed to find any events on the {module} module named `{event}`")

if TYPE_CHECKING:
    ModuleConverter = Module
else:

    class ModuleConverter(commands.Converter):
        async def convert(self, ctx, argument):
            try:
                module = modules[argument.lower()]
            except KeyError:
                module = None
            if not module:
                raise commands.BadArgument(NO_MODULE.format(module=argument))
            return module


@dataclass(frozen=True)
class Settings:
    events: list[Event]
    set_to: Optional[Union[bool, discord.TextChannel]]


if TYPE_CHECKING:
    Events = Settings
else:

    class Events(commands.Converter):
        async def convert(self, ctx: commands.Context, argument: str) -> Settings:
            if argument.count("=") > 1:
                raise commands.BadArgument(
                    INVALID_ARGS.format(
                        argument=argument.replace("`", "\\`"), prefix=ctx.clean_prefix
                    )
                )

            name, argument = argument.split("=") if argument.count("=") == 1 else (argument, None)
            name = name.split(".")

            # Match argument after '=' to either a boolean or text channel
            if argument:
                if argument.lower() in ("true", "1", "yes", "on", "enabled"):
                    argument = True
                elif argument.lower() in ("false", "0", "off", "no", "disabled"):
                    argument = False
                else:
                    try:
                        argument = await commands.TextChannelConverter().convert(ctx, argument)
                        if argument.guild != ctx.guild:
                            raise commands.BadArgument()
                    except commands.BadArgument:
                        raise commands.BadArgument(INVALID_CHANNEL.format(arg=argument)) from None
                    else:
                        ensure_required_permissions(argument)

            module_name = name.pop(0)
            try:
                # noinspection PyTypeChecker
                module: Module = modules[module_name.lower()]
            except KeyError:
                raise commands.BadArgument(NO_MODULE.format(module=module_name)) from None

            name = ".".join(name) if name else "*"
            events = deduplicate(
                flatten(
                    [
                        [*x.walk()] if x.children else x
                        for x in module.events
                        if fnmatch(name=x.full_id, pat=name)
                    ]
                )
            )
            if not events:
                raise commands.BadArgument(INVALID_EVENT.format(module=module.name, event=name))

            return Settings(events, argument)
