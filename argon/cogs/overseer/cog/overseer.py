from __future__ import annotations

import logging
from typing import Optional, Set, Union

import discord
from discord.ext.menus import MenuPages
from discord.utils import as_chunks

from argon import commands
from argon.bot import Argon
from argon.cogs.overseer.cog.converters import Events, ModuleConverter
from argon.cogs.overseer.cog.event_list import EventListPageSource
from argon.cogs.overseer.cog.listeners import OverseerEvents
from argon.cogs.overseer.config import EventStatus, GuildConfig
from argon.cogs.overseer.event import Event
from argon.cogs.overseer.module import Module
from argon.cogs.overseer.modules import debug
from argon.cogs.overseer.shared import icu as _
from argon.cogs.overseer.shared import modules
from argon.cogs.overseer.utils import ensure_required_permissions
from argon.translations import Humanize
from argon.utils import flatten
from argon.utils.chat_formatting import pagify, warning
from argon.utils.menus import ConfirmMenu

Suppressible = Union[
    discord.TextChannel,
    discord.VoiceChannel,
    discord.CategoryChannel,
    discord.Role,
    discord.Member,
]
MODIFIED = {
    EventStatus.DISABLED: _("\N{LARGE RED CIRCLE} Logging of event **`{event_id}`** disabled"),
    EventStatus.ENABLED: _("\N{LARGE GREEN CIRCLE} Logging of event **`{event_id}`** enabled"),
    EventStatus.ENABLED_WITHOUT_CHANNEL: _(
        "\N{LARGE YELLOW CIRCLE} Logging of event **`{event_id}`** enabled, but no log channel"
        " is set"
    ),
    EventStatus.ENABLED_WITH_OVERRIDE_CHANNEL: _(
        "\N{LARGE GREEN CIRCLE} Logging of event **`{event_id}`** enabled with channel"
        " override {channel}"
    ),
    EventStatus.ENABLED_MUTUAL_SUPPRESSED: _(
        "\N{LARGE PURPLE CIRCLE} Logging of event **`{event_id}`** enabled, but a different event"
        " in the same group is taking precedence"
    ),
    EventStatus.ENABLED_MUTUAL_SUPPRESSED_OVERRIDE: _(
        "\N{LARGE PURPLE CIRCLE} Logging of event **`{event_id}`** enabled with channel override"
        " {channel}, but a different event in the same group is taking precedence"
    ),
}


def chunk_events(per: int = 14, secret: bool = False) -> list[dict[Module, list[Event]]]:
    evs: list[tuple[Module, Event]] = [
        (x.module, x)
        for x in flatten([module.events for module in modules.values() if module.secret is secret])
        if not x.children and x.event_id != ""
    ]
    return_val: list[dict] = []
    for chunk in as_chunks(evs, per):
        chunk_val: dict[Module, list[Event]] = {}
        for module, event in chunk:
            if module not in chunk_val:
                chunk_val[module] = []
            chunk_val[module].append(event)
        return_val.append(chunk_val)
    return return_val


class Context(commands.Context):
    guild_cfg: GuildConfig


@_.cog()
class Overseer(commands.Cog, OverseerEvents):
    """Cut through the noise of moderation logging"""

    # TODO this is only here to fix the overseer command raising an error before it gets
    #      updated to replace its ext.menus implementation
    __version__ = "1.0.0"

    def __init__(self, bot: Argon):
        super().__init__()
        self.bot = bot

    async def cog_before_invoke(self, ctx: Context):
        ctx.guild_cfg = await GuildConfig.get(ctx.guild).load()

    async def red_delete_data_for_user(self, **_):
        pass  # nothing to delete

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    @commands.bot_has_permissions(add_reactions=True, embed_links=True)
    async def overseer(self, ctx: commands.Context):
        """Manage Overseer settings for the current server"""
        if not ctx.invoked_subcommand:
            # noinspection PyTypeChecker
            await ctx.invoke(self.list)

    @overseer.command(aliases=["toggle"], usage="<events...>")
    async def set(self, ctx: Context, *events: Events):
        """Change event logging settings

        `events` is a space-separated list of events, taking the form of `module.event` (see
        `[p]overseer list` for a full list), with an optional argument (either a yes/no value or
        text channel name/id) proceeding a `=` character.

        A few example inputs:

        - `server.name` \N{EM DASH} Toggle logging server name changes
        - `server.emoji=emoji-changelog` \N{EM DASH} Send emoji updates to #emoji-changelog
        - `role.create=false` \N{EM DASH} Disable logging for role creations

        Wildcard patterns (such as `member.update.*`) are supported.

        Specifying a module name or event group without any other arguments is treated the same as
        using a `*` match on it (meaning `server` is the same as `server.*`, `member.update`
        is the same as `member.update.*`, and so on).
        """
        if not events:
            await ctx.send_help()
            return

        # noinspection PyTypeChecker
        events: list[tuple[Event, Optional[Union[discord.TextChannel, bool]]]] = [
            (z, x.set_to) for x in events for z in x.events
        ]

        modified: Set[Event] = set()
        for event, set_to in events:
            if event.children:
                continue
            current_status = ctx.guild_cfg[event].enabled
            current_channel = ctx.guild_cfg[event].override_channel
            if set_to is None:
                set_to = not current_status

            if (isinstance(set_to, bool) and set_to is current_status and not current_channel) or (
                isinstance(set_to, discord.TextChannel) and set_to == current_channel
            ):
                continue

            ctx.guild_cfg[event].update(set_to)
            modified.add(event)

        if modified:
            await ctx.guild_cfg.save()
            modified_overview = {
                event: MODIFIED[ctx.guild_cfg[event].status()].format(
                    event_id=str(event),
                    channel=getattr(ctx.guild_cfg[event].override_channel, "mention", None),
                )
                for event in modified
            }
            colour = ctx.embed_colour
            for page in pagify("\n".join(modified_overview.values()), page_length=1_000):
                await ctx.send(
                    embed=discord.Embed(
                        title=_(
                            "{n, plural, one {1 Modified Setting} other {{n} Modified Settings}}"
                        ).format(n=len(modified)),
                        description=page,
                        colour=colour,
                    )
                )
        else:
            await ctx.send(
                warning(_("Failed to update settings for any specified events.").format())
            )

    @overseer.group(invoke_without_command=True)
    async def list(self, ctx: Context):
        """List current event logging settings"""
        await MenuPages(
            EventListPageSource(chunk_events(), per_page=1), clear_reactions_after=True
        ).start(ctx)

    @list.command(name="hidden", aliases=["secret"], hidden=True)
    @commands.is_owner()
    async def list_hidden(self, ctx: Context):
        await MenuPages(
            EventListPageSource(chunk_events(secret=True), per_page=1), clear_reactions_after=True
        ).start(ctx)

    @overseer.command(aliases=["dest", "channel"], usage="<module>... [channel]")
    async def destination(
        self,
        ctx: Context,
        mods: commands.Greedy[ModuleConverter],
        channel: discord.TextChannel = None,
    ):
        """Change the log channel for one or more modules

        This may be overridden on a per-event basis; see `[p]help overseer set`.

        Not specifying a channel will stop logging for the specified modules (with the exception
        of events that override this channel).
        """
        if not mods:
            raise commands.BadArgument()

        ensure_required_permissions(channel)
        for module in mods:
            ctx.guild_cfg[module].channel = channel
        await ctx.guild_cfg.save()

        if channel:
            await ctx.send(
                _(
                    "\N{WHITE HEAVY CHECK MARK} The following {n, count, one {module}"
                    " other {modules}} will now log to {channel}: {modules}"
                ).format(
                    n=len(mods), channel=channel.mention, modules=Humanize([x.name for x in mods])
                )
            )
        else:
            await ctx.send(
                _(
                    "\N{WHITE HEAVY CHECK MARK} Cleared log channels for the following"
                    "{n, count, one {module} other {modules}}: {modules}"
                ).format(n=len(mods), modules=Humanize([x.name for x in mods]))
            )

    @overseer.command(usage="[module...]")
    async def reset(self, ctx: Context, *mods: ModuleConverter):
        """Reset settings for the current server or specific modules

        This only resets event logging settings; if you'd like to more
        thoroughly reset all Overseer settings, see `[p]overseer wipe`.
        """
        count = (
            len(mods)
            if mods
            else len([x for x in ctx.guild_cfg.cache.keys() if not x.startswith("_")])
        )
        to_reset = [ctx.guild_cfg[x] for x in mods] if mods else [ctx.guild_cfg]
        if await ConfirmMenu(ctx.author).prompt(
            ctx,
            _(
                "\N{WARNING SIGN} Are you sure you want to reset settings for {count, plural, one"
                " {# module} other {# modules}}? **This action cannot be undone!**"
            ).format(count=count),
        ):
            for reset in to_reset:
                reset.reset()
            await ctx.guild_cfg.save()
            await ctx.send(
                _(
                    "\N{PUT LITTER IN ITS PLACE SYMBOL} Reset settings for {count, plural,"
                    " one {1 module} other {{count} modules}}."
                ).format(count=count)
            )
        else:
            await ctx.send(_("\N{CROSS MARK} Cancelled.").format())

    @overseer.command(hidden=True)
    async def wipe(self, ctx: Context):
        """Completely wipe Overseer settings for the current server

        This includes event logging settings, as well as:

        - `[p]overseer audit`
        - `[p]overseer suppress`
        - `[p]overseer embeds`
        """
        if await ConfirmMenu(ctx.author).prompt(
            ctx,
            _(
                "\N{WARNING SIGN} Are you __absolutely sure__ you want to __*completely*__"
                " wipe Overseer settings for this server? __***This action cannot"
                " be undone!***__\n"
                "This includes settings such as suppressed members, roles, and channels!"
            ).format(),
        ):
            ctx.guild_cfg.reset(True)
            await ctx.guild_cfg.save()
            await ctx.send(
                _(
                    "\N{PUT LITTER IN ITS PLACE SYMBOL} Wiped Overseer configuration for"
                    " this server."
                ).format()
            )
        else:
            await ctx.send(_("\N{CROSS MARK} Cancelled.").format())

    @overseer.command()
    async def embeds(self, ctx: Context, toggle: bool = None):
        """Toggle if events prefer logging with embeds

        Most events respect this setting, while a few ignore this setting and use
        a mix of message content and embeds regardless.

        Embeds will try to use the colour of whatever is relevant to the event
        in question, but will fall back to using either the colour set for
        [botname] by the bot owner or the bot's highest ranking role with a
        colour, as determined by `[p]set usebotcolour`."""
        ctx.guild_cfg.prefer_embeds = (
            toggle if toggle is not None else not ctx.guild_cfg.prefer_embeds
        )
        await ctx.guild_cfg.save()
        await ctx.send(
            (
                _("\N{LARGE GREEN CIRCLE} Event logging will now prefer using embeds.")
                if ctx.guild_cfg.prefer_embeds
                else _("\N{LARGE RED CIRCLE} Event logging will no longer prefer using embeds.")
            ).format()
        )

    @overseer.command()
    async def audit(self, ctx: Context, toggle: bool = None):
        """Toggle audit log retrieval for logging

        Not all events use audit log retrieval, either due to Discord limitations or
        other concerns with including such information.

        Additionally, if a member's roles are updated by a bot while this setting
        is enabled, only roles that grant permissions that are considered privileged
        will be logged; this includes the following permissions:

        - All `Manage` permissions
        - Mention everyone
        - Permissions typically reserved for mods/admins, such as
        Kick/Ban Members and Manage Messages
        """
        ctx.guild_cfg.audit_enabled = (
            toggle if toggle is not None else not ctx.guild_cfg.audit_enabled
        )
        await ctx.guild_cfg.save()
        await ctx.send(
            (
                _("\N{LARGE GREEN CIRCLE} Audit log information will now be included in logging.")
                if ctx.guild_cfg.audit_enabled
                else _(
                    "\N{LARGE RED CIRCLE} Audit log information will no longer be included"
                    " in logging."
                )
            ).format()
        )

    @overseer.group()
    async def suppress(self, ctx: Context):
        """Suppress logging relating to certain members, roles, or channels

        Suppressed categories will also implicitly suppress any channels
        it contains

        Suppressed roles also will not be logged in member role updates,
        along with normal role update logging
        """

    @suppress.command(name="add", usage="<channel/role/member>")
    async def suppress_add(self, ctx: Context, *, to_suppress: Suppressible):
        """Suppress logging from a member, role, or channel"""
        suppress = to_suppress.id
        if suppress in ctx.guild_cfg.suppress:
            await ctx.send(
                _("{mention} is already suppressed from logging.").format(mention=to_suppress),
                allowed_mentions=discord.AllowedMentions(roles=False, users=False),
            )
            return

        ctx.guild_cfg.suppress.add(suppress)
        await ctx.guild_cfg.save()
        await ctx.send(
            _("{mention} is now suppressed from logging.").format(mention=to_suppress),
            allowed_mentions=discord.AllowedMentions(roles=False, users=False),
        )

    @suppress.command(name="remove", usage="<channel/role/member>")
    async def suppress_remove(self, ctx: Context, *, to_suppress: Suppressible):
        """Stop suppressing logging from a member, role, or channel"""
        suppress = to_suppress.id
        if suppress not in ctx.guild_cfg.suppress:
            await ctx.send(
                _("{mention} is not currently suppressed from logging.").format(
                    mention=to_suppress
                ),
                allowed_mentions=discord.AllowedMentions(roles=False, users=False, everyone=False),
            )
            return

        ctx.guild_cfg.suppress.discard(suppress)
        await ctx.guild_cfg.save()
        await ctx.send(
            _("{mention} is no longer suppressed from logging.").format(mention=to_suppress),
            allowed_mentions=discord.AllowedMentions(roles=False, users=False, everyone=False),
        )

    # region Debug commands

    @overseer.group()
    @commands.is_owner()
    @commands.check(lambda ctx: logging.getLogger("argon").getEffectiveLevel() <= logging.DEBUG)
    async def debug(self, ctx: commands.Context):
        """Command group to debug Overseer"""

    @debug.command(name="basic")
    async def debug_basic(self, ctx: commands.Context):
        await debug.debug_event.in_context(ctx.guild)

    @debug.command(name="checks")
    async def debug_checks(self, ctx: commands.Context, debug_check: bool):
        await debug.debug_checks.in_context(ctx.guild, debug_check)

    # endregion
