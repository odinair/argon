from __future__ import annotations

import discord
from discord.ext.menus import ListPageSource, MenuPages

from argon.cogs.overseer.config import EventStatus, GuildConfig
from argon.cogs.overseer.event import Event
from argon.cogs.overseer.module import Module
from argon.cogs.overseer.shared import icu as _

# TODO: this entire file should be rewritten to use ui.View instead of ext.menus, or
#       the overseer command should be rewritten as a slash command w/ autocomplete

no_description = _("*No description provided*")
STATUSES = {
    # While these don't have any explicit reason to be translated, they still make use
    # of the red_icu transformers we have setup (most notably with the `channel` format arg),
    # so there's enough of a justification for not bothering with changing it.
    EventStatus.DISABLED: _("\N{LARGE RED CIRCLE} **`{event_id}`** — {description}"),
    EventStatus.ENABLED_MUTUAL_SUPPRESSED: _(
        "\N{LARGE PURPLE CIRCLE} **`{event_id}`** — {description}"
    ),
    EventStatus.ENABLED_MUTUAL_SUPPRESSED_OVERRIDE: _(
        "\N{LARGE PURPLE CIRCLE} **`{event_id}` ➜** {channel} — {description}"
    ),
    EventStatus.ENABLED_WITHOUT_CHANNEL: _(
        "\N{LARGE YELLOW CIRCLE} **`{event_id}`** — {description}"
    ),
    EventStatus.ENABLED_WITH_OVERRIDE_CHANNEL: _(
        "\N{LARGE GREEN CIRCLE} **`{event_id}` ➜** {channel} — {description}"
    ),
    EventStatus.ENABLED: _("\N{LARGE GREEN CIRCLE} **`{event_id}`** — {description}"),
}
KEY = _(
    "\N{LARGE RED CIRCLE} off **|**"
    " \N{LARGE GREEN CIRCLE} on **|**"
    " \N{LARGE YELLOW CIRCLE} on, no log channel **|**"
    " \N{LARGE PURPLE CIRCLE} on, suppressed"
)


class EventListPageSource(ListPageSource):
    @staticmethod
    def build_event_list(
        menu: MenuPages, guild: GuildConfig, module: Module, *events: Event
    ) -> str:
        channel = guild[module].channel
        ret = [
            " **|** ".join(
                [
                    (
                        _("\N{BELL} {channel}")
                        if channel
                        else _("\N{BELL WITH CANCELLATION STROKE} No log channel")
                    ).format(channel=channel, prefix=menu.ctx.clean_prefix),
                    str(module.description or no_description),
                ]
            ),
            "",
        ]

        for event in events:
            override_channel = guild[event].override_channel
            ret.append(
                STATUSES[guild[event].status()].format(
                    event_id=str(event),
                    description=event.description or no_description,
                    channel=override_channel,
                )
            )
        return "\n".join(ret)

    async def format_page(self, menu: MenuPages, page: dict[Module, list[Event]]):
        guild = menu.ctx.guild_cfg
        embed = discord.Embed(
            colour=menu.ctx.embed_colour,
            description=_(
                "**➜** `{prefix}overseer set` to change settings\n"
                "**➜** `{prefix}overseer destination` to change log channels\n\n"
                "**Key:** {key}"
            ).format(prefix=menu.ctx.clean_prefix, key=KEY)
            if menu.current_page == 0
            else None,
        )
        embed.set_author(
            name=_("Overseer Configuration for {guild}").format(guild=menu.ctx.guild),
            icon_url=menu.ctx.guild.icon or None,
        )
        embed.set_footer(
            text=_("Page {current} out of {total} | Overseer v{version}").format(
                current=menu.current_page + 1,
                total=self.get_max_pages(),
                version=menu.ctx.bot.get_cog("Overseer").__version__,
            )
        )
        for module, events in page.items():
            embed.add_field(
                name=str(module.name),
                value=self.build_event_list(menu, guild, module, *events),
                inline=False,
            )
        return embed
