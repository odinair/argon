from argon.bot import Argon


async def setup(bot: Argon):
    from argon.cogs.overseer import shared
    from argon.cogs.overseer.cog import Overseer

    shared.bot = bot
    await bot.add_cog(Overseer(bot))
