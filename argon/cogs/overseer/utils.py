# pylint:disable=isinstance-second-argument-not-valid-type
from __future__ import annotations

from contextvars import ContextVar
from typing import Iterable, Optional, Sequence, Type

import discord
from discord.raw_models import RawBulkMessageDeleteEvent

from argon import commands
from argon.cogs.overseer import shared
from argon.cogs.overseer.shared import icu as _
from argon.translations import Humanize
from argon.utils.chat_formatting import translate_permission

__all__ = (
    "IN_CONTEXT",
    "GUILD",
    "missing_permissions",
    "ensure_required_permissions",
    "chunks",
    "find_guild",
    "context_colour",
    "is_suppressed",
)
required_permissions = {"read_messages", "embed_links", "send_messages"}
IN_CONTEXT: ContextVar[bool] = ContextVar("in context", default=False)
GUILD: ContextVar[discord.Guild | None] = ContextVar("guild")


def missing_permissions(channel: discord.TextChannel):
    guild: discord.Guild = channel.guild
    return set.difference(
        required_permissions, {k for k, v in channel.permissions_for(guild.me) if v}
    )


# While the issue we're checking for technically would be a bad argument,
# Red doesn't return the message passed to BadArgument, so we're using
# UserFeedbackCheckFailure to ensure the message we're providing is shown
# to the end user.
def ensure_required_permissions(
    channel: discord.TextChannel,
    *,
    err: Type[Exception] = commands.CheckFailure,
    translate_error: bool = True,
) -> None:
    if missing := missing_permissions(channel):
        if not translate_error:
            raise err(f"Missing required permissions in {channel!r}: {missing}")

        raise err(
            _(
                "\N{WARNING SIGN} {channel} does not have the correct permissions set; please grant"
                " me the following {count, plural, one {permission} other {permissions}}"
                " and try again: {permissions}"
            ).format(
                channel=channel,
                count=len(missing),
                permissions=Humanize([*map(translate_permission, missing)]),
            )
        )


def chunks(seq: Sequence, chunk_every: int) -> Iterable[Sequence]:
    import warnings

    from discord.utils import as_chunks

    warnings.warn("Use discord.utils.as_chunks instead", DeprecationWarning)
    return as_chunks(seq, chunk_every)


def find_guild(*args) -> Optional[discord.Guild]:
    guild = GUILD.get(None)
    if guild is not None:
        return guild

    # noinspection PyShadowingNames
    def _find(*a):
        for arg in a:
            if isinstance(arg, discord.Guild):
                return arg
            elif isinstance(arg, discord.VoiceState) and arg.channel:
                # bot accounts can't be called nor added to group DMs, so we can
                # relatively safely assume that this property exists
                return arg.channel.guild
            elif guild := getattr(arg, "guild", None):
                # this block should catch the following types:
                #  discord.Role, discord.abc.GuildChannel, discord.Member,
                #  discord.Message
                return guild
            # only relevant in event listener checks
            elif isinstance(arg, RawBulkMessageDeleteEvent):
                channel = shared.bot.get_channel(arg.channel_id)
                if getattr(channel, "guild", None):
                    return channel.guild
            elif isinstance(arg, tuple) and (g := _find(*arg)):
                # noinspection PyUnboundLocalVariable
                return g

    guild = _find(*args)
    # the guild may be an Object in some rare cases
    if not guild or isinstance(guild, discord.Object):
        return None
    if IN_CONTEXT.get() is True:
        GUILD.set(guild)
    return guild


async def context_colour(*args) -> discord.Colour | None:
    guild = find_guild(*args)
    if not guild:
        raise TypeError("Could not infer guild config from arguments")

    for arg in args:
        if isinstance(arg, (discord.Role, discord.Member)) and arg.colour.value != 0:
            return arg.colour
        elif isinstance(arg, discord.Message):
            if arg.author.colour.value != 0:
                return arg.author.colour

    return guild.me.colour if guild.me.colour and guild.me.colour.value != 0 else shared.bot.colour


def is_suppressed(*args) -> bool:
    from argon.cogs.overseer.config import GuildConfig

    cfg = next((x for x in args if isinstance(x, GuildConfig)), None)
    if not cfg:
        guild = find_guild(*args)
        if not guild:
            raise TypeError("Could not infer guild config from arguments")
        cfg = GuildConfig.get(guild)

    ids = set()
    for arg in args:
        if isinstance(arg, discord.abc.GuildChannel):
            ids.add(arg.id)
            if arg.category:
                ids.add(arg.category.id)
        elif isinstance(arg, (discord.Message, commands.Context)):
            ids = {*ids, arg.author.id, arg.channel.id}
        elif isinstance(arg, discord.VoiceState) and arg.channel:
            ids.add(arg.channel.id)
        elif hasattr(arg, "id"):
            ids.add(arg.id)

    return bool(cfg.suppress.intersection(ids))
