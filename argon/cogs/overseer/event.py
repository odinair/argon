"""Core event handler logic

This module handles most of the logic that makes Overseer tick.
"""

from __future__ import annotations

import asyncio
from contextvars import copy_context
from math import inf
from typing import AsyncIterable, Callable, Generator, Iterable

import discord

from argon.cogs.overseer.abc import (
    ARGUMENTS,
    EventABC,
    Loggable,
    LogType,
    NoGuildException,
    SkipEvent,
)
from argon.cogs.overseer.config import GuildConfig
from argon.cogs.overseer.shared import log
from argon.cogs.overseer.utils import GUILD
from argon.cogs.overseer.utils import IN_CONTEXT as _IN_CONTEXT
from argon.cogs.overseer.utils import context_colour, find_guild, is_suppressed, missing_permissions
from argon.translations import LazyStr
from argon.utils import flatten

__all__ = ("Event",)


# Important implementation quirk: EventABC is the class that sets class-level attributes
class Event(EventABC):
    """Core event logic class

    Don't create instances of this class directly; create a :class:`Module`
    and use :meth:`Module.event` or :meth:`Event.event`

    Instantiated Event objects may be called to call the underlying decorated
    function and log the return value.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.parent is not None:
            if self.event_id in self.parent.children:
                raise ValueError(
                    f"An event with the ID {self.event_id!r} is already registered on"
                    f" event {self.parent!r}"
                )
            self.parent.children[self.event_id] = self
        if self.event_id != "":
            self.module.events.append(self)

    # region Logging logic
    async def __call__(self, *args, **kwargs):  # pylint:disable=invalid-overridden-method
        if _IN_CONTEXT.get() is False:
            log.debug("Setting up context")
            await copy_context().run(self._ctx_setup, args, kwargs)
        else:
            log.debug("Re-using existing context")
            await self._logging_logic(args, kwargs)

    async def _ctx_setup(self, args: tuple, kwargs: dict, guild: discord.Guild = None):
        """Setup contextvars for the current event invocation"""
        _IN_CONTEXT.set(True)
        if guild:
            log.debug("Using content guild %r", guild)
            GUILD.set(guild)
        ARGUMENTS.set((args, kwargs))
        await self._logging_logic(args, kwargs)

    async def _logging_logic(self, args: tuple, kwargs: dict):
        """Core logic for logging events"""
        guild = find_guild(*args, *kwargs.values())
        if not guild:
            raise NoGuildException(
                "Could not find an associated guild for this event; try using"
                " .in_context(guild, ...)"
            )
        guild_cfg = await GuildConfig.get(guild)
        if not await self._should_log(guild_cfg, args, kwargs):
            return

        if self.children:
            to_call = self.resolve(guild_cfg)
            if not to_call:
                log.debug("Found no applicable child events to call, skipping event")
                return
            log.debug("Calling %s child event(s): %r", len(to_call), to_call)
            timeout = 120
            _, pending = await asyncio.wait(
                [asyncio.ensure_future(x(*args, **kwargs)) for x in to_call],
                timeout=timeout,
            )
            if pending:
                log.warning(
                    "%s event(s) are still pending after %s seconds (parent event %r)",
                    len(pending),
                    timeout,
                    self,
                )
                log.debug("Pending futures: %r", pending)
                for fut in pending:
                    fut.cancel()
                log.warning("Cancelled %s pending event call(s)", len(pending))
            return

        if self.event_id == "" or not self.callable:
            raise RuntimeError(
                "Ghost event got past child event calls (did you forget to attach child events?)"
            )

        try:
            ret = await discord.utils.maybe_coroutine(self.callable, *args, **kwargs)
            if not ret:
                return
            # I appreciate the attempt pylint, but these are valid.
            # pylint:disable=isinstance-second-argument-not-valid-type
            ret = flatten(
                [x async for x in ret]
                if isinstance(ret, AsyncIterable)
                else list(ret)
                if isinstance(ret, (list, tuple, set, Generator))
                else [ret]
            )
            # pylint:enable=isinstance-second-argument-not-valid-type
        except SkipEvent as skip:
            log.debug("Event raised EventSkip (%s)", str(skip) or "no message")
            return
        else:
            await self._send(guild_cfg, await context_colour(*args, *kwargs.values()), ret)

    async def _should_log(self, cfg: GuildConfig, args: tuple, kwargs: dict) -> bool:
        """Check if the current event invocation should actually log anything"""
        if not self.has_required_features(cfg.guild):
            return False

        try:
            # all([]) returns True, so we can safely do this without checking
            # if there's actually any checks attached.
            if not await discord.utils.async_all(x(*args, **kwargs) for x in self.checks):
                return False
        except Exception as e:  # pylint:disable=broad-except
            log.exception(
                "Checks for event %r raised an error; not logging event invocation",
                self,
                exc_info=e,
            )
            log.debug("Event arguments: %r", (args, kwargs))
            return False

        if is_suppressed(cfg, *args, *kwargs.values()):
            return False
        # Events with child events work differently, so we skip the enabled
        # check on them.
        return bool(self.children or cfg[self].channel)

    async def _send(self, guild_cfg, colour: discord.Colour, return_values: Iterable[LogType]):
        """Send returned log values to the event's configured channel"""
        channel = guild_cfg[self].channel
        if missing_permissions(channel):
            return
        for val in return_values:
            if not val:
                continue
            if isinstance(val, LazyStr):
                val = str(val)
            if not isinstance(val, (str, discord.Embed, dict)):
                log.warning(
                    "%s event parser returned an unexpected value of type %r; ignoring",
                    self,
                    type(val).__name__,
                )
                log.debug("  Unexpected value: %r", val)
                continue
            if guild_cfg.prefer_embeds and isinstance(val, str):
                val = discord.Embed(colour=colour, description=val)
            send_args: dict = (
                val
                if isinstance(val, dict)
                else {"embed" if isinstance(val, discord.Embed) else "content": val}
            )
            if send_args.pop("allowed_mentions", None) is not None:
                log.warning(
                    "%r returned a dict that contains an allowed_mentions key,"
                    " but Overseer blocks all mentions from events",
                    self,
                )
            log.debug("Sending to channel %r: %r", channel, send_args)
            await channel.send(**send_args, allowed_mentions=discord.AllowedMentions.none())

    # endregion

    @property
    def all_parents(self):
        """All parents of the current event

        Ghost events (events with a blank :attr:`event_id`) are not included in the returned list.
        """
        return (
            [x for x in [*self.parent.all_parents, self.parent] if x.event_id != ""]
            if self.parent
            else []
        )

    @property
    def full_id(self) -> str:
        """The current event's full ID, including all parent events"""
        return ".".join([*[x.event_id for x in self.all_parents], self.event_id])

    def has_required_features(self, guild: discord.Guild) -> bool:
        """Check if the given guild has all the required features"""
        return not self.requires_features or set.issubset(self.requires_features, guild.features)

    def event(self, event_id: str, **kwargs) -> Callable[[Callable[..., Loggable]], EventABC]:
        """Add a new child event to the current event

        .. seealso:: :meth:`Module.event`
        """
        if self.parent and self.parent.mutually_exclusive:
            raise RuntimeError(
                "Cannot add a child event to an event that's a child of a mutually exclusive event"
            )

        if self.mutually_exclusive:
            kwargs.setdefault("priority", inf)
        return self.module.event(event_id, **kwargs, parent=self)

    def ghost(self) -> Event:
        """Return a new ghost Event on the current event"""
        if self.parent and self.parent.mutually_exclusive:
            raise RuntimeError(
                "Cannot add a child event to an event that's a child of a mutually exclusive event"
            )

        return Event(event_id="", parent=self, module=self.module, callable=None)

    def resolve(self, cfg: GuildConfig) -> list[Event]:
        """Resolve which child events would be called when calling the current event"""
        if not self.children:
            return []
        resolved = [
            *filter(
                lambda x: x.children or cfg[x].enabled,
                # lower numbers are sorted first
                sorted(self.children.values(), key=lambda x: x.priority),
            )
        ]
        if self.mutually_exclusive and resolved:
            return [resolved[0]]
        return resolved

    async def in_context(self, context_guild: discord.Guild, *args, **kwargs):
        # noinspection PyUnresolvedReferences
        """Call the current event with a given guild as the context guild

        This is usually only ever helpful if none of the arguments you pass can be used
        to determine which guild should receive the current event's output.

        Example
        --------
        >>> # Instead of calling `event(arg1, arg2, ...)` directly:
        >>> event.in_context(guild, arg1, arg2, ...)
        """
        return await copy_context().run(self._ctx_setup, args, kwargs, context_guild)

    def walk(self) -> Iterable[Event]:
        """Walk through all child events of this event"""
        if not self.children:
            return
        for child in self.children.values():
            if child.children:
                yield from child.walk()
            else:
                yield child
