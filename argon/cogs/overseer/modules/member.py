import asyncio

import discord

from argon.cogs.overseer.audit import AuditHandler, id_check
from argon.cogs.overseer.config import GuildConfig
from argon.cogs.overseer.module import Module
from argon.cogs.overseer.shared import icu as _
from argon.cogs.overseer.utils import context_colour
from argon.utils.chat_formatting import inline

Member = Module("member", name=_("Member"), description=_("Member join, leave, and update logging"))
change_strs = {True: _("{item} — granted"), False: _("{item} — revoked")}
# Any roles that have any of these permissions are exempted from the typical behaviour of
# member.update.roles ignoring role changes if the responsible user is a bot
PRIVILEGED_PERMS = discord.Permissions(
    discord.Permissions.elevated().value
    | discord.Permissions(
        # why are these first three not considered elevated permissions?
        manage_nicknames=True,
        mute_members=True,
        deafen_members=True,
        mention_everyone=True,
    ).value
)
audit = AuditHandler()
audit.check(
    discord.AuditLogAction.member_update,
    discord.AuditLogAction.member_role_update,
    discord.AuditLogAction.bot_add,
    check=id_check,
)


# region Checks
def _roles(before: discord.Member, after: discord.Member):
    cfg = GuildConfig.get(after.guild)
    ar = [x for x in after.roles if not x.managed and x.id not in cfg.suppress]
    br = [
        x
        for x in before.roles
        if not x.managed and x in after.guild.roles and x.id not in cfg.suppress
    ]
    return ar, br


async def roles_changed(before: discord.Member, after: discord.Member):
    # Sleep before doing anything to allow discord.py time to remove roles that are
    # being deleted from the guild's role list
    await asyncio.sleep(0.3)
    ar, br = _roles(before, after)
    return ar != br


# endregion


@Member.event("join")
def join(_: discord.Member):
    pass


@join.event("bot", description=_("Bot user join"))
@Member.check(lambda m: m.bot)
async def bot_join(member: discord.Member):
    added_by = await audit(discord.AuditLogAction.bot_add)
    if not added_by:
        return _("\N{ROBOT FACE} {member} was added to the server").format(member=member)
    return _("\N{ROBOT FACE} {added_by} added bot {member}").format(
        added_by=added_by, member=member
    )


@join.event("human", description=_("Member join"))
@Member.check(lambda m: not m.bot)
def member_join(member: discord.Member):
    return _("\N{INBOX TRAY} {member} joined the server").format(member=member)


@Member.event("leave", description=_("Member leave"))
def leave(member: discord.Member):
    return _("\N{OUTBOX TRAY} {member} left the server").format(member=member)


@Member.event("update")
def update(*_):
    pass


@update.event("name", description=_("Member username changes"))
@Member.check(lambda b, a: b.name != a.name)
def _update_name(before: discord.Member, after: discord.Member):
    return _("\N{LABEL} {member} changed their username from {before}").format(
        member=after, before=inline(before.name)
    )


@update.event("nickname", description=_("Member nickname changes"))
@Member.check(lambda b, a: b.nick != a.nick and not b.bot)
async def _update_nick(before: discord.Member, after: discord.Member):
    changed_by = await audit(discord.AuditLogAction.member_update)
    after_nick = inline(after.nick) if after.nick else _("*none*")
    before_nick = inline(before.nick) if before.nick else _("*none*")
    content = (
        (
            _("\N{LABEL} {member} changed their nickname: {change}")
            if changed_by == after
            else _("\N{LABEL} {moderator} changed {member}'s nickname: {change}")
        )
        if changed_by
        else _("\N{LABEL} {member}'s nickname was changed: {change}")
    )

    return content.format(
        member=after,
        moderator=changed_by,
        change=f"{before_nick!s} \N{HEAVY TRIANGLE-HEADED RIGHTWARDS ARROW} {after_nick!s}",
    )


@update.event("roles", description=_("Member role changes"))
@Member.check(roles_changed)
async def _update_roles(before: discord.Member, after: discord.Member):
    ar, br = _roles(before, after)
    changes = {
        **{role: True for role in ar if role not in br},
        **{role: False for role in br if role not in ar},
    }
    changed_by = await audit(discord.AuditLogAction.member_role_update)
    if changed_by and changed_by.bot:
        changes = {r: v for r, v in changes.items() if r.permissions.value & PRIVILEGED_PERMS.value}
        if not changes:
            return
    content = (
        (
            _("\N{LOCK WITH INK PEN} {member} updated their own roles")
            if changed_by == after
            else _("\N{LOCK WITH INK PEN} {moderator} updated roles for {member}")
        )
        if changed_by
        else _("\N{LOCK WITH INK PEN} Roles for {member} updated")
    )

    return {
        "content": content.format(member=after, moderator=changed_by),
        "embed": discord.Embed(
            description="\n".join(
                change_strs[v].format(item=k) for k, v in changes.items() if v is not None
            ),
            colour=await context_colour(after),
        ),
    }
