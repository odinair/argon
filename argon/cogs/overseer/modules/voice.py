import discord

from argon.cogs.overseer.module import Module
from argon.cogs.overseer.shared import icu as _

Voice = Module("voice", name=_("Voice"), description=_("Member voice state logging"))


update = Voice.ghost()


@update.event("channel", description=_("Member voice channel"))
@Voice.check(lambda __, b, a: b.channel != a.channel)
def channel(member: discord.Member, before: discord.VoiceState, after: discord.VoiceState):
    return (
        _("\N{MULTIPLE MUSICAL NOTES} {member} joined voice channel {after}")
        if before.channel is None
        else _("\N{MULTIPLE MUSICAL NOTES} {member} left voice channel {before}")
        if after.channel is None
        else _(
            "\N{MULTIPLE MUSICAL NOTES} {member} switched from voice channel {before} to {after}"
        )
    ).format(member=member, before=before.channel, after=after.channel)


# While Discord does have server mute/deafens in the audit log now, we don't include this
# mainly due to the fact that this could get extremely noisy, along with the fact that it could
# result in getting ourselves rate limited pretty quickly.
@update.event("mute")
def mute(*_):
    pass


@mute.event("self", description=_("Member self mute"))
@Voice.check(lambda _, b, a: b.self_mute != a.self_mute)
def _mute_self(member: discord.Member, __, after: discord.VoiceState):
    return (
        _("\N{FACE WITHOUT MOUTH} {member} muted")
        if after.self_mute
        else _("\N{FACE WITH OPEN MOUTH} {member} unmuted")
    ).format(member=member)


@mute.event("server", description=_("Member server mute"))
@Voice.check(lambda _, b, a: b.mute != a.mute)
def _mute_server(member: discord.Member, __, after: discord.VoiceState):
    return (
        _("\N{FACE WITHOUT MOUTH} {member} is now server muted")
        if after.mute
        else _("\N{FACE WITH OPEN MOUTH} {member} is no longer server muted")
    ).format(member=member)


@update.event("deaf")
def deaf(*_):
    pass


@deaf.event("self", description=_("Member self deafen"))
@Voice.check(lambda _, b, a: b.self_deaf != a.self_deaf)
def _deaf_self(member: discord.Member, __, after: discord.VoiceState):
    return (
        _("\N{SPEAKER WITH CANCELLATION STROKE} {member} deafened")
        if after.self_deaf
        else _("\N{SPEAKER} {member} undeafened")
    ).format(member=member)


@deaf.event("server", description=_("Member server deafen"))
@Voice.check(lambda _, b, a: b.deaf != a.deaf)
def _deaf_server(member: discord.Member, __, after: discord.VoiceState):
    return (
        _("\N{SPEAKER WITH CANCELLATION STROKE} {member} is now server deafened")
        if after.deaf
        else _("\N{SPEAKER} {member} is no longer server deafened")
    ).format(member=member)
