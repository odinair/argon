import asyncio
from datetime import timedelta
from difflib import unified_diff
from typing import Union

import discord
from babel.units import format_compound_unit

from argon.cogs.overseer.audit import AuditHandler, id_check
from argon.cogs.overseer.module import Module
from argon.cogs.overseer.shared import icu as _
from argon.cogs.overseer.utils import context_colour
from argon.translations import Humanize
from argon.utils.chat_formatting import bold, box, translate_permission

MessageableChannel = discord.TextChannel | discord.VoiceChannel
change_strs = {
    True: _("{item} — granted"),
    False: _("{item} — revoked"),
    None: _("{item} — cleared"),
}
Channel = Module(
    "channel", name=_("Channel"), description=_("Channel creation, deletion, and update logging")
)
audit = AuditHandler()
audit.check(
    discord.AuditLogAction.channel_create,
    discord.AuditLogAction.channel_delete,
    discord.AuditLogAction.channel_update,
    discord.AuditLogAction.thread_update,
    discord.AuditLogAction.thread_delete,
    check=id_check,
)


@Channel.event("create", description=_("Channel creation"), friendly_name=_("Channel creation"))
async def create(channel: discord.abc.GuildChannel):
    created_by = await audit(discord.AuditLogAction.channel_create)
    guild: discord.Guild = channel.guild
    private: set[Union[discord.Member, discord.Role]] = (
        {
            *[rm for rm, p in channel.overwrites.items() if p.read_messages is True],
            *[x for x in guild.roles if x.permissions.administrator],
        }
        if channel.overwrites and channel.overwrites_for(guild.default_role).read_messages is False
        else set()
    )
    if private:
        if len(private) > 10:
            private = {*[*private][:10], _("and {count} more...").format(count=len(private) - 10)}

        content = (
            _(
                "\N{SCROLL} {admin} created private {type, select, category {category}"
                " other {channel}} {channel}, restricted to \N{EM DASH} {roles}"
            )
            if created_by
            else _(
                "\N{SCROLL} Private {type, select, category {category} other {channel}}"
                " {channel} has been created, restricted to \N{EM DASH} {roles}"
            )
        )
    else:
        content = (
            _(
                "\N{SCROLL} {admin} created {type, select, category {category} other {channel}}"
                " {channel}"
            )
            if created_by
            else _(
                "\N{SCROLL} {type, select, category {Category} other {Channel}} {channel}"
                " has been created"
            )
        )
    return content.format(
        channel=channel,
        admin=created_by,
        roles=Humanize([x.mention for x in private]),
        type="category" if isinstance(channel, discord.CategoryChannel) else "channel",
    )


@Channel.event("delete", description=_("Channel deletion"), friendly_name=_("Channel deletion"))
async def delete(channel: discord.abc.GuildChannel):
    deleted_by = await audit(discord.AuditLogAction.channel_delete)
    return (
        _(
            "\N{WASTEBASKET} {admin} deleted {type, select, category {category} other {channel}}"
            " {channel}"
        ).format(
            channel=channel.name,
            admin=deleted_by,
            type="category" if isinstance(channel, discord.CategoryChannel) else "channel",
        )
        if deleted_by
        else _(
            "\N{WASTEBASKET} {type, select, category {Category} other {Channel}} {channel}"
            " {channel} has been deleted"
        ).format(
            channel=channel.name,
            type="category" if isinstance(channel, discord.CategoryChannel) else "channel",
        )
    )


@Channel.event("update", friendly_name=_("Channel updates"))
def update(*_):
    pass


@update.event("name", description=_("Channel name changes"), friendly_name=_("Channel name"))
@Channel.check(lambda b, a: b.name != a.name)
async def _update_name(before: discord.abc.GuildChannel, after: discord.abc.GuildChannel):
    changed_by = await audit(discord.AuditLogAction.channel_update)
    return (
        _("\N{PENCIL}\N{VARIATION SELECTOR-16} {admin} renamed {channel} from `{before}`").format(
            channel=after, before=before.name, admin=changed_by
        )
        if changed_by
        else _(
            "\N{PENCIL}\N{VARIATION SELECTOR-16} {channel} has been renamed from `{before}`"
        ).format(channel=after, before=before.name)
    )


@update.event(
    "category", description=_("Channel category changes"), friendly_name=_("Channel category")
)
@Channel.check(lambda b, a: b.category != a.category)
def _update_category(__, after: discord.abc.GuildChannel):
    return (
        _("\N{PENCIL}\N{VARIATION SELECTOR-16} {channel} was moved to category {category}")
        if after.category
        else _("\N{PENCIL}\N{VARIATION SELECTOR-16} {channel} is now uncategorized")
    ).format(channel=after, category=after.category)


def _overwrite_changes(a: dict, b: dict) -> dict:
    a, b = a.copy(), b.copy()
    a |= {x: discord.PermissionOverwrite() for x in b if x not in a}
    b |= {x: discord.PermissionOverwrite() for x in a if x not in b}
    return {
        r: {x: z for x, z in o if getattr(b[r], x, None) is not z}
        for r, o in a.items()
        if b[r] != o
    }


@update.event(
    "overwrites",
    description=_("Channel permission overwrite changes"),
    friendly_name=_("Channel permissions"),
)
@Channel.check(lambda b, a: bool(_overwrite_changes(a.overwrites, b.overwrites)))
async def _update_overwrites(before: discord.abc.GuildChannel, after: discord.abc.GuildChannel):
    # Try to avoid logging overwrite changes propagated down from categories
    # This sleep might not be long enough in some cases, but in most cases
    # should be enough.
    if after.category:
        await asyncio.sleep(0.3)
        # We're using Guild#get_channel here to ensure that we're getting the most current data
        if after.guild.get_channel(after.id).permissions_synced:
            return
    # noinspection PyTypeChecker
    changes = _overwrite_changes(after.overwrites, before.overwrites)

    # We aren't including audit log information due to rate limit concerns
    for k, v in changes.items():
        if not v:
            continue
        yield {
            "content": (
                _(
                    "\N{CLOSED LOCK WITH KEY} Overwrites for member {item} in {channel}"
                    " were updated"
                )
                if isinstance(k, discord.Member)
                else _(
                    "\N{CLOSED LOCK WITH KEY} Overwrites for role {item} in {channel}"
                    " were updated"
                )
            ).format(channel=after, item=k),
            "embed": discord.Embed(
                description="\n".join(
                    change_strs[nv].format(item=bold(translate_permission(p)))
                    for p, nv in v.items()
                ),
                colour=await context_colour(after),
            ),
        }


@update.event("topic", description=_("Channel topic changes"), friendly_name=_("Channel topic"))
@Channel.check(
    lambda before, after: isinstance(before, discord.TextChannel)
    and (before.topic or "") != (after.topic or "")
)
async def _update_topic(before: MessageableChannel, after: MessageableChannel):
    changed_by = await audit(discord.AuditLogAction.channel_update)
    content = (
        _("\N{PENCIL}\N{VARIATION SELECTOR-16} {admin} changed the topic for channel {channel}")
        if changed_by
        else _("\N{PENCIL}\N{VARIATION SELECTOR-16} Topic for {channel} has been updated")
    )

    return "\n".join(
        [
            content.format(channel=after, admin=changed_by),
            box(
                "\n".join(
                    [
                        *unified_diff(
                            (before.topic.split("\n") if before.topic else []),
                            (after.topic.split("\n") if after.topic else []),
                            lineterm="",
                        )
                    ][2:]
                ),
                lang="diff",
            ),
        ]
    )


@update.event(
    "nsfw",
    description=_("Text channel NSFW status changes"),
    friendly_name=_("Text channel NSFW status"),
)
@Channel.check(lambda b, a: isinstance(b, MessageableChannel) and b.nsfw != a.nsfw)
async def _update_nsfw(__, after: MessageableChannel):
    changed_by = await audit(discord.AuditLogAction.channel_update)
    if not changed_by:
        if after.nsfw:
            return _("\N{CROSS MARK} {channel} is now marked as NSFW").format(channel=after)
        else:
            return _("\N{CROSS MARK} {channel} is no longer marked as NSFW").format(channel=after)
    elif after.nsfw:
        return _("\N{CROSS MARK} {admin} marked {channel} as NSFW").format(
            channel=after, admin=changed_by
        )
    else:
        return _("\N{CROSS MARK} {admin} marked {channel} as not NSFW").format(
            channel=after, admin=changed_by
        )


@update.event(
    "slowmode",
    description=_("Channel slow mode changes"),
    friendly_name=_("Text channel slow mode"),
)
@Channel.check(
    lambda b, a: isinstance(b, MessageableChannel) and b.slowmode_delay != a.slowmode_delay
)
async def _update_slow(__, after: MessageableChannel):
    changed_by = await audit(discord.AuditLogAction.channel_update)
    # 1 second isn't possible with the Discord client, but is possible with the API.
    return (
        _(
            "\N{STOPWATCH} {admin} {slow, plural, =0 {cleared slow mode}"
            " one {set slow mode to 1 second} other {set slow mode to {slow} seconds}}"
            " in {channel}"
        ).format(channel=after, slow=after.slowmode_delay, admin=changed_by)
        if changed_by
        else _(
            "\N{STOPWATCH} Slow mode in {channel} {slow, plural, =0 {cleared}"
            " one {set to one second} other {set to {slow} seconds}}"
        ).format(channel=after, slow=after.slowmode_delay)
    )


@update.event(
    "userlimit",
    description=_("Voice user limit changes"),
    friendly_name=_("Voice channel user limit"),
)
@Channel.check(lambda b, a: isinstance(b, discord.VoiceChannel) and b.user_limit != a.user_limit)
async def _update_userlimit(__, after: discord.VoiceChannel):
    changed_by = await audit(discord.AuditLogAction.channel_update)
    return (
        _(
            "\N{PENCIL}\N{VARIATION SELECTOR-16} {admin} changed user limit for {channel} to"
            "{limit, plural, =0 {removed} one {changed to one member}"
            " other {changed to {limit} members}}"
        ).format(channel=after, limit=after.user_limit, admin=changed_by)
        if changed_by
        else _(
            "\N{PENCIL}\N{VARIATION SELECTOR-16} User limit for {channel} {limit, plural,"
            "=0 {removed} one {changed to one member} other {changed to {limit} members}}"
        ).format(channel=after, limit=after.user_limit)
    )


@update.event(
    "bitrate",
    description=_("Voice channel bitrate changes"),
    friendly_name=_("Voice channel bitrate"),
)
@Channel.check(lambda b, a: isinstance(b, discord.VoiceChannel) and b.bitrate != a.bitrate)
async def _update_bitrate(__, after: discord.VoiceChannel):
    bitrate = Humanize(
        format_compound_unit,
        int(str(after.bitrate)[:-3]),
        "kilobit",
        denominator_unit="second",
    )
    changed_by = await audit(discord.AuditLogAction.channel_update)
    return (
        _("\N{SPEAKER WITH ONE SOUND WAVE} {admin} set bitrate for {channel} to {bitrate}").format(
            channel=after, bitrate=bitrate, admin=changed_by
        )
        if changed_by
        else _(
            "\N{SPEAKER WITH ONE SOUND WAVE} Bitrate for {channel} has been changed to {bitrate}"
        ).format(channel=after, bitrate=bitrate)
    )


@Channel.event("threads", friendly_name=_("Threads"))
def threads(*__, **___):
    pass


@threads.event("create", description=_("Thread creations"), friendly_name=_("Thread creation"))
@Channel.check(
    # Make sure that this thread was created in the past 15 seconds
    # This is important as there is no other way to differentiate between the bot being
    # added to the thread by the gateway, or the thread actually having been created.
    lambda thread: thread.created_at
    >= (discord.utils.utcnow() - timedelta(seconds=15))
)
def thread_create(thread: discord.Thread):
    return _("\N{SPOOL OF THREAD} {user} created thread {thread}").format(
        thread=thread, user=thread.owner
    )


@threads.event(
    "delete", description=_("Thread deletions (not archives!)"), friendly_name=_("Text deletion")
)
async def thread_delete(thread: discord.Thread):
    responsible = await audit(discord.AuditLogAction.thread_delete)
    if responsible:
        return _("\N{SPOOL OF THREAD} {user} deleted thread `{thread}` in {channel}").format(
            thread=thread.name, channel=thread.parent, user=responsible
        )
    return _("\N{SPOOL OF THREAD} Thread `{thread}` in {channel} was deleted").format(
        thread=thread.name, channel=thread.parent
    )


thread_update = threads.ghost()


@thread_update.event(
    "slowmode", description=_("Thread slow mode"), friendly_name=_("Thread slow mode")
)
@Channel.check(lambda b, a: b.slowmode_delay != a.slowmode_delay)
async def thread_slowmode(__: discord.Thread, after: discord.Thread):
    responsible = await audit(discord.AuditLogAction.thread_update)
    if responsible:
        # pyseeyou replaces # in format arguments in plurals, who'd've thunk?
        if after.slowmode_delay == 0:
            return _("\N{SPOOL OF THREAD} {user} cleared slowmode on {thread}").format(
                user=responsible, thread=after
            )
        return _(
            "\N{SPOOL OF THREAD} {user} set slowmode on {thread} to {after, plural,"
            " one {1 second} other {# seconds}}"
        ).format(after=after.slowmode_delay, user=responsible, thread=after)
    return _(
        "\N{STOPWATCH} Slowmode on thread was {thread} {after, plural,"
        " =0 {cleared} one {set to 1 second} other {set to # seconds}}"
    ).format(after=after.slowmode_delay, thread=after)


@thread_update.event("lock", description=_("Thread locks"), friendly_name=_("Thread locks"))
@Channel.check(lambda b, a: b.locked != a.locked)
async def _thread_lock(__: discord.Thread, after: discord.Thread):
    responsible = await audit(discord.AuditLogAction.thread_update)
    if responsible:
        if after.locked:
            return _("\N{SPOOL OF THREAD} {user} locked thread {thread}").format(
                thread=after, user=responsible
            )
        else:
            return _("\N{SPOOL OF THREAD} {user} unlocked thread {thread}").format(
                thread=after, user=responsible
            )

    if after.locked:
        return _("\N{SPOOL OF THREAD} Thread {thread} was locked").format(thread=after)
    else:
        return _("\N{SPOOL OF THREAD} Thread {thread} was unlocked").format(thread=after)


@thread_update.event(
    "name", description=_("Thread name changes"), friendly_name=_("Thread name change")
)
@Channel.check(lambda b, a: b.name != a.name)
def _thread_name(before: discord.Thread, after: discord.Thread):
    return _(
        "\N{SPOOL OF THREAD} Thread {thread} in {channel} was renamed \N{EM DASH}"
        " `{before}` \N{HEAVY TRIANGLE-HEADED RIGHTWARDS ARROW} `{after}`"
    ).format(
        thread=after,
        channel=after.parent,
        before=discord.utils.escape_markdown(before.name),
        after=discord.utils.escape_markdown(after.name),
    )


@thread_update.event(
    "duration",
    description=_("Thread archive duration"),
    friendly_name=_("Thread auto-archive duration"),
)
@Channel.check(lambda b, a: b.auto_archive_duration != a.auto_archive_duration)
def _thread_auto_archive(__: discord.Thread, after: discord.Thread):
    return _(
        "\N{SPOOL OF THREAD} Thread {thread} in {channel} will now auto-archive after {delta}"
        " of inactivity"
    ).format(
        thread=after,
        channel=after.parent,
        delta=Humanize(timedelta(minutes=after.auto_archive_duration)),
    )
