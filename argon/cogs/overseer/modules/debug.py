from argon.cogs.overseer.module import Module
from argon.cogs.overseer.shared import log

"""
This module is only ever usable by the bot owner with `[p]overseer debug`.

Its meant to be very simple responses to debug various parts of how Overseer logs
what event parsers return (think `[p]ping` simple).
"""

Debug = Module(
    "debug",
    name="Debug",
    description="Utility module for debugging Overseer's logging internals",
    secret=True,
)


@Debug.event("simple", description="returns hello world")
def debug_event():
    return "hello world"


def _check(value: bool) -> bool:
    expected = True
    equal = value is expected
    log.debug("Check value: %r %s %r", value, "==" if equal else "!=", expected)
    return equal


@Debug.event("checks", description="simple debug event with a check", checks=[_check])
def debug_checks(_):
    return "hello checks"


@Debug.event("yield", description="debug sync yield logging")
def debug_yield():
    yield "hello yield"


@Debug.event("asyncyield", description="debug async yield logging")
async def debug_async_yield():
    yield "hello async yield"


@Debug.event("exclusive", mutually_exclusive=True)
def exclusive():
    pass


@exclusive.event("one", priority=1)
def _exclusive_one():
    return "exclusive 1"


@exclusive.event("two", priority=2)
def _exclusive_two():
    return "exclusive 2"


@exclusive.event("three", priority=3)
def _exclusive_three():
    return "exclusive 3"
