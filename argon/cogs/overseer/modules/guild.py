from datetime import timedelta

import discord
from discord.utils import get

from argon.cogs.overseer.audit import AuditHandler
from argon.cogs.overseer.config import GuildConfig
from argon.cogs.overseer.module import Module
from argon.cogs.overseer.shared import icu as _
from argon.cogs.overseer.utils import GUILD, context_colour
from argon.translations import Humanize
from argon.utils import generate_diff


def available(*args, **kwargs):
    args = (*args, *kwargs.values())
    return not any(x.unavailable for x in args if isinstance(x, discord.Guild))


Guild = Module("server", name=_("Server"), description=_("Server update logging"))
audit = AuditHandler()
audit.check(discord.AuditLogAction.guild_update, check=lambda *_, **__: True)


update = Guild.ghost()


@update.event("2fa", description=_("Two-factor authentication requirement for moderators"))
@Guild.check(lambda b, a: b.mfa_level != a.mfa_level)
def guild_2fa(__, after: discord.Guild):
    # Only the guild owner can enable/disable the 2fa requirement, so including audit information
    # is redundant.
    return (
        _("\N{CLOSED LOCK WITH KEY} Two-factor authentication is now required for moderators")
        if bool(after.mfa_level)  # instead of being a boolean or enum, this is instead... an int?
        else _("\N{OPEN LOCK} Two-factor authentication is no longer required for moderators")
    )


@update.event("name", description=_("Server name"))
@Guild.check(lambda b, a: b.name != a.name)
async def guild_name(before: discord.Guild, after: discord.Guild):
    changed_by = await audit()
    return (
        _(
            "\N{LABEL} {admin} changed the server name \N{EM DASH} "
            "`{before}` \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} `{after}`"
        ).format(after=after, admin=changed_by, before=before)
        if changed_by
        else _(
            "\N{LABEL} Server name has been changed \N{EM DASH} "
            "`{before}` \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} `{after}`"
        ).format(after=after, before=before)
    )


@update.event("owner", description=_("Ownership transfers"))
@Guild.check(lambda b, a: b.owner_id != a.owner_id)
def guild_owner_transfer(before: discord.Guild, after: discord.Guild):
    return _("\N{CROWN} **Server ownership has been transferred** from {before} to {after}").format(
        before=before.owner, after=after.owner
    )


# noinspection PyTypeChecker
guild_afk = update.event("afk")(lambda *_: None)


@guild_afk.event("channel", description=_("Voice AFK channel"))
@Guild.check(lambda b, a: b.afk_channel != a.afk_channel)
async def afk_channel(__, after: discord.Guild):
    changed_by = await audit()
    if not changed_by:
        content = (
            _("\N{SLEEPING SYMBOL} AFK channel has been changed to {channel}")
            if after.afk_channel
            else _("\N{SLEEPING SYMBOL} AFK channel has been unset")
        )
    else:
        content = (
            _("\N{SLEEPING SYMBOL} {admin} changed the AFK channel to {channel}")
            if after.afk_channel
            else _("\N{SLEEPING SYMBOL} {admin} unset the AFK channel")
        )

    return content.format(admin=changed_by, channel=after.afk_channel)


@guild_afk.event("timeout", description=_("Voice AFK timeout"))
@Guild.check(lambda b, a: b.afk_timeout != a.afk_timeout)
async def afk_timeout(__, after: discord.Guild):
    changed_by = await audit()
    content = (
        _("\N{STOPWATCH} {admin} changed the AFK timeout to {time}")
        if changed_by
        else _("\N{STOPWATCH} AFK timeout changed to {time}")
    )

    return content.format(time=Humanize(timedelta(seconds=after.afk_timeout)), admin=changed_by)


@update.event("filter", description=_("Image content filter"))
@Guild.check(lambda b, a: b.explicit_content_filter != a.explicit_content_filter)
async def guild_filter(__, after: discord.Guild):
    changed_by = await audit()
    content = (
        _(
            "\N{FRAME WITH PICTURE} {admin} set the server content filter to {status, select,"
            "     disabled {disabled}"
            "      no_role {set to scan images from members without any roles}"
            "  all_members {set to scan images from all members}"
            "}"
        )
        if changed_by
        else _(
            "\N{FRAME WITH PICTURE} Server content filter has been {status, select,"
            "     disabled {disabled}"
            "      no_role {set to scan images from members without any roles}"
            "  all_members {set to scan images from all members}"
            "}"
        )
    )

    return content.format(status=str(after.explicit_content_filter), admin=changed_by)


@update.event("verification", description=_("Member verification level"))
@Guild.check(lambda b, a: b.verification_level != a.verification_level)
async def guild_verif(__, after: discord.Guild):
    changed_by = await audit()
    content = (
        _(
            "\N{ALIEN MONSTER} {admin} changed the server verification level to {level, select, "
            "      none {no verification requirements}"
            "       low {require a verified email}"
            "    medium {require being registered on Discord for 5 minutes}"
            "      high {require being in the server for 10 minutes}"
            "   highest {require a verified phone number}"
            "     other {*an unknown verification level* (`{level}`)}"
            "}"
        )
        if changed_by
        else _(
            "\N{ALIEN MONSTER} Server verification level has been changed to"
            " {level, select, "
            "      none {no verification requirements}"
            "       low {require a verified email}"
            "    medium {require being registered on Discord for 5 minutes}"
            "      high {require being in the server for 10 minutes}"
            "   highest {require a verified phone number}"
            "     other {*an unknown verification level* (`{level}`)}"
            "}"
        )
    )

    return content.format(admin=changed_by, level=str(after.verification_level))


@update.event(
    "description", description=_("Community server description"), requires_features={"COMMUNITY"}
)
@Guild.check(lambda b, a: b.description != a.description)
async def guild_description(before: discord.Guild, after: discord.Guild):
    diff = generate_diff(before=before.description, after=after.description)
    embed = discord.Embed(colour=await context_colour(), description=diff)
    changed_by = await audit()
    return (
        {
            "content": _("\N{MEMO} {admin} changed the server description").format(
                admin=changed_by
            ),
            "embed": embed,
        }
        if changed_by
        else {
            "content": _("\N{MEMO} Server description has been updated").format(),
            "embed": embed,
        }
    )


@Guild.event("emoji", description=_("Server emoji additions, deletions, and renames"))
@Guild.check(available)
def guild_emoji(before: list[discord.Emoji], after: list[discord.Emoji]):
    before, after = set(before), set(after)
    cfg = GuildConfig.get(GUILD.get())

    def _emoji(e: discord.Emoji, mention_emoji: bool = True, link: bool = False):
        name = f"`:{e.name}:`" if e.require_colons else f"`{e.name}`"
        if cfg.prefer_embeds and link:
            name = f"[{name}]({e.url})"
        if e.is_usable() and mention_emoji:
            name = f"{e!s} {name}"
        return name

    # Added
    for emoji in set.difference(after, before):
        if emoji.roles:
            yield _(
                "\N{SMILING FACE WITH OPEN MOUTH} Emoji {emoji} has been added;"
                " exclusive to {role_count, plural, one {role} other {roles}} {role_list}"
            ).format(
                emoji=_emoji(emoji),
                role_count=len(emoji.roles),
                role_list=Humanize([x.mention for x in emoji.roles]),
            )
        else:
            yield _("\N{SMILING FACE WITH OPEN MOUTH} Emoji {emoji} has been added").format(
                emoji=_emoji(emoji)
            )

    # Name changed
    for emoji in [
        x
        for x in set.intersection(before, after)
        if get(before, id=x.id).name != get(after, id=x.id).name
    ]:
        be, ae = get(before, id=emoji.id), get(after, id=emoji.id)
        yield _("\N{PENCIL}\N{VARIATION SELECTOR-16} Emoji {before} renamed ➜ {after}").format(
            before=_emoji(be), after=_emoji(ae, mention_emoji=False)
        )

    # Removed
    for emoji in set.difference(before, after):
        yield _("\N{PUT LITTER IN ITS PLACE SYMBOL} Emoji {emoji} has been removed").format(
            emoji=_emoji(emoji, mention_emoji=False, link=True)
        )
