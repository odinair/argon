from typing import Sequence

import discord
from babel.lists import format_list

from argon.cogs.overseer.module import Module
from argon.cogs.overseer.shared import icu as _
from argon.cogs.overseer.utils import context_colour
from argon.translations import get_babel_locale
from argon.utils import generate_diff
from argon.utils.chat_formatting import box

Message = Module("message", name=_("Message"), description=_("Message edit and deletion logging"))


@Message.event("edit", description=_("Message edits"))
@Message.check(lambda b, a: not a.author.bot and b.content != a.content)
async def edit(before: discord.Message, after: discord.Message):
    return {
        "content": _("\N{PENCIL}\N{VARIATION SELECTOR-16} Message `{mid}` sent by {author} in {channel} was edited").format(
            mid=after.id, author=after.author, channel=after.channel
        ),
        "embed": discord.Embed(
            colour=await context_colour(after),
            description=box(generate_diff(before.content, after.content), lang="diff"),
        ),
    }


@Message.event("delete", description=_("Message deletions"))
@Message.check(lambda m: bool(m.content or m.system_content))
@Message.check(lambda m: not m.author.bot)
async def delete(message: discord.Message):
    embed = discord.Embed(
        colour=await context_colour(message),
        description=message.system_content,
        timestamp=message.created_at,
    )

    if message.type not in (discord.MessageType.default, discord.MessageType.reply):
        return {
            "content": _("\N{WASTEBASKET} A system message in {channel} was deleted").format(
                channel=message.channel
            ),
            "embed": embed,
        }

    if message.attachments:
        embed.add_field(
            name=_("{count, plural, one {Attachment} other {Attachments}}").format(
                count=len(message.attachments)
            ),
            value="\n".join(f"<{x.proxy_url}>" for x in message.attachments),
            inline=False,
        )

    return {
        "content": _(
            "\N{WASTEBASKET} Message `{mid}` sent by {author} in {channel} was deleted"
        ).format(mid=message.id, author=message.author, channel=message.channel),
        "embed": embed,
    }


# @Message.event("filtered", description=_("Filtered messages"), requires_cogs=["filter"])
async def filtered(message: discord.Message, hits: Sequence[str]):
    embed = discord.Embed(
        colour=await context_colour(message),
        description=message.content,
        timestamp=message.created_at,
    ).add_field(
        name=_("Filter hits"),
        value=format_list([f"`{x}`" for x in hits], locale=get_babel_locale()),
    )

    if message.attachments:
        embed.add_field(
            name=_("{count, plural, one {Attachment} other {Attachments}}").format(
                count=len(message.attachments)
            ),
            value="\n".join(f"<{x.proxy_url}>" for x in message.attachments),
            inline=False,
        )

    return {
        "content": _(
            "\N{SPEAK-NO-EVIL MONKEY} Message sent by {author} in {channel} filtered"
        ).format(author=message.author, channel=message.channel),
        "embed": embed,
    }


@Message.event("bulkdelete", description=_("Message bulk deletions"))
@Message.check(lambda __, message_ids: bool(message_ids))
def bulk_delete(channel: discord.abc.Messageable, message_ids: set[int]):
    return _(
        # While it's impossible for the one plurality case to happen in English,
        # it's still possible for other languages to use other plurality rules,
        # and I don't want to have to make it clear in other ways that
        # this message can be used with the plural format option.
        "\N{WASTEBASKET} {count, plural, one {One message has} other {{count} messages have}}"
        " been bulk deleted in {channel}"
    ).format(count=len(message_ids), channel=channel)
