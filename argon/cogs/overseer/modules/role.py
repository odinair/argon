from typing import Optional

import discord

from argon.cogs.overseer.audit import AuditHandler, id_check
from argon.cogs.overseer.config import GuildConfig
from argon.cogs.overseer.module import Module
from argon.cogs.overseer.shared import icu as _
from argon.cogs.overseer.shared import log
from argon.cogs.overseer.utils import context_colour
from argon.translations import Humanize
from argon.utils.chat_formatting import bold, inline, translate_permission

Role = Module("role", name=_("Role"), description=_("Role creation, deletion, and update logging"))
change_strs = {True: _("{item} — granted"), False: _("{item} — revoked")}
audit = AuditHandler()
audit.check(
    discord.AuditLogAction.role_create,
    discord.AuditLogAction.role_delete,
    discord.AuditLogAction.role_update,
    check=id_check,
)


@Role.event("create", description=_("Role creation"))
async def create(role: discord.Role):
    hoisted = (
        _("\N{LARGE GREEN CIRCLE} Displayed separately")
        if role.hoist
        else _("\N{LARGE RED CIRCLE} Not displayed separately")
    )
    mentionable = (
        _("\N{LARGE GREEN CIRCLE} Mentionable")
        if role.mentionable
        else _("\N{LARGE RED CIRCLE} Not mentionable")
    )
    colour = (
        _("\N{LARGE GREEN CIRCLE} Colour: {colour}")
        if role.colour.value
        else _("\N{LARGE RED CIRCLE} No role colour set")
    )
    permissions = (
        _("\N{LARGE GREEN CIRCLE} Permissions: {permissions}")
        if role.permissions.value
        else _("\N{LARGE RED CIRCLE} No permissions granted")
    )

    created_by = await audit(discord.AuditLogAction.role_create)
    content = (
        _("\N{CROSSED SWORDS}\N{VARIATION SELECTOR-16} {moderator} created role {role}\n\n{info}")
        if created_by
        else _("\N{CROSSED SWORDS}\N{VARIATION SELECTOR-16} Role {role} has been created\n\n{info}")
    )

    return content.format(
        role=role,
        info="\n".join(
            [
                hoisted.format(),
                mentionable.format(),
                colour.format(colour=str(role.colour)),
                permissions.format(
                    permissions=Humanize(
                        [bold(translate_permission(x)) for x, y in role.permissions if y]
                    )
                ),
            ]
        ),
        moderator=created_by,
    )


@Role.event("delete", description=_("Role deletion"))
async def delete(role: discord.Role):
    deleted_by = await audit(discord.AuditLogAction.role_delete)

    return (
        _("\N{WASTEBASKET} {moderator} deleted role `{role}`").format(
            role=role.name.replace("`", "\\`"), moderator=deleted_by
        )
        if deleted_by
        else _("\N{WASTEBASKET} Role `{role}` has been deleted").format(
            role=role.name.replace("`", "\\`")
        )
    )


@Role.event("update")
def update(*_):
    pass


@update.event("name", description=_("Role name changes"))
@Role.check(lambda b, a: b.name != a.name)
async def _update_name(before: discord.Role, after: discord.Role):
    changed_by = await audit(discord.AuditLogAction.role_update, skip_bots=True)

    return (
        _(
            "\N{PENCIL}\N{VARIATION SELECTOR-16} {moderator} changed name for role {role}:"
            " {before} \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} {after}"
        ).format(
            role=after,
            before=inline(before.name),
            moderator=changed_by,
            after=inline(after.name),
        )
        if changed_by
        else _(
            "\N{PENCIL}\N{VARIATION SELECTOR-16} Name for role {role} has been changed:"
            " {before} \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} {after}"
        ).format(role=after, before=inline(before.name), after=inline(after.name))
    )


@update.event("permissions", description=_("Role permissions changes"))
@Role.check(lambda b, a: b.permissions.value != a.permissions.value)
async def _update_permissions(before: discord.Role, after: discord.Role):
    changed = {
        k: getattr(after.permissions, k)
        for k, __ in discord.Permissions()
        if getattr(before.permissions, k) is not getattr(after.permissions, k)
    }

    if all(v is None for v in changed.values()):
        log.warning(
            "Permissions value changed from %s to %s, but no permissions were changed?",
            before.permissions.value,
            after.permissions.value,
        )
        return

    changed_by = await audit(discord.AuditLogAction.role_update)
    content = (
        _("\N{CLOSED LOCK WITH KEY} {moderator} updated permissions for role {role}")
        if changed_by
        else _("\N{CLOSED LOCK WITH KEY} Permissions for role {role} have been updated")
    )

    return {
        "content": content.format(role=after, moderator=changed_by),
        "embed": discord.Embed(
            description="\n".join(
                change_strs[v].format(item=bold(translate_permission(k)))
                for k, v in changed.items()
                if v is not None
            ),
            colour=await context_colour(after),
        ),
    }


@update.event("colour", description=_("Role colour changes"))
@Role.check(lambda b, a: b.colour != a.colour)
async def _update_colour(before: discord.Role, after: discord.Role):
    changed_by = await audit(discord.AuditLogAction.role_update, skip_bots=True)
    if not changed_by:
        content = (
            _(
                "\N{LOWER LEFT PAINTBRUSH} Colour for role {role} has been changed:"
                " `{before}` \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} `{after}`"
            )
            if after.colour.value
            else _(
                "\N{LOWER LEFT PAINTBRUSH} Colour for role {role} has been changed:"
                " `{before}` \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} *removed*"
            )
        )
    else:
        content = (
            _(
                "\N{LOWER LEFT PAINTBRUSH} {moderator} changed colour for role {role}:"
                " `{before}` \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} `{after}`"
            )
            if after.colour.value
            else _(
                "\N{LOWER LEFT PAINTBRUSH} {moderator} changed colour for role {role}:"
                " `{before}` \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} *removed*"
            )
        )

    content = content.format(
        role=after, after=str(after.colour), before=str(before.colour), moderator=changed_by
    )
    if GuildConfig.get(after.guild).prefer_embeds:
        return discord.Embed(
            colour=after.colour if after.colour.value != 0 else None,
            description=content,
        )
    else:
        return content


@update.event("mentionable", description=_("Role mentionable state changes"))
@Role.check(lambda b, a: b.mentionable != a.mentionable)
async def _update_mentionable(__, after: discord.Role):
    changed_by = await audit(discord.AuditLogAction.role_update)
    if not changed_by:
        content = (
            _("\N{BELL} Role {role} is now mentionable")
            if after.mentionable
            else _("\N{BELL WITH CANCELLATION STROKE} Role {role} is no longer mentionable")
        )
    else:
        content = (
            _("\N{BELL} {moderator} made role {role} mentionable")
            if after.mentionable
            else _("\N{BELL WITH CANCELLATION STROKE} {moderator} made role {role} unmentionable")
        )

    return content.format(role=after, moderator=changed_by)


@update.event("hoist", description=_("Role hoisted state changes"))
@Role.check(lambda before, after: before.hoist != after.hoist)
async def _update_hoist(__, after: discord.Role):
    changed_by = await audit(discord.AuditLogAction.role_update)
    if not changed_by:
        content = (
            _("\N{BELL} Role {role} is now displayed separately")
            if after.hoist
            else _(
                "\N{BELL WITH CANCELLATION STROKE} Role {role} is no longer displayed separately"
            )
        )
    else:
        content = (
            _("\N{PENCIL}\N{VARIATION SELECTOR-16} {moderator} made role {role} display separately")
            if after.hoist
            else _("\N{PENCIL}\N{VARIATION SELECTOR-16} {moderator} made role {role} no longer display separately")
        )

    return content.format(moderator=changed_by, role=after)


@Role.event("personal")
def personal(*_, **__):
    pass


@personal.event("name", description=_("Personal role name changes"))
async def personal_name(member: discord.Member, old_name: str, new_name: str):
    return _(
        "\N{MEMO} {member} updated their role name:"
        " {old} \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} {new}"
    ).format(
        member=member,
        old=inline(old_name),
        new=inline(new_name),
    )


@personal.event("colour", description=_("Personal role colour changes"))
async def personal_colour(member: discord.Member, old_colour: Optional[str], new_colour: str):
    return _(
        "\N{LOWER LEFT PAINTBRUSH} {member} updated their role colour:"
        " {old} \N{HEAVY ROUND-TIPPED RIGHTWARDS ARROW} {new}"
    ).format(
        member=member,
        old=inline(old_colour) if old_colour else _("*no colour*"),
        new=inline(new_colour),
    )
