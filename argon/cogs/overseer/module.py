from __future__ import annotations

from typing import Callable, Iterable

from argon.cogs.overseer.abc import CheckCallable, Loggable, ModuleABC
from argon.cogs.overseer.event import Event
from argon.cogs.overseer.shared import log, modules
from argon.translations.lazy import LazyStr


# Important implementation quirk: ModuleABC is the class that sets class-level attributes
class Module(ModuleABC):
    """Core module class

    Create an instance of this class to setup events usable for logging; see :meth:`event` for a
    usage example.

    .. seealso:: :class:`overseer.event.Event`
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.id.startswith("_"):
            raise ValueError("Module IDs must not start with an underscore")
        if self.id in modules:
            raise RuntimeError(f"Duplicated module ID {self.id!r}")
        log.debug("Registering module %r", self)
        modules[self.id] = self

    def event(  # pylint:disable=arguments-differ
        self,
        event_id: str,
        *,
        checks: list[CheckCallable] = None,  # pylint:disable=unused-argument
        description: str | LazyStr | None = None,
        friendly_name: str | LazyStr = ...,
        parent: Event = None,
        mutually_exclusive: bool = False,
        priority: int = 0,
        requires_features: set[str] | None = None,
        **kwargs,
    ) -> Callable[[Callable[..., Loggable]], Event]:
        """Create a new event on the current module

        This method should be used as a decorator. The decorated function may return
        a loggable return value by either returning or yielding it,
        be it in a sync or async function.

        All keyword arguments passed that aren't listed below are passed to :class:`Event`.

        Example
        -------
        >>> module = Module(
        ...     "example",
        ...     name="Example Module",
        ...     description="Super cool example module",
        ... )
        >>> @module.event("event")
        ... def event():
        ...     return "Hello, world!"
        >>> await event()  # if enabled, logs 'Hello, world!' to the guild's log channel

        Arguments
        ----------
        event_id: str
            The internal event ID; this is also used when referring to this event in commands,
            such as with ``[p]overseer (list|set)``
        friendly_name: str | LazyStr
            The friendly name used in ``[p]overseer list``
        description: Union[str, LazyStr]
            Optional event description used in ``[p]overseer list``. This may be omitted.
        parent: EventABC
            Which event this event is a child of. This may be omitted.
        checks: list[CheckCallable]
            A list of pre-defined check callables; this is an alternative to using
            :meth:`Module.check`. This may be omitted.
        mutually_exclusive: bool
            Whether this event is a mutually exclusive event group or not. Events considered
            mutually exclusive must have at least one child event, and will only ever call one when
            called, even if multiple child events are enabled.
        priority: float
            The priority in which this event will be called. This is best used with a mutually
            exclusive parent event, as there's no guarantee that events with a higher priority
            will actually log before other events. Lower numbers are prioritized before higher
            numbers.
            Child events of mutually exclusive events default to a priority of `inf`, meaning
            that events with a priority set will by default be preferred over ones that don't.
        requires_features: set[str]
            A set of :attr:`discord.Guild.features` values that the current guild must have for
            this event to function.
        """

        def decorator(func: Callable[..., Loggable]) -> Event:
            nonlocal event_id, checks
            event_id = event_id if event_id is not None else func.__name__
            checks = (checks or []) + getattr(func, "__overseer_checks__", [])
            return Event(
                event_id=event_id,
                callable=func,
                friendly_name=friendly_name,
                module=self,
                parent=parent,
                checks=checks,
                description=description,
                mutually_exclusive=mutually_exclusive,
                priority=priority,
                requires_features=requires_features,
                **kwargs,
            )

        return decorator

    def ghost(self) -> Event:
        """Create a ghost event

        Ghost events don't appear in ``[p]overseer`` and cannot be modified on their own,
        and as such are mostly useful for organizing events under a parent event in order
        to call all such events with one function call.
        """
        return Event(module=self, event_id="", callable=None)

    @staticmethod
    def check(predicate: CheckCallable):
        """Add a check to an event

        Event invocations that fail checks will never call the underlying function.

        Example
        -------
        >>> module = Module(...)
        >>> @module.event(...)
        ... @module.check(lambda *args: ...)  # remember to return True if this should log!
        ... def event(*args):
        ...     ...
        """

        def decorator(func):
            if isinstance(func, Event):
                func.checks.append(predicate)
            elif hasattr(func, "__overseer_checks__"):
                func.__overseer_checks__.append(predicate)
            else:
                func.__overseer_checks__ = [predicate]
            return func

        return decorator

    def walk(self) -> Iterable[Event]:
        """Walk through all events and child events on this module

        Events that have children are not returned directly.
        """
        for event in self.events:
            if event.children:
                yield from event.walk()
            else:
                yield event
