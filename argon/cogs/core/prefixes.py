import discord

from argon import commands, db
from argon.bot import Argon
from argon.cogs.core._shared import _
from argon.utils.chat_formatting import error, success, warning


class Prefixes:
    bot: Argon

    @commands.group(aliases=["prefix"])
    async def prefixes(self, ctx: commands.Context):
        """Manage server prefixes

        Mentioning the bot will always work as a prefix regardless of any settings changed here.
        """
        if not ctx.invoked_subcommand:
            # noinspection PyTypeChecker
            await ctx.invoke(self.prefix_local_list)

    @prefixes.command(name="list")
    async def prefix_local_list(self, ctx: commands.Context):
        """List all prefixes for the current server"""
        prefixes = await self.bot.prefixes(..., ctx.message)
        # <@...> and <@!...> are always prefixes, so remove one of them to avoid
        # listing the same prefix twice
        prefixes = prefixes[1:]
        embed = discord.Embed(
            description="\n".join(prefixes),
            title=_("Prefixes for {server}").format(server=ctx.guild.name),
            colour=discord.Colour.blurple(),
        )
        await ctx.send(embed=embed)

    @prefixes.command(name="add")
    @commands.has_guild_permissions(manage_guild=True)
    async def prefix_local_add(self, ctx: commands.Context, prefix: str):
        """Add a prefix for the current server"""
        if self.bot.user.mention in prefix:
            return await ctx.send(
                warning(_("Mentions to myself are always valid prefixes.").format())
            )
        guild = await db.Guild.find_one_or_create(ctx.guild)

        if not guild.prefixes:
            guild.prefixes = self.bot.bot_config.prefixes.copy()
            await guild.set({"prefixes": [*guild.prefixes, prefix]})
        elif prefix in guild.prefixes:
            return await ctx.send(
                warning(_("That's already one of my configured prefixes!").format())
            )
        else:
            await guild.update({"$push": {"prefixes": prefix}})
        await ctx.send(success(_("Okay, I will now listen for that as a prefix.").format()))

    @prefixes.command(name="remove")
    @commands.has_guild_permissions(manage_guild=True)
    async def prefix_local_remove(self, ctx: commands.Context, prefix: str):
        """Remove a prefix for the current server"""
        guild = await db.Guild.find_one_or_create(ctx.guild)
        if prefix not in guild.prefixes:
            await ctx.send(warning(_("That's not one of my configured prefixes.").format()))
            return
        await guild.update({"$pull": {"prefixes": prefix}})
        await ctx.send(success(_("Okay, I will no longer listen for that as a prefix.").format()))

    @prefixes.command(name="reset")
    @commands.has_guild_permissions(manage_guild=True)
    async def prefix_local_reset(self, ctx: commands.Context):
        """Reset this server's prefixes"""
        await db.Guild.find_one({"guild_id": ctx.guild.id}).set({"prefixes": []})
        await ctx.send(success(_("Reset prefixes for this server.").format()))

    @prefixes.group(name="global")
    @commands.is_owner()
    async def prefix_global(self, ctx: commands.Context):
        """Manage bot-wide prefixes"""

    @prefix_global.command(name="list")
    async def prefix_global_list(self, ctx: commands.Context):
        """List all prefixes"""
        prefixes = self.bot.bot_config.prefixes.copy()
        prefixes.insert(0, f"<@!{self.bot.user.id}> ")
        embed = discord.Embed(
            description="\n".join(prefixes),
            title=_("Prefixes for {bot}").format(bot=self.bot.user.name),
            colour=discord.Colour.blurple(),
        )
        await ctx.send(embed=embed)

    @prefix_global.command(name="add")
    async def prefix_global_add(self, ctx: commands.Context, prefix: str):
        """Add a global prefix"""
        # This is possible with `[p]prefix global add ""`
        if not prefix:
            return await ctx.send(
                error(
                    _(
                        "Cannot add an empty prefix as a global prefix. (tip: you can still add"
                        ' this as a per-server prefix with `{prefix}prefix add ""`)'
                    ).format(prefix=ctx.clean_prefix)
                )
            )
        if self.bot.user.mention in prefix:
            return await ctx.send(
                warning(_("Mentions to myself are always valid prefixes.").format())
            )
        bot = self.bot.bot_config
        if prefix in bot.prefixes:
            return await ctx.send(
                warning(_("That's already one of my configured prefixes!").format())
            )
        await bot.update({"$push": {"prefixes": prefix}})
        await ctx.send(success(_("Okay, I will now listen for that as a prefix.").format()))

    @prefix_global.command(name="remove")
    async def prefix_global_remove(self, ctx: commands.Context, prefix: str):
        """Remove a prefix"""
        bot = self.bot.bot_config
        if prefix in bot.prefixes:
            await bot.update({"$pull": {"prefixes": prefix}})
            await ctx.send(
                success(_("Okay, I will no longer listen for that as a prefix.").format())
            )
        else:
            await ctx.send(warning(_("That's not one of my configured prefixes.").format()))
