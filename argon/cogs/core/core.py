import time

import discord
from babel.units import format_unit

from argon import commands
from argon.bot import Argon
from argon.cogs.core._shared import _
from argon.cogs.core.permissions import Permissions
from argon.cogs.core.prefixes import Prefixes
from argon.cogs.core.settings import Settings
from argon.translations import Humanize


@_.cog()
class Core(commands.Cog, Permissions, Prefixes, Settings):
    """Core bot commands"""

    def __init__(self, bot: Argon):
        super().__init__()
        self.bot = bot

    @commands.command()
    async def ping(self, ctx: commands.Context):
        """Get the current latency to Discord"""
        message = _(
            "\N{TABLE TENNIS PADDLE AND BALL} **Pong!**\n\n"
            "Websocket latency to Discord is `{ws_latency}`\n\n"
            "Message send latency is `{message_latency}`"
        )
        ws_latency = Humanize(format_unit, self.bot.latency * 1000, "millisecond", length="short")
        embed = discord.Embed(
            colour=discord.Colour.greyple(),
            description=message.format(ws_latency=ws_latency, message_latency=_("calculating...")),
        )
        t1 = time.time()
        msg = await ctx.send(embed=embed)
        t2 = time.time()
        latency = (t2 - t1) * 1000
        message_latency = Humanize(format_unit, latency, "millisecond", length="short")
        embed.description = message.format(ws_latency=ws_latency, message_latency=message_latency)
        embed.colour = (
            discord.Colour.red()
            if latency >= 300
            else discord.Colour.yellow()
            if latency >= 150
            else discord.Colour.green()
        )
        await msg.edit(embed=embed)
