from datetime import timedelta
from typing import Optional

import discord

from argon import commands, db
from argon.bot import Argon
from argon.cogs.core._shared import _
from argon.translations import Humanize
from argon.utils.chat_formatting import success, warning


class Settings:
    bot: Argon

    @commands.group(name="set")
    async def core_settings(self, ctx: commands.Context):
        """Manage core bot settings"""

    @core_settings.command(name="stickerspam")
    @commands.guild_only()
    @commands.has_guild_permissions(manage_messages=True)
    async def set_welcome_sticker_spam(self, ctx: commands.Context, toggle: bool = None):
        """Toggle anti-welcome sticker spam

        This prevents members from spamming reply stickers on welcome messages;
        in particular, this restricts members to only one reply per join message.
        """
        data = await db.Guild.find_one_or_create(ctx.guild)
        new_val = not data.sticker_spam if toggle is None else toggle
        await data.set({"sticker_spam": new_val})
        if new_val:
            await ctx.send(
                success(
                    _(
                        "Members will now be prevented from spamming stickers in"
                        " reply to welcome messages."
                    ).format()
                )
            )
        else:
            await ctx.send(
                success(
                    _(
                        "Members will no longer be prevented from spamming stickers in"
                        " reply to welcome messages."
                    ).format()
                )
            )

    @core_settings.group(name="delete")
    @commands.guild_only()
    @commands.has_guild_permissions(manage_messages=True)
    async def set_delete(self, ctx: commands.Context):
        """Command invocation message deletion settings"""

    @set_delete.command(name="delay")
    async def set_delete_delay(self, ctx: commands.Context, delay: int = None):
        """Set how long to wait before deleting command messages

        Commands marked to be deleted with `[p]set delete command` will ignore this setting,
        even if the delay on it is higher than this setting.
        """
        guild = await db.Guild.find_one_or_create(ctx.guild)
        await guild.set({"delete_delay": delay})
        if delay is None:
            await ctx.send(
                success(_("Command invocation messages will no longer be deleted.").format())
            )
        else:
            await ctx.send(
                success(
                    _("Command invocation messages will now be deleted after {delta}.").format(
                        delta=Humanize(timedelta(seconds=delay))
                    )
                )
            )

    @set_delete.command(name="command")
    async def set_delete_command(
        self, ctx: commands.Context, delay: Optional[int] = None, *, command: str
    ):
        """Set how long to wait before deleting invocations of certain commands

        If `delay` is not specified, then the given command will not be deleted any differently
        from the setting of `[p]set delete delay`.
        """
        cmd = discord.utils.get(self.bot.walk_commands(), qualified_name=command)
        if not cmd:
            return await ctx.send(warning(_("That's not a valid command.").format()))
        guild = await db.Guild.find_one_or_create(ctx.guild)
        await guild.set({"command_delete": guild.command_delete | {command: delay}})
        if delay is None:
            await ctx.send(
                success(
                    _("Invocations of `{command}` will no longer be deleted.").format(
                        command=command
                    )
                )
            )
        else:
            await ctx.send(
                success(
                    _("Invocations of `{command}` will now be deleted after {delta}.").format(
                        delta=Humanize(timedelta(seconds=delay)), command=command
                    )
                )
            )

    @core_settings.command(name="colour", aliases=["color"])
    @commands.is_owner()
    async def set_bot_colour(self, ctx: commands.Context, colour: discord.Colour = None):
        """Set the default bot embed colour"""
        await self.bot.bot_config.set({"colour": getattr(colour, "value", None)})
        await ctx.send(success(_("Set bot colour.").format()))

    @core_settings.command(name="startupchannel")
    @commands.is_owner()
    async def set_startup_log_channel(self, ctx: commands.Context, channel: discord.TextChannel):
        """Set where the bot will log when it starts up"""
        await self.bot.bot_config.set({"startup_channel": channel.id})
        await ctx.send(
            success(
                _("Okay, I will now send a message in {channel} on startup.").format(
                    channel=channel
                )
            )
        )

    # TODO there's plans to use this but nothing concrete yet
    @core_settings.command(name="modchannel", hidden=True)
    @commands.guild_only()
    @commands.has_guild_permissions(manage_channels=True)
    async def set_mod_channel(self, ctx: commands.Context, channel: discord.TextChannel):
        """Set the server's mod channel"""
        data = await db.Guild.find_one_or_create(ctx.guild)
        await data.set({"mod_channel": channel.id})
        await ctx.send(
            success(_("Set the server's mod channel to {channel}.").format(channel=channel))
        )
