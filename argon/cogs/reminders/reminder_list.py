from __future__ import annotations

import discord

from argon import db, commands
from argon.cogs.reminders.modal import EditReminderModal
from argon.cogs.reminders.shared import _
from argon.utils.menus import ConfirmView, PaginatorView


class JumpModal(discord.ui.Modal):
    page_number = discord.ui.TextInput(label="Reminder", style=discord.TextStyle.short)

    def __init__(self, view: ReminderList, *args, **kwargs):
        super().__init__(*args, timeout=120, **kwargs)
        self.view = view
        self.page_number.label = _("Reminder ({range})").format(range=f"1-{len(view.pages)}")
        self.page_number.min_length = 1
        self.page_number.max_length = len(str(len(view.pages)))

    async def on_submit(self, interaction: commands.Interaction):
        try:
            self.view.current_reminder = int(self.page_number.value) - 1
        except ValueError:
            await interaction.response.send_message(
                content=f"``{self.page_number.value}`` could not be converted to a reminder",
                ephemeral=True,
            )
        else:
            self.view.update_buttons()
            await interaction.response.edit_message(**await self.view.send_kwargs())


# noinspection PyUnusedLocal
class ReminderList(PaginatorView):
    page: db.Reminder

    def __init__(
        self, reminders: list[db.Reminder], user: discord.abc.User, *, timeout: float = 600.0,
            original_interaction: commands.Interaction | None = None
    ):
        super().__init__(user, reminders, timeout=timeout)
        self.user = user
        self.original_interaction = original_interaction

    def update_buttons(self):
        super().update_buttons()
        self.remove_reminder.label = _("\N{BLACK UNIVERSAL RECYCLING SYMBOL} Delete").format()
        self.edit.label = _("\N{MEMO} Edit reminder").format()
        self.jump.label = _("\N{RIGHTWARDS ARROW WITH HOOK} Jump to reminder").format()

    async def send_kwargs(self) -> dict:
        kwargs = await super().send_kwargs()
        if not self.pages:
            kwargs["view"] = None
        return kwargs

    def format_page(self) -> discord.Embed:
        # This is structured the way it is for readability
        if not self.pages:  # sourcery skip
            # this is possible if the only reminder set is deleted through this view
            return discord.Embed(description=_("You have no active reminders!").format())

        return (
            discord.Embed(colour=discord.Colour.blurple(), description=self.page.text)
            .set_footer(
                text=_("Reminder {current} out of {total}").format(
                    current=self.current_page + 1, total=self.page_count
                )
            )
            .add_field(
                name="\N{ZERO WIDTH JOINER}",
                value=_("*Reminder is due {due, timestamp, relative}*").format(due=self.page.due),
            )
        )

    @discord.ui.button(style=discord.ButtonStyle.blurple, row=2)
    async def jump(self, interaction: commands.Interaction, __):
        await interaction.response.send_modal(JumpModal(self, title=_("Jump to reminder").format()))

    @discord.ui.button(style=discord.ButtonStyle.blurple, row=2)
    async def edit(self, interaction: commands.Interaction, __):
        async def post_edit():
            await self.original_interaction.edit_original_response(**await self.send_kwargs())

        await interaction.response.send_modal(
            EditReminderModal(self.page, post_edit_hook=post_edit)
        )

    @discord.ui.button(style=discord.ButtonStyle.red, row=2)
    async def remove_reminder(self, interaction: commands.Interaction, __):
        confirm = ConfirmView(yes_colour=discord.ButtonStyle.red, user=interaction.user, timeout=60)
        await interaction.response.send_message(
            _("Are you sure you want to delete this reminder? **This cannot be undone!**").format(),
            ephemeral=True,
            view=confirm,
        )
        if not await confirm.wait_result():
            await interaction.delete_original_response()
            return

        current = self.page
        self.pages.pop(self.pages.index(current))
        await current.delete()
        await interaction.edit_original_response(
            content=_("Okay, that reminder has been deleted.").format(), view=None
        )
        self.current_page -= 1
        self.update_buttons()
        await self.original_interaction.edit_original_response(**await self.send_kwargs())
        if not self.pages:
            self.stop()
