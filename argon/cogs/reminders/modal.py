from datetime import datetime, timedelta
from typing import Awaitable, Callable, Optional

import discord

from argon import commands, db
from argon.cogs.reminders.shared import _, scheduler
from argon.utils.chat_formatting import success
from argon.utils.time import TimeDelta

__all__ = ("CreateReminderModal", "EditReminderModal", "RescheduleView")


class CreateReminderModal(discord.ui.Modal):
    schedule_for = discord.ui.TextInput(label="Remind in", required=True)
    text = discord.ui.TextInput(
        label="Text", max_length=1500, required=True, style=discord.TextStyle.paragraph
    )

    def __init__(self, *, ephemeral: bool = False, **kwargs):
        super().__init__(
            title=kwargs.pop("title", _("Create a new reminder").format()),
            timeout=kwargs.pop("timeout", 60 * 10),
            **kwargs,
        )
        self.text.label = _("Reminder").format()
        self.schedule_for.label = _("Remind in").format()
        self.schedule_for.placeholder = _("e.g '1d', '12h', etc, minimum 1 minute").format()
        self.context_message: discord.PartialMessage | None = None
        self.ephemeral = ephemeral

    def _parse_time(self) -> timedelta:
        return TimeDelta().min_time("1m").sync_convert(self.schedule_for.value)

    async def on_submit(self, interaction: commands.Interaction, /) -> None:
        try:
            remind_in = self._parse_time()
        except commands.BadArgument as e:
            await interaction.response.send_message(str(e), ephemeral=True)
            return

        reminder = db.Reminder(
            text=self.text.value,
            due=datetime.utcnow() + remind_in,
            user_id=interaction.user.id,
            created_at=datetime.utcnow(),
            context_message=None if self.context_message is None else self.context_message.jump_url,
        )
        await reminder.save()
        if remind_in <= timedelta(minutes=15):
            scheduler.add(reminder.id, reminder.due)
        await interaction.response.send_message(
            success(_("Reminder scheduled for **{due, timestamp}**").format(due=reminder.due)),
            ephemeral=self.ephemeral,
        )


class EditReminderModal(CreateReminderModal):
    def __init__(
        self,
        reminder: db.Reminder,
        post_edit_hook: Optional[Callable[[], Awaitable | None]] = None,
        **kwargs,
    ):
        super().__init__(title=kwargs.pop("title", _("Edit Reminder").format()))
        self.reminder = reminder
        self.text.default = reminder.text
        self.schedule_for.required = False
        self.post_edit_hook = post_edit_hook

    async def on_submit(self, interaction: commands.Interaction, /) -> None:
        try:
            remind_in = self._parse_time()
        except commands.BadArgument:
            pass
        else:
            if self.reminder.id in scheduler:
                scheduler.stop(self.reminder.id)
            self.reminder.due = datetime.utcnow() + remind_in

        self.reminder.text = self.text.value
        await self.reminder.save()
        await interaction.response.send_message(
            _("Successfully updated the reminder for **{due, timestamp}**").format(
                due=self.reminder.due
            ),
            ephemeral=True,
        )
        if self.post_edit_hook:
            await discord.utils.maybe_coroutine(self.post_edit_hook)


class RescheduleView(discord.ui.View):
    def __init__(self, *, context_message: str | None = None):
        super().__init__(timeout=5)
        self.reschedule.label = _("Reschedule").format()
        if context_message is not None:
            self.add_item(
                discord.ui.Button(
                    label=_("Context").format(), url=context_message, style=discord.ButtonStyle.link
                )
            )

    @discord.ui.button(custom_id="reminder$reschedule_sent", style=discord.ButtonStyle.blurple)
    async def reschedule(self, *a, **kw):
        # the actual logic of this is handled in on_interaction; this view simply exists as a
        # convenient way to send this with a reminder message
        pass
