import logging
import re
import unicodedata
import warnings
from datetime import datetime, timezone
from typing import Iterator, Literal

import discord

from argon._shared import _
from argon.translations import LazyStr, PythonLazyStr

__all__ = (
    "error",
    "success",
    "warning",
    "info",
    "parse_unicode",
    "pagify",
    "translate_permission",
    "inline",
    "box",
    "bold",
    "text_to_file",
    "timestamp",
    "deny",
)

UNICODE_REGEX = re.compile(r"\\N{(?P<NAME>[A-Z -]+)}|\\U(?P<ID>\d+)")
log = logging.getLogger("argon.utils.chat_formatting")
TimestampFormat = Literal["t", "T", "d", "D", "f", "F", "R"]
# This dict is checked for missing keys at the bottom of this file
PERMISSIONS = {
    #
    # General server permissions
    #
    "read_messages": _("View channel"),
    "manage_channels": _("Manage channels"),
    "manage_roles": _("Manage roles"),
    "manage_expressions": _("Manage expressions"),
    "create_expressions": _("Create expressions"),
    "manage_events": _("Manage events"),
    "create_events": _("Create events"),
    "view_audit_log": _("View audit log"),
    "view_guild_insights": _("View server insights"),
    "view_creator_monetization_analytics": _("View monetization analytics"),
    "manage_webhooks": _("Manage webhooks"),
    "manage_guild": _("Manage server"),
    "use_external_apps": _("Use external apps"),
    #
    # Membership permissions
    #
    "create_instant_invite": _("Create invite"),
    "change_nickname": _("Change nickname"),
    "manage_nicknames": _("Manage nicknames"),
    "kick_members": _("Kick members"),
    "ban_members": _("Ban members"),
    "moderate_members": _("Moderate members"),
    #
    # Text channel permissions
    #
    "send_messages": _("Send messages"),
    "send_voice_messages": _("Send voice messages"),
    "send_messages_in_threads": _("Send messages in threads"),
    "create_public_threads": _("Create public threads"),
    "create_private_threads": _("Create private threads"),
    "embed_links": _("Embed links"),
    "attach_files": _("Attach files"),
    "add_reactions": _("Add reactions"),
    "external_emojis": _("Use external emojis"),
    "external_stickers": _("Use external stickers"),
    # the full permission name isn't being used here for the sake of brevity, and also
    # to avoid potential @everyone pings
    "mention_everyone": _("Mention everyone and all roles"),
    "manage_messages": _("Manage messages"),
    "manage_threads": _("Manage threads"),
    "read_message_history": _("Read message history"),
    "send_tts_messages": _("Send text-to-speech messages"),
    "use_application_commands": _("Use application commands"),
    "send_polls": _("Create polls"),
    #
    # Voice channel permissions
    #
    "connect": _("Connect"),
    "speak": _("Speak"),
    "stream": _("Video"),
    "use_voice_activation": _("Use voice activity"),
    "priority_speaker": _("Priority speaker"),
    "mute_members": _("Mute members"),
    "deafen_members": _("Deafen members"),
    "move_members": _("Move members"),
    "use_embedded_activities": _("Use activities"),
    "use_soundboard": _("Use soundboard"),
    "use_external_sounds": _("Use external sounds"),
    #
    # Stage channel permissions
    #
    "request_to_speak": _("Request to speak"),
    #
    # Advanced/uncategorized permissions
    #
    "administrator": _("Administrator"),
}


def get_permission(perm: str) -> LazyStr:
    return PERMISSIONS.get(perm, PythonLazyStr(perm.replace("_", " ").title()))


def translate_permission(perm: str) -> str:
    return str(get_permission(perm))


def deny(text: str) -> str:
    """Prepend a no entry emoji before the given text"""
    # I'm not sure why there's two different emojis with almost the same name but significantly
    # different designs, but... ok.
    return f"\N{NO ENTRY} {text}"


def error(text: str) -> str:
    """Prepend a no entry sign emoji before the given text"""
    return f"\N{NO ENTRY SIGN} {text}"


def success(text: str) -> str:
    """Prepend a green checkmark emoji before the given text"""
    return f"\N{WHITE HEAVY CHECK MARK} {text}"


def warning(text: str) -> str:
    """Prepend a warning sign emoji before the given text"""
    return f"\N{WARNING SIGN}\N{VARIATION SELECTOR-16} {text}"


def info(text: str) -> str:
    """Prepend an information emoji before the given text"""
    return f"\N{INFORMATION SOURCE}\N{VARIATION SELECTOR-16} {text}"


# noinspection PyShadowingBuiltins
def timestamp(
    dt: datetime, style: TimestampFormat | None = None, *, warn_nonutc: bool = True
) -> str:
    """Return the given datetime as a timestamp markdown object

    This is preferable over :func:`discord.utils.format_dt` as this can handle datetimes
    that are not already UTC-aware, whereas ``format_dt`` expects all datetimes given
    to already be UTC-aware.

    This function will warn if the given datetime is already timezone-aware, but is not
    *UTC*-aware; this behaviour can be suppressed by passing ``warn_nonutc=True``.

    .. seealso:: :func:`discord.utils.format_dt`
    """
    if dt is None:
        return ""
    if dt.tzinfo is None:
        dt = dt.replace(tzinfo=timezone.utc)
    elif dt.tzinfo != timezone.utc and warn_nonutc:
        warnings.warn("The given datetime is not in the UTC timezone", UserWarning)
    return discord.utils.format_dt(dt, style)


def inline(text: str) -> str:
    """Wrap the given text in an inline code block"""
    code = "`" if "`" not in text else "``"
    return f"{code}{discord.utils.escape_markdown(text)}{code}"


def bold(text: str) -> str:
    """Embolden the given text"""
    return f"**{text}**"


def box(text: str, *, lang: str = ""):
    """Wrap the given text in a code block"""
    return f"```{lang}\n{text}\n```"


def parse_unicode(text: str, strict: bool = False) -> str:
    r"""Parse unicode named character escape sequences

    For example, this replaces ``\N{INFORMATION SOURCE}`` with the relevant emoji character,
    and similarly with ``\U00008505``.

    Parameters
    ----------
    text: str
        The text to replace unicode character escape sequences in
    strict: bool
        If this is :obj:`True`, any invalid character escape sequences will raise a
        :class:`KeyError` instead of logging a warning and skipping it.
    """
    if not isinstance(text, str):
        warnings.warn("the given text is not a string", UserWarning)
        return text

    # {
    #   '\N{CHARACTER}': 'CHARACTER',
    #   '\U00000001': "00000001",
    # }
    chars: set[tuple[str, str]] = {
        (x.group(0), x.group("NAME") or x.group("ID")) for x in UNICODE_REGEX.finditer(text)
    }
    for orig, char in chars:
        replacement = None
        if char.isdigit():
            try:
                replacement = chr(int(char))
            except ValueError:
                if strict:
                    raise
                warnings.warn(f"{char!r} is not a valid character", UserWarning)
        else:
            try:
                replacement = unicodedata.lookup(char)
            except KeyError:
                if strict:
                    raise
                warnings.warn(
                    f"Failed to find any unicode character by the name {char!r}", UserWarning
                )
        if replacement is not None:
            text = text.replace(orig, replacement)
    return text


# The following function is taken from Red and modified to remove behaviour we don't need
# https://github.com/Cog-Creators/Red-DiscordBot/blob/ad6b8662b2/redbot/core/utils/chat_formatting.py#L235-L305
def pagify(
    text: str,
    delims=None,
    *,
    priority: bool = False,
    shorten_by: int = 8,
    page_length: int = 2000,
) -> Iterator[str]:
    """Generate multiple pages from the given text.

    Note
    ----
    This does not respect code blocks or inline code.

    Parameters
    ----------
    text: str
        The content to pagify and send.
    delims: Sequence[str]
        Characters where page breaks will occur. If no delimiters are found
        in a page, the page will break after ``page_length`` characters.
        By default, this only contains the newline character.
    priority: bool
        Set to :code:`True` to choose the page break delimiter based on the
        order of ``delims``. Otherwise, the page will always break at the
        last possible delimiter.
    shorten_by: int
        How much to shorten each page by. Defaults to 8.
    page_length: int
        The maximum length of each page. Defaults to 2000.

    Yields
    ------
    str
        Pages of the given text.
    """
    if delims is None:
        delims = ["\n"]

    in_text = text
    page_length -= shorten_by
    while len(in_text) > page_length:
        closest_delim = (in_text.rfind(d, 1, page_length) for d in delims)
        if priority:
            closest_delim = next((x for x in closest_delim if x > 0), -1)
        else:
            closest_delim = max(closest_delim)
        closest_delim = closest_delim if closest_delim != -1 else page_length
        to_send = in_text[:closest_delim]
        if len(to_send.strip()) > 0:
            yield to_send
        in_text = in_text[closest_delim:]

    if len(in_text.strip()) > 0:
        yield in_text


# https://github.com/Cog-Creators/Red-DiscordBot/blob/ad6b8662b2/redbot/core/utils/chat_formatting.py#L561-L585
def text_to_file(
    text: str, filename: str = "file.txt", *, spoiler: bool = False, encoding: str = "utf-8"
):
    """Prepares text to be sent as a file on Discord, without character limit.

    This writes text into a bytes object that can be used for the ``file`` or ``files`` parameters
    of :meth:`discord.abc.Messageable.send`.

    Parameters
    ----------
    text: str
        The text to put in your file.
    filename: str
        The name of the file sent. Defaults to ``file.txt``.
    spoiler: bool
        Whether the attachment is a spoiler. Defaults to ``False``.
    encoding: str
        The encoding to use for the returned file. Defaults to ``utf-8``.

    Returns
    -------
    discord.File
        The file containing your text.
    """
    from io import BytesIO

    file = BytesIO(text.encode(encoding))
    return discord.File(file, filename, spoiler=spoiler)


# This is a function to reduce on variable pollution in globals
# noinspection PyShadowingNames
def _check_missing_permissions():
    # Check for permissions set in VALID_FLAGS that don't have translation entries
    missing_perms = set.difference({*discord.Permissions.VALID_FLAGS}, {*PERMISSIONS})
    # Don't warn if aliases are missing from the permissions dict, and just copy them instead
    for perm in missing_perms.copy():
        pv = getattr(discord.Permissions, perm)
        if hasattr(pv, "alias") and pv.alias in PERMISSIONS:
            log.debug("Permission alias %r has a string from %r, copying that", perm, pv.alias)
            PERMISSIONS[perm] = PERMISSIONS[pv.alias]
            missing_perms.discard(perm)
    # Now warn for any permissions that are actually missing
    if missing_perms:
        log.warning(
            "The following permission(s) are missing translation entries: %r", missing_perms
        )
        PERMISSIONS.update(
            {
                perm: " ".join(
                    # Split the first word so we can title case it, and keep the rest lowercase
                    # Discord previously used to do this, but was so inconsistent that they seem
                    # to have changed back to simply title casing everything, but personally
                    # I think this looks nicer for our use case.
                    [
                        (permstr := perm.replace("guild", "server").split("_")).pop(0).title(),
                        *permstr,
                    ]
                )
                for perm in missing_perms
            }
        )


_check_missing_permissions()
del _check_missing_permissions
