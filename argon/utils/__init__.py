import os
import textwrap
from collections import defaultdict
from difflib import unified_diff
from typing import Any, MutableMapping, Optional, Sequence, TypeVar, Union, Callable

import discord
from discord import app_commands

T = TypeVar("T")
__all__ = (
    "qualified_app_command_name",
    "overwrites_channels",
    "missing_permissions",
    "deduplicate",
    "flatten",
    "generate_diff",
    "find_user_from_ref",
    "find_member_from_ref",
    "format_audit_reason",
    "MISSING",
    "SLASH_GUILD",
    "SLASH_GUILDS",
    "keydefaultdict",
    "slash_guild",
)
SLASH_GUILD = (
    discord.Object(id=gid) if (gid := os.getenv("SLASH_GUILD")) and gid.isdigit() else None
)
SLASH_GUILDS = [SLASH_GUILD.id] if SLASH_GUILD else []


def slash_guild(func):
    return app_commands.guilds(SLASH_GUILD)(func) if SLASH_GUILD else func


def qualified_app_command_name(
    command: app_commands.Command | app_commands.Group | app_commands.ContextMenu,
) -> str:
    if isinstance(command, app_commands.ContextMenu):
        return command.name
    names = [command.name]
    if command.parent:
        names.insert(0, qualified_app_command_name(command.parent))
    return " ".join(names)


class _Missing:
    def __bool__(self):
        return False

    def __repr__(self):
        return "<argon.core.utils.MISSING>"


MISSING = _Missing()


def format_audit_reason(moderator: discord.abc.User, reason: str = None) -> str:
    from argon._shared import _

    if not reason:
        return _("Action requested by {user}").format(user=str(moderator))
    return _("Action requested by {user}. Reason: {reason}").format(
        user=str(moderator), reason=textwrap.shorten(reason, 400)
    )


def missing_permissions(
    channel: discord.abc.GuildChannel | discord.Thread,
    member: discord.Member,
    permissions: discord.Permissions,
) -> set[str]:
    """Get a set of permissions a member does not have from the required set in a channel"""
    return {
        k
        for k, v in discord.Permissions(permissions.value & ~channel.permissions_for(member).value)
        if v
    }


def overwrites_channels(guild: discord.Guild) -> list[discord.abc.GuildChannel]:
    """Return a list of channels that overwrites should be modified for

    This excludes channels that would have any changed overwrites synced from their parent category,
    and channels that the bot would not have access to modify overwrites for.
    """
    return [
        channel
        for channel in guild.channels
        # Make sure that this is not synced with a parent category
        if not (channel.category and channel.permissions_synced)
        # and we also have the necessary permissions to modify overwrites
        and channel.permissions_for(guild.me).read_messages
        and channel.permissions_for(guild.me).manage_roles
    ]


async def find_user_from_ref(message: discord.Message) -> Optional[discord.abc.User]:
    if not message.reference or isinstance(
        message.reference.resolved, discord.DeletedReferencedMessage
    ):
        return None

    ref: discord.MessageReference = message.reference
    if ref.resolved is None:
        try:
            msg = await message.channel.fetch_message(ref.message_id)
        except discord.HTTPException:
            return None
    else:
        msg = ref.resolved

    return msg.author


async def find_member_from_ref(message: discord.Message) -> Optional[discord.Member]:
    if not message.guild:
        return None
    member = await find_user_from_ref(message)
    if not member:
        return None
    return message.guild.get_member(member.id)


def deduplicate(seq: Sequence[T], preserve_last: bool = False) -> list[T]:
    """Remove duplicate entries from a given sequence

    If ``preserve_last`` is :obj:`True`, the given values will be preserved in the order that
    they're last seen, instead of the default behavior of the first time they're seen; this
    behaviour incurs a performance penalty depending on the size of the given sequence.

    The given sequence items must all be hashable.

    Example
    -------
    >>> deduplicate([1, 2, 4, 3, 1, 5])
    [1, 2, 4, 3, 5]
    >>> deduplicate([1, 2, 4, 3, 1, 5], preserve_last=True)
    [2, 4, 3, 1, 5]
    """
    # this does slow us down a small bit, but not enough to be of a concern for the
    # few times that we'll make use of it (495ns vs 660ns with the above examples)
    if preserve_last:
        seq = reversed(seq)
    deduped = dict.fromkeys(seq).keys()
    if preserve_last:
        return [*reversed(deduped)]
    return [*deduped]


def flatten_dict(dct: MutableMapping[str, Any], parent_key: str = "", sep: str = "."):
    # originally from https://stackoverflow.com/a/6027615
    for k, v in dct.items():
        new_key = parent_key + sep + str(k) if parent_key else str(k)
        if isinstance(v, MutableMapping):
            yield from flatten_dict(v, new_key, sep=sep)
        else:
            # noinspection PyRedundantParentheses
            yield (new_key, v)


def flatten_list(lst: Sequence[Any]):
    for item in lst:
        if isinstance(item, (list, tuple, set)):
            yield from flatten_list(item)
        else:
            yield item


def flatten(to_flatten: Union[MutableMapping, Sequence], sep: str = ".") -> Union[dict, list]:
    return (
        dict(flatten_dict(to_flatten, sep=sep))
        if isinstance(to_flatten, MutableMapping)
        else list(flatten_list(to_flatten))
    )


def generate_diff(before: str, after: str) -> str:
    return "\n".join(
        [
            *unified_diff(
                # Adding a ZWJ after each ` character prevents messages from
                # escaping the code block fencing
                before.replace("`", "`\N{ZERO WIDTH JOINER}").split("\n") if before else [],
                after.replace("`", "`\N{ZERO WIDTH JOINER}").split("\n") if after else [],
                lineterm="",
            )
        ][2:]
    )


# noinspection PyPep8Naming
class keydefaultdict(defaultdict):
    default_factory: Callable[[Any], Any]

    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        ret = self[key] = self.default_factory(key)
        return ret
