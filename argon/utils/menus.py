from contextlib import suppress
from typing import Any, ClassVar

import discord

from argon import commands
from argon._shared import _
from argon.translations import LazyStr
from argon.utils.chat_formatting import deny

__all__ = (
    "ConfirmMenu",
    "ConfirmView",
    "PaginatorView",
    "OwnedView",
    "PromptView",
    "update_button_rows",
)


class OwnedView(discord.ui.View):
    """Basic view superclass implementing ownership

    This is a simple wrapper around making a view with an interaction_check implementation
    that checks that the interacting user is the user passed when creating the view.
    """

    def __init__(self, user: discord.abc.User, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user

    async def interaction_check(self, interaction: commands.Interaction) -> bool:
        if self.user == interaction.user:
            return True
        await interaction.response.send_message(
            deny(_("This button is not intended for you.")), ephemeral=True
        )
        return False


class PaginatorView(OwnedView):
    """Simple and extensible paginator view

    This is designed as an alternative to the Jishaku paginator that behaves in largely
    the same way for the end-user, while also being much easier to extend to change the
    behaviour of the pagination view.
    """

    PAGINATOR_BUTTON_ROW: ClassVar[int] = 1

    def __init__(self, user: discord.abc.User, pages: list, *, timeout: float = 600.0):
        super().__init__(user, timeout=timeout)
        self.pages = pages
        self._current_page = 0
        self.update_buttons()
        update_button_rows(
            self.first_page, self.back, self.forward, self.last_page, row=self.PAGINATOR_BUTTON_ROW
        )

    def _clamped_page_index(self, page: int) -> int:
        # to ensure that we always return a page that actually exists, we clamp this to...
        return min(
            # never go under 0, even if the user somehow manages to get a negative page number
            max(0, page),
            # never go above the amount of pages we actually have; this also has the side effect
            # of forcing this method to always return -1 when there are no pages
            self.page_count - 1,
        )

    @property
    def current_page(self) -> int:
        """The index of the paginator's current page

        This value is clamped to page indexes that actually exist, except in the case where there
        are no pages, in which case this will always return -1.

        Setting this value will also perform the same clamping action; as such, there's no guarantee
        that what you set this to will always be exactly the same as what you receive back when
        accessing this property again.
        """
        return self._clamped_page_index(self._current_page)

    @current_page.setter
    def current_page(self, value: int):
        self._current_page = self._clamped_page_index(value)

    @property
    def page(self) -> Any:
        """The current page this paginator is on"""
        return self.pages[self.current_page]

    @property
    def page_count(self) -> int:
        """The total amount of pages on this paginator"""
        return len(self.pages)

    def update_buttons(self):
        """Update the paginator buttons

        This should be called whenever the page index changes in any manner.
        """
        pagemax = self.page_count - 1
        self.first_page.disabled = self.current_page <= 0
        self.first_page.label = "\N{BLACK LEFT-POINTING DOUBLE TRIANGLE WITH VERTICAL BAR} 1"
        self.back.disabled = self.current_page <= 0
        self.forward.disabled = self.current_page == pagemax
        self.last_page.disabled = self.current_page == pagemax
        self.last_page.label = (
            f"\N{BLACK RIGHT-POINTING DOUBLE TRIANGLE WITH VERTICAL BAR} {self.page_count}"
        )
        self.current_page_button.label = f"{self.current_page + 1}"

    async def format_page(self) -> str | discord.Embed:
        """Format the current page and return it

        This method is intended to be overridden; by default, this simply returns the current page.

        This method may be async.
        """
        return self.pages[self.current_page]

    async def send_kwargs(self) -> dict:
        """The keyword argument dict to pass to Messageable#send and Message#edit"""
        page = await discord.utils.maybe_coroutine(self.format_page)
        return {"embed" if isinstance(page, discord.Embed) else "content": page, "view": self}

    @discord.ui.button(style=discord.ButtonStyle.secondary)
    async def first_page(self, interaction: commands.Interaction, __):
        self.current_page = 0
        self.update_buttons()
        await interaction.response.edit_message(**await self.send_kwargs())

    @discord.ui.button(
        label="\N{BLACK LEFT-POINTING TRIANGLE}", style=discord.ButtonStyle.secondary
    )
    async def back(self, interaction: commands.Interaction, __):
        self.current_page -= 1
        self.update_buttons()
        await interaction.response.edit_message(**await self.send_kwargs())

    @discord.ui.button(style=discord.ButtonStyle.blurple, label="1")
    async def current_page_button(self, interaction: commands.Interaction, __):
        self.update_buttons()
        await interaction.response.edit_message(**await self.send_kwargs())

    @discord.ui.button(
        label="\N{BLACK RIGHT-POINTING TRIANGLE}", style=discord.ButtonStyle.secondary
    )
    async def forward(self, interaction: commands.Interaction, __):
        self.current_page += 1
        self.update_buttons()
        await interaction.response.edit_message(**await self.send_kwargs())

    @discord.ui.button(style=discord.ButtonStyle.secondary)
    async def last_page(self, interaction: commands.Interaction, __):
        self.current_page = self.page_count
        self.update_buttons()
        await interaction.response.edit_message(**await self.send_kwargs())


class PromptView(OwnedView):
    """A view with a single method that waits for the view to stop before returning a result

    This doesn't do anything on its own, but is instead a basis for views
    that expect to behave as a prompt, such as :class:`ConfirmView`.
    """

    __prompt_delete_after__: bool = True

    def __init__(self, *args, **kwargs):
        from argon.utils import MISSING

        super().__init__(*args, **kwargs)
        self.result: Any = MISSING

    async def wait_result(self) -> Any:
        await self.wait()
        return self.result

    async def prompt(
        self,
        ctx: commands.Context,
        content: str = None,
        *,
        delete_after: bool = ...,
        default: Any = None,
        **kwargs,
    ) -> Any:
        """Use the current View to prompt for a response

        Only the user passed when creating this PromptView instance will be able to use
        the buttons attached to it; see also :class:`OwnedView`.

        Native view timeouts are supported, as is anything that makes ``self.wait()`` return.
        The final value of ``self.result`` will be what is returned.

        Any extra keyword arguments specified will be passed to :meth:`Messageable.send`.

        Arguments
        ----------
        ctx: commands.Context
            The context to use for sending this prompt
        content: Optional[str]
            The message content to send
        delete_after: bool
            If :obj:`True`, the prompt message will be deleted once the prompt returns a result;
            if omitted, this uses the value from ``self.__prompt_delete_after__``
        default: Any
            The default value to return if nothing is set in ``self.result`` when the prompt
            returns
        """
        delete_after = delete_after if delete_after is not ... else self.__prompt_delete_after__
        self.result = default
        msg = await ctx.send(content=content, view=self, **kwargs)
        await self.wait()
        if delete_after:
            with suppress(discord.HTTPException):
                await msg.delete()
        return self.result


# noinspection PyUnusedLocal
class ConfirmView(PromptView):
    """A simple yes or no confirmation prompt

    Example
    -------
    >>> @commands.hybrid_command()
    ... async def my_command(ctx: commands.Context):
    ...     view = ConfirmView(ctx.author)
    ...     if not await view.prompt(ctx, content="Do you wish to continue?", ephemeral=True):
    ...         return
    ...     # do something
    """

    def __init__(
        self,
        user: discord.User | discord.Member,
        default: bool = False,
        yes_colour: discord.ButtonStyle = discord.ButtonStyle.green,
        no_colour: discord.ButtonStyle = discord.ButtonStyle.grey,
        confirm_text: str | LazyStr = _("Confirm"),
        deny_text: str | LazyStr = _("Cancel"),
        *args,
        **kwargs,
    ):
        super().__init__(user, *args, **kwargs)
        self.__default = default

        def callback(value):
            async def cb(*_):
                self.result = value
                self.stop()

            return cb

        yes = discord.ui.Button(
            style=yes_colour, label=str(confirm_text), emoji="\N{WHITE HEAVY CHECK MARK}"
        )
        no = discord.ui.Button(style=no_colour, label=str(deny_text), emoji="\N{CROSS MARK}")
        yes.callback = callback(True)
        no.callback = callback(False)

        self.add_item(yes)
        self.add_item(no)

    async def on_timeout(self) -> None:
        self.result = self.__default

    # the following two methods are overridden for type hinting purposes
    async def wait_result(self) -> bool:
        return await super().wait_result()

    async def prompt(self, *args, **kwargs) -> bool:
        return await super().prompt(*args, **kwargs)


def update_button_rows(*buttons: discord.ui.Button, row: int):
    for btn in buttons:
        btn.row = row


# backwards compatibility
ConfirmMenu = ConfirmView
