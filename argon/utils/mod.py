import discord

from argon._shared import _


def hierarchy_allows(mod: discord.Member, target: discord.Member) -> str | None:
    """Utility function to determine if a member is allowed to action on a member based
    on role hierarchy

    Arguments
    ---------
    mod: discord.Member
        The moderator acting on ``target``
    target: discord.Member
        The member being actioned on

    Returns
    -------
    str
        This action is not allowed, and a prettified string has been returned to send to the
        moderator in question
    None
        The moderator may action on the target member
    """
    guild = mod.guild
    me = guild.me

    if mod == target or me == target:
        return _("I cannot allow this; self harm is bad \N{PENSIVE FACE}").format()
    if me.top_role <= target.top_role:
        return _("{target} is equal to or higher than me in the role hierarchy.").format(
            target=target
        )
    if mod.top_role <= target.top_role and guild.owner != mod:
        return _("{target} is equal to or higher tan you in the role hierarchy.").format(
            target=target
        )

    return None
