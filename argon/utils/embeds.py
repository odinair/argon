import discord

from argon._shared import _
from argon.utils import MISSING
from argon.utils import chat_formatting as cf

__all__ = ("info", "success", "warning", "error", "deny", "mod_action")


def info(text: str) -> discord.Embed:
    return discord.Embed(colour=discord.Colour.blue(), description=cf.info(text))


def success(text: str) -> discord.Embed:
    return discord.Embed(colour=discord.Colour.green(), description=cf.success(text))


def warning(text: str) -> discord.Embed:
    return discord.Embed(colour=discord.Colour.yellow(), description=cf.warning(text))


def error(text: str) -> discord.Embed:
    return discord.Embed(colour=discord.Colour.red(), description=cf.error(text))


def deny(text: str) -> discord.Embed:
    return discord.Embed(colour=discord.Colour.dark_red(), description=cf.deny(text))


def mod_action(
    action: str, reason: str = MISSING, *, colour: discord.Colour = discord.Colour.blurple()
) -> discord.Embed:
    embed = discord.Embed(colour=colour)
    embed.description = cf.bold(action)
    if reason is not MISSING:
        reason = reason if reason is not None else _("*no reason specified*").format()
        embed.description = f"{embed.description} | {reason}"
    return embed
