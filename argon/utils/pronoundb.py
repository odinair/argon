"""Simple PronounDB (https://pronoundb.org/) integration"""

import asyncio
import logging
from typing import MutableMapping

import aiohttp
import discord
from cachetools import TTLCache

from argon.translations import Translator, LazyStr

__all__ = ("get_pronouns", "format_pronouns", "PDB_PRONOUNS")
log = logging.getLogger("argon.utils.pronouns")
_ = Translator()
PRONOUNS: MutableMapping[int, list[str] | None] = TTLCache(ttl=60 * 60, maxsize=250)
PDB_TIMEOUT = aiohttp.ClientTimeout(total=3)
PDB_PRONOUNS: dict[str, LazyStr] = {
    # normative
    "he": _("Goes by he/him"),
    "it": _("Goes by it/its"),
    "she": _("Goes by she/her"),
    "they": _("Goes by they/them"),
    # meta sets
    "any": _("Any pronouns"),
    "ask": _("Ask me my pronouns"),
    "avoid": _("Avoid pronouns, use my name"),
    "other": _("Other pronouns"),
}


async def get_pronouns(
    member: discord.User | discord.Member, *, timeout: aiohttp.ClientTimeout = None
) -> list[str] | None:
    """Get a member's pronouns from PronounDB"""
    if member.id not in PRONOUNS:
        try:
            async with aiohttp.ClientSession(timeout=timeout or PDB_TIMEOUT) as session:
                async with session.get(
                    f"https://pronoundb.org/api/v2/lookup?platform=discord&ids={member.id}"
                ) as r:
                    resp_json: dict = await r.json()
                    PRONOUNS[member.id] = (
                        resp_json.get(f"{member.id}", {}).get("sets", {}).get("en")
                    )
        except aiohttp.ClientError | asyncio.TimeoutError:
            log.warning("Failed to get pronouns for member %r", member.id, exc_info=True)
            PRONOUNS[member.id] = None

    return PRONOUNS[member.id]


async def format_pronouns(
    member: discord.User | discord.Member, *, timeout: aiohttp.ClientTimeout = None
) -> str | LazyStr | None:
    """Get a formatted pronoun string for the given member"""
    pronouns = await get_pronouns(member, timeout=timeout)
    if pronouns is None or not len(pronouns):
        return None
    if len(pronouns) > 1:
        return "/".join(pronouns)
    return PDB_PRONOUNS.get(pronouns[0])
